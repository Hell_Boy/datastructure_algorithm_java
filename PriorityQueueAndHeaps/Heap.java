
import java.util.Arrays;

public class Heap {
    
    public int[] array;
    public int count;
    public int capacity;
    public int heap_type;
    
    public Heap(int capacity, int heap_type){
        this.heap_type = heap_type;
        this.capacity = capacity;
        this.count= 0;
        this.array = new int[capacity];
    }

    public int getParent(int i){
        if(i < 1 || i > this.count)
        return -1;

        return (i-1)/2;
    }

    public int getLeftChild(int i){

        int left = 2*i+1;
        if(left > this.count-1)
        return -1;
        
        return left;
    }

    public int getRightChild(int i){

        int right = 2*i+2;
        if(right > this.count-1)
        return -1;

        return right;
    }

    // For max heap max element is at root

    public int getMaximum(){

        if(this.count == 0)
        return -1;

        return this.array[0];
    }


    public void heapifyPrelocateDown(int i){

        int left = getLeftChild(i);
        int right = getRightChild(i);
        int max = i;
        int temp = 0;

        if(left != -1 && this.array[left] > this.array[max])
        max = left;

        if(right != -1 && this.array[right] > this.array[max])
        max = right;

        if(max != i){

            temp  = this.array[i];
            this.array[i] = this.array[max];
            this.array[max] = temp;
            heapifyPrelocateDown(max);
        }
    }

    public int deleteMax(){

        if(this.count == 0)
        return -1;

        int data = this.array[0];
        this.array[0] = this.array[this.count -1];
        this.count--;
        heapifyPrelocateDown(0);
        return data;
    }

    public void insert(int data){

        if(this.count == this.capacity){

            resizeHeap();
        }

        int i = this.count;
        this.count++;
        this.array[i] = data;

        heapifyPrelocateDown(0);
    }

    private void resizeHeap(){
        int[] array_old = Arrays.copyOfRange(this.array, 0, this.count-1);
        this.array = new int[this.capacity*2];

        if(this.array == null){
            System.out.println("Memory Error");
            return;
        }

        for (int i = 0; i < this.count-1; i++) {
            this.array[i] = array_old[i];
        }


        this.capacity = this.capacity*2;
        array_old = null;
    }

    public void destroyHeap(){
        this.count = 0;
        this.array = null;
    }

    public void buildHeap(Heap h, int[] a, int n){

        if(h == null)
        return;

        while(n > h.capacity)
        h.resizeHeap();

        for (int i = 0; i < n; i++) {
            h.array[i] = a[i];
        }

        for(int i = (n-1)/2; i > -1; i--)
        h.heapifyPrelocateDown(i);
    }

    // public void heapSort(int[]a, int n){
    //     Heap h = new Heap(n,0);
    //     int old_size, i , temp;
    //     h.buildHeap(h, a, n);
    //     old_size = h.count;
    //     for(int i = n-1; i>0; i++){
    //         temp = a[0];
    //         a[0]= a[h.count-1];
    //         h.count--;
    //         h.heapifyPrelocateDown(0);
    //         System.out.println(temp);
    //     }

    //     h.count = old.old_size;
    // }
}