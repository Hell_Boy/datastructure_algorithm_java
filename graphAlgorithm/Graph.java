
// Graph Implementation using Adjancey Matrix (used for dense data => Every node connected with every other node), space complexity O(n^2)
// Another Graph Implementation methods are Adjancey List (used for sparse data using linked list for loosely connected node)

import java.util.*;

public class Graph {
    
    private static final String NEWLINE = System.getProperty("line.separator");
    private final int V;
    private int E;
    private boolean [][] adjMatrix;
    boolean[] visited;

    // Empty graph with V vertices
    public Graph(int V){

        if(V < 0) throw new IllegalArgumentException("Too few vertices");

        this.V = V;
        this.E = 0;
        this.adjMatrix = new boolean[V][V];
        
    }

    // Random graph with V vertices and E edges
    public Graph(int V, int E){
        this(V);
        // E must be < nC2 combintation where n is number of Edges and 2 is the edge formed using vertices + n which is self loop
        if(E > (long) V*(V-1)/2 +V)
        throw new IllegalArgumentException("Too many edges");

        if(E < 0)
        throw new IllegalArgumentException("Too less edges");

        Random random = new Random();
        while(this.E !=E){
            int u = random.nextInt(V);
            int v = random.nextInt(V);

            addEdge(u,v);
        }

    }

    // number of vertices and edges

    public int V() {
        return V;
    }

    public int E(){
        return E;
    }

    public void addEdge(int u, int v){

        if(!adjMatrix[u][v]) E++;
        adjMatrix[u][v] = true;
        adjMatrix[v][u] = true;
    }

    // does graph contains edge
    public boolean contains(int u, int v){
        return adjMatrix[u][v];
    }

    // return list of neigbors of u
    public Iterable<Integer> adjMatrix(int u){
        return new AdjIterator(u);
    }


    
    // support iteration over graph
    private class AdjIterator implements Iterator<Integer>, Iterable<Integer> {

        private int u;
        private int v = 0;

        AdjIterator(int u){
            this.u = u;
        }

        public Iterator<Integer> iterator(){
            return this;
        }

        public boolean hasNext(){

            while(v<V){
                if(adjMatrix[u][v]) return true;
                v++;
            }
            return false;
        }

        public Integer next(){

            if(!hasNext())
            throw new NoSuchElementException();
            return v++;
        }

        public void remove(){
            throw new UnsupportedOperationException();
        }
    }

    // string representation of Graph - takes quadratic time
    public String toString(){

        StringBuilder s = new StringBuilder();
        s.append("Undirected graph"+NEWLINE);
        s.append("Vertices : "+V+" and edges : "+E+NEWLINE);
        s.append(NEWLINE);
        s.append("    ");
        for(int i = 0; i < V; i++)
        s.append(String.format("%7s", i));
        s.append(NEWLINE);
        s.append(NEWLINE);

        for (int u = 0; u < V; u++) {
            s.append(u+" : ");
            for(int v = 0 ; v< V; v++)
            s.append(String.format("%7s", adjMatrix[u][v]));
            s.append(NEWLINE);
        }

        return s.toString();
    }


    // DFS
    public void DFS(){

        Stack<Integer> s = new Stack<Integer>();

        visited = new boolean[V];

        s.push(0);

        // Loop active nodes

        while(!s.empty()){

            int currentNode = s.pop();
            
            if(!visited[currentNode]){

                visited[currentNode] = true;

                System.out.println("DFS visited node : "+currentNode);

                for(int i=0; i < V; i++)
                if((adjMatrix[currentNode][i] == true) && !visited[i])
                s.push(i);
            }
        }
    }

    public void BFS(){
        
        Queue<Integer> q = new LinkedList<Integer>();
        visited = new  boolean[V];

        q.add(0);

        while(!q.isEmpty()){
   
            int currentNode = q.remove();

            System.out.println("BFS visited node : "+currentNode);

            if(!visited[currentNode]){

                visited[currentNode] = true;

                for (int i = 0; i < V; i++) {

                    if((adjMatrix[currentNode][i]) == true && !visited[i])
                    q.add(i);
                }
            }
        }


    }
}