
public class BasicOperationsOfBT {
    public static void main(String[] args) {
        
        // BinaryTreeNode root = new BasiOpertaion().getBT1();
        
        // new BasiOpertaion().printBinaryTreePath(root);

        // //  region Tree Traversal
        
        // // Pre Order Traversal (DLR)
        // System.out.println("===> PreOrder Traversal <===\n");
        // new BasiOpertaion().PreOrderTraversal(root);
        // System.out.println("\n=============> End <==============\n");
        
        //  // Post Order Traversal (LRD)
        //  System.out.println("===> PostOrder Traversal <===\n");
        //  new BasiOpertaion().PostOrderTraversal(root);
        //  System.out.println("\n=============> End <==============\n");
        
        // //  In Order Traversal (LDR)
        //  System.out.println("===> InOrder Traversal <===\n");
        //  new BasiOpertaion().InOrderTraversal(root);
        //  System.out.println("\n=============> End <==============\n");
        
        // //  Level Order Traversal 
        //  System.out.println("===> Level Order Traversal <===\n");
        //  new BasiOpertaion().LevelOrderTraversal(root);
        //  System.out.println("\n=============> End <==============\n");
        
        // //  endregion
        
        
        // Maximum Value in binary tree
        
        // Max with recursion
        // int maxValueInBinaryTree = new BasiOpertaion().maxInBinaryTree(root);
        // System.out.println(maxValueInBinaryTree);
        
        // // Max with loop
        // int maxValueInBinaryTreeUsingLoop = new BasiOpertaion().maxInBinaryTreeWithoutRecurrsion(root);
        // System.out.println(maxValueInBinaryTreeUsingLoop);
        
        // // Search an element in binary tree
        // boolean found = new BasiOpertaion().findInBT(root, 15);
        // System.out.println("Element present in BT : "+found);
        
        // // Search an element in binary tree non recursive
        // boolean found_1 = new BasiOpertaion().findInBTNonRecursive(root, 15);
        // System.out.println("Element present in BT : "+found_1);
        
        
        // Insert an element in binary tree insertToBinaryTreeLevelOrder
        // BinaryTreeNode new_root = new BasiOpertaion().insertToBinaryTreeLevelOrder(root, 35);
        //  System.out.println("===> Level Order Traversal <===\n");
        //  new BasiOpertaion().LevelOrderTraversal(new_root);
        //  System.out.println("\n=============> End <==============\n");
        
        // //  insertToBinaryTreeLevelOrder2
        // BinaryTreeNode new_root_1 = new BasiOpertaion().insertToBinaryTreeLevelOrder2(root, 35);
        //  System.out.println("===> Level Order Traversal <===\n");
        //  new BasiOpertaion().LevelOrderTraversal(new_root_1);
        //  System.out.println("\n=============> End <==============\n");
        
        
        
        //  Insert an element in binary tree Recursive Method 
        // BinaryTreeNode recursive_root = new BasiOpertaion().insertToBinaryTreeLevelOrderRecursive(root, 0);
        // System.out.println("===> Level Order Traversal <===\n");
        // new BasiOpertaion().LevelOrderTraversal(recursive_root);
        // System.out.println("\n=============> End <==============\n");
        
        // Tree size 
        // int treeSize = new BasiOpertaion().size(root);
        // int treeSize = new BasiOpertaion().sizeNonRecursive(root);
        // System.out.println("Tree size : "+treeSize);
        
        
        // Reverse Level Order Traversal
        // System.out.println("Reverse Order Traversal : ");
        // new BasiOpertaion().reverseLevelOrderTraversal(root);
        
        // Length Of Binary Tree
        // System.out.println("Length : "+ new BasiOpertaion().lengthOfBinaryTree(root));
        
        // Not working
        // System.out.println("Length : "+ new BasiOpertaion().heightOfBinaryTreeWithoutRecursion(root));
        
        // Deepest Node
        // BinaryTreeNode deepestNode = new BasiOpertaion().deepestNode(root);
        // System.out.println("Deepest Node : "+deepestNode.data);
        
        // Leaf Node count
        // int leafNodes = new BasiOpertaion().numberOfLeafNodes(root);
        // System.out.println("Leaf Nodes : "+leafNodes);
        
        // Full Node count
        // int fullNodesCount = new BasiOpertaion().numberOfFullNodes(root);
        // System.out.println("Full Nodes : "+fullNodesCount);
        
        // Half Node Count
        // int halfNodesCount = new BasiOpertaion().numberOfHalfNodes(root);
        // System.out.println("Half Nodes : "+halfNodesCount);
        
        // Print Tree path from root to leaf
        // new BasiOpertaion().printBinaryTreePath(root);
        
        // hasPathSum
        // boolean pathSumExists = new BasiOpertaion().hasPathSum(root, 8);
        // System.out.println("Has path sum : "+pathSumExists);
        
        // Tree node sum
        // int sum = new BasiOpertaion().addBT(root);
        // int sum = new BasiOpertaion().addBTNonRecursively(root);
        // System.out.println("Sum : "+sum );
        
        // Structurally Identical Tree check
        // BinaryTreeNode root1 = root;
        // BinaryTreeNode root2 =  new BinaryTreeNode(2);
        // root2.setRight(new BinaryTreeNode(5));
        
        // System.out.println("Tree 1 Size : "+ new BasiOpertaion().sizeNonRecursive(root1));
        // System.out.println("Tree 2 Size : "+ new BasiOpertaion().sizeNonRecursive(root2));
        
        // boolean structurallyIdenticalTree = new BasiOpertaion().structurallyIdenticalTree(root1, root2);
        
        // System.out.println("Trees are structurally equal : "+structurallyIdenticalTree);
        
        // Diameter of BT
        /**
        *                           7
        * 
        *                   8               28
        * 
        *              18       48      78       38
        * 
        *                   68       58
        */
        
        // BinaryTreeNode temp = new BinaryTreeNode(7);
        
        // temp.setLeft(new BinaryTreeNode(8));
        // temp.setRight(new BinaryTreeNode(28));
        
        // temp.getRight().setRight(new BinaryTreeNode(38));
        // temp.getRight().setLeft(new BinaryTreeNode(78));
        
        // temp.getLeft().setLeft(new BinaryTreeNode(18));
        // temp.getLeft().setRight(new BinaryTreeNode(48));
        // temp.getLeft().getRight().setRight(new BinaryTreeNode(58));
        // temp.getLeft().getRight().setLeft(new BinaryTreeNode(68));
        
        // int diameterOfBT = new BasiOpertaion().diameterOfBT(temp);
        // System.out.println("Diameter of BT : "+diameterOfBT);
        
        
        // Max Sum at any level of Binary Tree
        // int maxSum = new BasiOpertaion().levelMaxSumOfBT(root);
        // System.out.println("Maximum possible sum : "+maxSum);4
        
        // Print all paths
        // new BasiOpertaion().printPathsOfBT(root);

        // Mirror binary tree
        // initial tree
        // new BasiOpertaion().printPathsOfBT(root);
        // System.out.println("Mirror");
        // new BasiOpertaion().printPathsOfBT(new BasiOpertaion().mirror(root));

        // Check if mirror binary tree

        // BinaryTreeNode temp = new BinaryTreeNode(7);
        
        // temp.setLeft(new BinaryTreeNode(8));
        // temp.setRight(new BinaryTreeNode(28));
        
        // temp.getRight().setRight(new BinaryTreeNode(38));
        // temp.getRight().setLeft(new BinaryTreeNode(78));
        
        // temp.getLeft().setLeft(new BinaryTreeNode(18));
        // temp.getLeft().setRight(new BinaryTreeNode(48));
        // temp.getLeft().getRight().setRight(new BinaryTreeNode(58));
        // temp.getLeft().getRight().setLeft(new BinaryTreeNode(68));

        // System.out.println("Are mirror : "+ new BasiOpertaion().areMirror(root, temp));

    }
}