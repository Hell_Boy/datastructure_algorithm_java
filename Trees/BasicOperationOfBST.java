
public class BasicOperationOfBST {
    public static void main(String[] args) {
        
        BinaryTreeNode root = new BasiOpertaion().getBST1();
        new BasiOpertaion().printBinaryTreePath(root);
        
        BinaryTreeNode root2 = new BasiOpertaion().getBST2();
        // new BasiOpertaion().printBinaryTreePath(root2);


        // Find an element in binary tree
        // int searchElement = 14;
        // BinaryTreeNode dataNode = new BasiOpertaion().findElementBST(root, searchElement);
        // BinaryTreeNode dataNode = new BasiOpertaion().findElementBSTNonRecursive(root, searchElement);

        // if(dataNode == null){
        //     System.out.println(searchElement +" is not in tree.");
        // }else{
        //     System.out.println(searchElement+" is present in binary tree.");
        // }


        // Find min element in BST
        // BinaryTreeNode minNode = new BasiOpertaion().findMinBST(root);
        // BinaryTreeNode minNode = new BasiOpertaion().findMinBSTNonRecursive(root);
        // System.out.println("Minimum element in bst : "+ minNode.getData());

        // Find max element in BST
        // BinaryTreeNode maxNode = new BasiOpertaion().findMaxBST(root2);
        // BinaryTreeNode maxNode = new BasiOpertaion().findMaxBSTNonRecursive(root);
        // System.out.println("Max element BST : "+maxNode.getData());

        // Insert an element in BST.insertNodeBST(root, 5);
        // System.out.println("Updated BST");
        // new BasiOpertaion().printBinaryTreePath(updated_bst);
        // BinaryTreeNode updated_bst = new BasiOpertaion();

        // Delete Node BST
        // BinaryTreeNode tree = new BasiOpertaion().deleteNodeBST(root, 2);
        // new BasiOpertaion().printBinaryTreePath(tree);

        // Find LCA Node BST
        // BinaryTreeNode lca = new BasiOpertaion().nodeLCA_BST(root2, new BinaryTreeNode(3), new BinaryTreeNode(7));
        // System.out.println("LCA : "+lca.getData());

        // Is Binary Search Tree
        // boolean isBST = new BasiOpertaion().isBST(root2);
        // boolean isBST = new BasiOpertaion().isBST(new BasiOpertaion().getBT1());
        // System.out.println("Is BST : "+isBST);

        // Convert BST to DLL
        // BasiOpertaion basicOps = new BasiOpertaion();
        // basicOps.convertBSTToDLL(root2);
        // basicOps.displayDLL();
        

        // Sorted array to BST
        // int a[] = new int[]{1,2,3,4,5,6,7,8,0,11,15};
        // Arrays.sort(a);
        // System.out.print("Sorted array : ");

        // for (int i : a) {
        //     System.out.print(i+" ");
        // }

        // System.out.println();

        // BinaryTreeNode sortedArrayBST = new BasiOpertaion().sortedArraysToBST(a, 0, a.length-1);
        // new BasiOpertaion().printBinaryTreePath(sortedArrayBST);

    
        // Kth Smallest element
        // new BasiOpertaion().kthSmallestElementBST(null, 5);
        // System.out.println("Kth smallest node : "+kthSmallestNode);

        // Print elements BTW range
        // new BasiOpertaion().printElementsInRangeBST(root, 1, 9);
        new BasiOpertaion().printElementsInRangeBSTNonRecursive(root, 1, 9);
    }
}