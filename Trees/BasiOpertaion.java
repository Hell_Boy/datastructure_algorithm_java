
/**
* BasiOpertaion
*/

/**
* Tree

1

2                        3

4          5           6           7

8    9    10    11    12    13    14    15
*/

import java.util.*;


public class BasiOpertaion {
    
    // public static void main(final String[] args) {
        // System.out.println("Hello world");
        
        // final BinaryTreeNode root = new BinaryTreeNode(1);
        
        // final BinaryTreeNode level_1_left = new BinaryTreeNode(2);
        // final BinaryTreeNode level_1_right = new BinaryTreeNode(3);
        
        // final BinaryTreeNode level_2_left_left = new BinaryTreeNode(4);
        // final BinaryTreeNode level_2_left_right = new BinaryTreeNode(5);
        // final BinaryTreeNode level_2_right_left = new BinaryTreeNode(6);
        // final BinaryTreeNode level_2_right_right = new BinaryTreeNode(7);
        // final BinaryTreeNode level_2_right_right = null;
        
        // BinaryTreeNode level_3_left_right_left = new BinaryTreeNode(8);
        // BinaryTreeNode level_3_left_right_right = new BinaryTreeNode(9);
        // BinaryTreeNode level_3_left_left_left = new BinaryTreeNode(10);
        // BinaryTreeNode level_3_left_left_right = new BinaryTreeNode(11);
        
        // BinaryTreeNode level_3_right_right_left = new BinaryTreeNode(12);
        // BinaryTreeNode level_3_right_right_right = new BinaryTreeNode(13);
        // BinaryTreeNode level_3_right_left_left = new BinaryTreeNode(14);
        // BinaryTreeNode level_3_right_left_right = new BinaryTreeNode(15);
        
        
        // root.setLeft(level_1_left);
        // root.setRight(level_1_right);
        
        // level_1_left.setLeft(level_2_left_left);       
        // level_1_left.setRight(level_2_left_right);
        // level_1_right.setLeft(level_2_right_left);
        // level_1_right.setRight(level_2_right_right);
        
        // level_2_left_left.setLeft(level_3_left_left_left);
        // level_2_left_left.setRight(level_3_left_left_right);
        // level_2_left_right.setLeft(level_3_left_right_left);
        // level_2_left_right.setRight(level_3_left_right_right);
        // level_2_right_left.setLeft(level_3_right_left_left);
        // level_2_right_left.setLeft(level_3_right_left_right);
        // level_2_right_right.setLeft(level_3_right_right_left);
        // level_2_right_right.setRight(level_3_right_right_right);
        
        
        
        // //  region Tree Traversal
        
        // // Pre Order Traversal (DLR)
        // System.out.println("===> PreOrder Traversal <===\n");
        // new BasiOpertaion().PreOrderTraversal(root);
        // System.out.println("\n=============> End <==============\n");
        
        //  // Post Order Traversal (LRD)
        //  System.out.println("===> PostOrder Traversal <===\n");
        //  new BasiOpertaion().PostOrderTraversal(root);
        //  System.out.println("\n=============> End <==============\n");
        
        // //  In Order Traversal (LDR)
        //  System.out.println("===> InOrder Traversal <===\n");
        //  new BasiOpertaion().InOrderTraversal(root);
        //  System.out.println("\n=============> End <==============\n");
        
        // //  Level Order Traversal 
        //  System.out.println("===> Level Order Traversal <===\n");
        //  new BasiOpertaion().LevelOrderTraversal(root);
        //  System.out.println("\n=============> End <==============\n");
        
        // //  endregion
        
        
        // Maximum Value in binary tree
        
        // Max with recursion
        // int maxValueInBinaryTree = new BasiOpertaion().maxInBinaryTree(root);
        // System.out.println(maxValueInBinaryTree);
        
        // // Max with loop
        // int maxValueInBinaryTreeUsingLoop = new BasiOpertaion().maxInBinaryTreeWithoutRecurrsion(root);
        // System.out.println(maxValueInBinaryTreeUsingLoop);
        
        // // Search an element in binary tree
        // boolean found = new BasiOpertaion().findInBT(root, 15);
        // System.out.println("Element present in BT : "+found);
        
        // // Search an element in binary tree non recursive
        // boolean found_1 = new BasiOpertaion().findInBTNonRecursive(root, 15);
        // System.out.println("Element present in BT : "+found_1);
        
        
        // Insert an element in binary tree insertToBinaryTreeLevelOrder
        // BinaryTreeNode new_root = new BasiOpertaion().insertToBinaryTreeLevelOrder(root, 35);
        //  System.out.println("===> Level Order Traversal <===\n");
        //  new BasiOpertaion().LevelOrderTraversal(new_root);
        //  System.out.println("\n=============> End <==============\n");
        
        // //  insertToBinaryTreeLevelOrder2
        // BinaryTreeNode new_root_1 = new BasiOpertaion().insertToBinaryTreeLevelOrder2(root, 35);
        //  System.out.println("===> Level Order Traversal <===\n");
        //  new BasiOpertaion().LevelOrderTraversal(new_root_1);
        //  System.out.println("\n=============> End <==============\n");
        
        
        
        //  Insert an element in binary tree Recursive Method 
        // BinaryTreeNode recursive_root = new BasiOpertaion().insertToBinaryTreeLevelOrderRecursive(root, 0);
        // System.out.println("===> Level Order Traversal <===\n");
        // new BasiOpertaion().LevelOrderTraversal(recursive_root);
        // System.out.println("\n=============> End <==============\n");
        
        // Tree size 
        // int treeSize = new BasiOpertaion().size(root);
        // int treeSize = new BasiOpertaion().sizeNonRecursive(root);
        // System.out.println("Tree size : "+treeSize);
        
        
        // Reverse Level Order Traversal
        // System.out.println("Reverse Order Traversal : ");
        // new BasiOpertaion().reverseLevelOrderTraversal(root);
        
        // Length Of Binary Tree
        // System.out.println("Length : "+ new BasiOpertaion().lengthOfBinaryTree(root));
        
        // Not working
        // System.out.println("Length : "+ new BasiOpertaion().heightOfBinaryTreeWithoutRecursion(root));
        
        // Deepest Node
        // BinaryTreeNode deepestNode = new BasiOpertaion().deepestNode(root);
        // System.out.println("Deepest Node : "+deepestNode.data);
        
        // Leaf Node count
        // int leafNodes = new BasiOpertaion().numberOfLeafNodes(root);
        // System.out.println("Leaf Nodes : "+leafNodes);
        
        // Full Node count
        // int fullNodesCount = new BasiOpertaion().numberOfFullNodes(root);
        // System.out.println("Full Nodes : "+fullNodesCount);
        
        // Half Node Count
        // int halfNodesCount = new BasiOpertaion().numberOfHalfNodes(root);
        // System.out.println("Half Nodes : "+halfNodesCount);
        
        // Print Tree path from root to leaf
        // new BasiOpertaion().printBinaryTreePath(root);
        
        // hasPathSum
        // boolean pathSumExists = new BasiOpertaion().hasPathSum(root, 8);
        // System.out.println("Has path sum : "+pathSumExists);
        
        // Tree node sum
        // int sum = new BasiOpertaion().addBT(root);
        // int sum = new BasiOpertaion().addBTNonRecursively(root);
        // System.out.println("Sum : "+sum );
        
        // Structurally Identical Tree check
        // BinaryTreeNode root1 = root;
        // BinaryTreeNode root2 =  new BinaryTreeNode(2);
        // root2.setRight(new BinaryTreeNode(5));
        
        // System.out.println("Tree 1 Size : "+ new BasiOpertaion().sizeNonRecursive(root1));
        // System.out.println("Tree 2 Size : "+ new BasiOpertaion().sizeNonRecursive(root2));
        
        // boolean structurallyIdenticalTree = new BasiOpertaion().structurallyIdenticalTree(root1, root2);
        
        // System.out.println("Trees are structurally equal : "+structurallyIdenticalTree);
        
        // Diameter of BT
        /**
        *                           7
        * 
        *                   8               28
        * 
        *              18       48      78       38
        * 
        *                   68       58
        */
        
        // BinaryTreeNode temp = new BinaryTreeNode(7);
        
        // temp.setLeft(new BinaryTreeNode(8));
        // temp.setRight(new BinaryTreeNode(28));
        
        // temp.getRight().setRight(new BinaryTreeNode(38));
        // temp.getRight().setLeft(new BinaryTreeNode(78));
        
        // temp.getLeft().setLeft(new BinaryTreeNode(18));
        // temp.getLeft().setRight(new BinaryTreeNode(48));
        // temp.getLeft().getRight().setRight(new BinaryTreeNode(58));
        // temp.getLeft().getRight().setLeft(new BinaryTreeNode(68));
        
        // int diameterOfBT = new BasiOpertaion().diameterOfBT(temp);
        // System.out.println("Diameter of BT : "+diameterOfBT);
        
        
        // Max Sum at any level of Binary Tree
        // int maxSum = new BasiOpertaion().levelMaxSumOfBT(root);
        // System.out.println("Maximum possible sum : "+maxSum);4
        
        // Print all paths
        // new BasiOpertaion().printPathsOfBT(root);
        
        // Mirror binary tree
        // initial tree
        // new BasiOpertaion().printPathsOfBT(root);
        // System.out.println("Mirror");
        // new BasiOpertaion().printPathsOfBT(new BasiOpertaion().mirror(root));
        
        // Check if mirror binary tree
        
        //     BinaryTreeNode temp = new BinaryTreeNode(7);
        
        //     temp.setLeft(new BinaryTreeNode(8));
        //     temp.setRight(new BinaryTreeNode(28));
        
        //     temp.getRight().setRight(new BinaryTreeNode(38));
        //     temp.getRight().setLeft(new BinaryTreeNode(78));
        
        //     temp.getLeft().setLeft(new BinaryTreeNode(18));
        //     temp.getLeft().setRight(new BinaryTreeNode(48));
        //     temp.getLeft().getRight().setRight(new BinaryTreeNode(58));
        //     temp.getLeft().getRight().setLeft(new BinaryTreeNode(68));
        
        //     System.out.println("Are mirror : "+ new BasiOpertaion().areMirror(root, temp));
        
        
        // }
        
        public BinaryTreeNode getBT1(){
            final BinaryTreeNode root = new BinaryTreeNode(1);
            
            final BinaryTreeNode level_1_left = new BinaryTreeNode(2);
            final BinaryTreeNode level_1_right = new BinaryTreeNode(3);
            
            final BinaryTreeNode level_2_left_left = new BinaryTreeNode(4);
            final BinaryTreeNode level_2_left_right = new BinaryTreeNode(5);
            final BinaryTreeNode level_2_right_left = new BinaryTreeNode(6);
            final BinaryTreeNode level_2_right_right = new BinaryTreeNode(7);
            
            root.setLeft(level_1_left);
            root.setRight(level_1_right);
            
            level_1_left.setLeft(level_2_left_left);       
            level_1_left.setRight(level_2_left_right);
            level_1_right.setLeft(level_2_right_left);
            level_1_right.setRight(level_2_right_right);
            
            return root;
        }
        
        
        public BinaryTreeNode getBT2(){
            BinaryTreeNode temp = new BinaryTreeNode(7);
            
            temp.setLeft(new BinaryTreeNode(8));
            temp.setRight(new BinaryTreeNode(28));
            
            temp.getRight().setRight(new BinaryTreeNode(38));
            temp.getRight().setLeft(new BinaryTreeNode(78));
            
            temp.getLeft().setLeft(new BinaryTreeNode(18));
            temp.getLeft().setRight(new BinaryTreeNode(48));
            temp.getLeft().getRight().setRight(new BinaryTreeNode(58));
            temp.getLeft().getRight().setLeft(new BinaryTreeNode(68));
            
            return temp;
        }
        
        public void PreOrderTraversal(final BinaryTreeNode root){
            
            if(root != null){
                System.out.print(root.data+" ");
                PreOrderTraversal(root.getLeft());
                System.out.println();
                PreOrderTraversal(root.getRight());
                System.out.println();
            }
        }
        
        public void PostOrderTraversal(final BinaryTreeNode root){
            if(root != null){
                PostOrderTraversal(root.getLeft());
                System.out.println();
                PostOrderTraversal(root.getRight());
                System.out.print(root.data+" ");
                
            }
        }
        
        public void InOrderTraversal(final BinaryTreeNode root){
            
            if(root != null){
                
                InOrderTraversal(root.left);
                System.out.print(root.data+" ");
                InOrderTraversal(root.right);
                System.out.println();
            }
        }
        
        public void LevelOrderTraversal(final BinaryTreeNode root){
            
            if(root == null){
                System.out.println("Tree is empty.");
                return;
            }
            
            final Queue<BinaryTreeNode> queue = new  LinkedList<BinaryTreeNode>();
            BinaryTreeNode tempNode = root;
            
            while(tempNode != null){
                
                System.out.print(tempNode.getData()+" ");
                
                
                queue.offer(tempNode.getLeft());
                queue.offer(tempNode.getRight());
                
                tempNode = queue.poll();
                
                // System.out.println();
            }
            
        }
        
        public int maxInBinaryTree(final BinaryTreeNode root){
            
            int maxValue = Integer.MIN_VALUE;
            
            if(root != null){
                
                final int leftMaxVal = maxInBinaryTree(root.getLeft());
                final int rightMaxVal = maxInBinaryTree(root.getRight());
                
                if(leftMaxVal > rightMaxVal)
                maxValue = leftMaxVal;
                else
                maxValue = rightMaxVal;
                
                if(root.data > maxValue)
                maxValue = root.data;
                
            }
            
            return maxValue;
        }
        
        public int maxInBinaryTreeWithoutRecurrsion(final BinaryTreeNode root){
            
            int maxVal = Integer.MIN_VALUE;
            
            BinaryTreeNode temp = root;
            
            final Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
            
            while(temp != null){
                
                if(maxVal < temp.data)
                maxVal = temp.data;
                
                queue.offer(temp.getLeft());
                queue.offer(temp.getRight());
                
                temp = queue.poll();
            }
            
            return maxVal;
        }
        
        public boolean findInBT(final BinaryTreeNode root, final int data){
            
            if(root == null)
            return false;
            
            if(root.data == data)
            return true;
            
            return findInBT(root.getLeft(), data) || findInBT(root.getRight(), data);
        }
        
        public boolean findInBTNonRecursive(final BinaryTreeNode root, final int data){
            
            BinaryTreeNode temp = root;
            
            if(temp == null)
            return false;
            
            final Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
            
            while(temp != null){
                
                if(temp.getData() == data)
                return true;
                
                queue.offer(temp.left);
                queue.offer(temp.right);
                
                temp = queue.poll();
            }
            return false;
        }
        
        public BinaryTreeNode insertToBinaryTreeLevelOrder(final BinaryTreeNode root, final int data){
            
            if(root == null)
            return null;
            
            final Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
            
            BinaryTreeNode temp = root;
            
            while(temp !=null){
                
                // System.out.println(temp.getData());
                
                queue.offer(temp.getLeft());
                queue.offer(temp.getRight());
                temp = queue.poll();
                
                if(temp.getLeft() == null){
                    
                    temp.setLeft(new BinaryTreeNode(data));
                    break;
                }else if(temp.getRight() == null){
                    temp.setRight(new BinaryTreeNode(data));
                    break;
                }
            }
            
            return root;
        }
        
        public BinaryTreeNode insertToBinaryTreeLevelOrder2(final BinaryTreeNode root, final int data){
            
            if(root == null)
            return null;
            
            final Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
            queue.offer(root);
            
            
            while(!queue.isEmpty()){
                
                final BinaryTreeNode temp = queue.poll();
                
                if(temp != null){
                    if(temp.getLeft() !=null){
                        
                        queue.offer(temp.getLeft());
                        
                    }else{
                        temp.setLeft(new BinaryTreeNode(data));
                        return root;
                    }
                    
                    if(temp.getRight() !=null){
                        queue.offer(temp.getRight());
                    }else{
                        temp.setRight(new BinaryTreeNode(data));
                        return root;
                    }
                }
                
            }
            
            return root;
        }
        
        public BinaryTreeNode insertToBinaryTreeLevelOrderRecursive(BinaryTreeNode root, final int data){
            
            if(root == null){
                root = new BinaryTreeNode(data);
            }else{
                insertHelper(root, data);
            }
            
            return root;
        }
        
        public void insertHelper(final BinaryTreeNode root, final int data){
            
            if(root.getData() >= data){
                
                if(root.getLeft() != null){
                    
                    insertHelper(root.getLeft(), data);
                }else {
                    root.setLeft(new BinaryTreeNode(data));
                }
                
            }else{
                
                if(root.getRight() != null){
                    
                    insertHelper(root.getRight(), data);
                }else {
                    root.setRight(new BinaryTreeNode(data));
                }
            }
        }
        
        public int size(final BinaryTreeNode root){
            
            
            if(root == null){
                return 0;
            }
            
            final int leftCount = root.getLeft() == null ? 0 : size(root.getLeft());
            final int rightCount = root.getRight() == null  ? 0 : size(root.getRight());
            
            return 1 + leftCount + rightCount;
        }
        
        public int sizeNonRecursive(final BinaryTreeNode root){
            
            int count = 0;
            
            if(root == null)
            return count;
            
            final Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
            queue.offer(root);
            
            while(!queue.isEmpty()){
                
                final BinaryTreeNode temp = queue.poll();
                
                count++;
                
                if(temp.getLeft() != null){
                    queue.offer(temp.getLeft());
                }
                
                if(temp.getRight() != null){
                    queue.offer(temp.getRight());
                }
            }
            
            return count;
        }
        
        public void reverseLevelOrderTraversal(final BinaryTreeNode root){
            
            if (root == null){
                return;
            }
            
            final Stack<BinaryTreeNode> stack = new Stack<BinaryTreeNode>();
            
            final Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
            queue.offer(root);
            
            while(!queue.isEmpty()){
                
                final BinaryTreeNode temp = queue.poll();
                
                stack.push(temp);
                
                if(temp.getLeft() != null){
                    queue.offer(temp.getLeft());
                }
                
                if(temp.getRight() != null){
                    queue.offer(temp.getRight());
                }
                
            }
            
            while(!stack.isEmpty()){
                System.out.println("Print elem : "+stack.pop().getData());
            }
        }
        
        public int lengthOfBinaryTree(final BinaryTreeNode root){
            
            if(root == null){
                return -1;
            }
            
            final int leftCount = lengthOfBinaryTree(root.getLeft());
            final int rightCount = lengthOfBinaryTree(root.getRight());
            
            return (leftCount > rightCount) ? leftCount+1 : rightCount+1;
            
        }
        
        // Not working
        public int heightOfBinaryTreeWithoutRecursion(final BinaryTreeNode root){
            
            int count = 0;
            
            if(root == null)
            return count;
            
            count++;
            
            final Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
            queue.offer(root);
            queue.offer(null);
            
            while(!queue.isEmpty()){
                
                final BinaryTreeNode currentNode = queue.poll();
                
                if(currentNode !=null){
                    
                    if(currentNode.getLeft() == null && currentNode.getRight() == null)
                    return count;
                    
                    if(currentNode.getLeft() != null){
                        queue.offer(currentNode.getLeft());
                    }
                    
                    if(currentNode.getRight() != null){
                        queue.offer(currentNode.getRight());
                    }
                    
                }else{
                    
                    // if(!queue.isEmpty()){
                        count++;
                        // queue.offer(null);
                        // }
                    }
                }
                
                return count;
            }
            
            
            public BinaryTreeNode deepestNode(final BinaryTreeNode root) {
                
                if(root == null)
                return null;
                
                final Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
                queue.offer(root);
                
                BinaryTreeNode temp = null;
                
                while (!queue.isEmpty()) {
                    
                    
                    temp = queue.poll();
                    
                    if(temp.getLeft() !=null)
                    queue.offer(temp.getLeft());
                    
                    if(temp.getRight() !=null)
                    queue.offer(temp.getRight());
                }
                
                return temp;
                
            }
            
            public int numberOfLeafNodes(final BinaryTreeNode root){
                
                if(root == null)
                return 0;
                
                int leafNodes = 0;
                
                final Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
                queue.offer(root);
                
                while (!queue.isEmpty()){
                    
                    final BinaryTreeNode temp = queue.poll();
                    
                    if(temp.getLeft() == null && temp.getRight() == null)
                    leafNodes++;
                    
                    if(temp.getLeft() != null)
                    queue.offer(temp.getLeft());
                    
                    if(temp.getRight() != null)
                    queue.offer(temp.getRight());
                }
                
                
                return leafNodes;
            }
            
            public int numberOfFullNodes(final BinaryTreeNode root){
                
                if(root == null)
                return 0;
                
                int fullNodesCount = 0;
                
                final Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
                queue.offer(root);
                
                while(!queue.isEmpty()){
                    
                    final BinaryTreeNode temp = queue.poll();
                    
                    if(temp.getLeft() != null && temp.getRight() != null ){
                        fullNodesCount++;
                    }
                    
                    if(temp.getLeft() != null)
                    queue.offer(temp.getLeft());
                    
                    if(temp.getRight() != null)
                    queue.offer(temp.getRight());
                }
                
                return fullNodesCount;
            }
            
            
            public int numberOfHalfNodes(final BinaryTreeNode root){
                
                if(root == null)
                return 0;
                
                int halfNodesCount = 0;
                
                final Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
                queue.offer(root);
                
                while (!queue.isEmpty()){
                    
                    final BinaryTreeNode temp = queue.poll();
                    
                    if( (temp.getLeft() == null && temp.getRight() != null) ||
                    (temp.getRight() == null && temp.getLeft() != null)  
                    ){
                        halfNodesCount++;
                    }
                    
                    if(temp.getLeft() != null)
                    queue.offer(temp.getLeft());
                    
                    if(temp.getRight() != null)
                    queue.offer(temp.getRight());
                }
                return halfNodesCount;
            }
            
            // region Print binary tree from it's root-to-leaf
            
            public void printBinaryTreePath(final BinaryTreeNode root){
                final int[] path = new int[256];
                
                printPath(root,path, 0);
            }
            
            public void printPath(final BinaryTreeNode root, final int[] path, int pathLen){
                
                if(root == null)
                return;
                
                path[pathLen] = root.getData();
                pathLen++;
                
                if(root.getLeft() == null && root.getRight() == null){
                    printArray(path, pathLen);
                }else{
                    
                    printPath(root.getLeft(), path, pathLen);
                    printPath(root.getRight(), path, pathLen);
                }
            }
            
            public void printArray(final int[] path, final int len){
                
                // for (int data : path) {
                    //     System.out.print(data+" --> ");
                    // }
                    
                    for (int i = 0; i < len; i++) {
                        System.out.print(path[i]+" --> ");
                    }
                    
                    System.out.println("null");
                }
                
                // endregion
                
                public boolean hasPathSum(final BinaryTreeNode root, final int sum){
                    
                    if(root == null)
                    return false;
                    
                    if(root.getLeft() == null && root.getRight() == null && root.getData() == sum)
                    return true;
                    
                    return hasPathSum(root.getLeft(), sum-root.getData()) || hasPathSum(root.getRight(), sum-root.getData());
                    
                }
                
                
                public int addBT(final BinaryTreeNode root){
                    
                    if(root == null)
                    return 0;
                    
                    return (root.getData() + addBT(root.getLeft())+ addBT(root.getRight()));
                }
                
                
                public int addBTNonRecursively(final BinaryTreeNode root){
                    
                    if(root == null)
                    return 0;
                    
                    final Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
                    queue.offer(root);
                    
                    int sum = 0;
                    while(!queue.isEmpty()){
                        
                        final BinaryTreeNode temp =  queue.poll();
                        
                        sum += temp.getData();
                        
                        if(temp.getLeft() != null)
                        queue.offer(temp.getLeft());
                        
                        if(temp.getRight() != null)
                        queue.offer(temp.getRight());
                        
                    }
                    
                    return sum;
                }
                
                public boolean structurallyIdenticalTree(final BinaryTreeNode root1, final BinaryTreeNode root2){
                    
                    if(root1 == null && root2 == null)
                    return true;
                    
                    if(root1 == null || root2 == null)
                    return false;
                    
                    return structurallyIdenticalTree(root1.getLeft(), root2.getLeft()) && structurallyIdenticalTree(root1.getRight(), root2.getRight());
                }
                
                
                public int diameterOfBT(final BinaryTreeNode root){
                    
                    if(root == null)
                    return 0;
                    
                    final int len1 = heightOfBinaryTreeWithoutRecursion(root.getLeft()) + heightOfBinaryTreeWithoutRecursion(root.getRight()) +1;
                    
                    final int len2 = Math.max(diameterOfBT(root.getLeft()), diameterOfBT(root.getRight()));
                    
                    return Math.max(len1, len2);
                }
                
                public int levelMaxSumOfBT(final BinaryTreeNode root){
                    
                    if(root == null)
                    return 0;
                    
                    final Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
                    queue.offer(root);
                    
                    int maxSum = root.getData();
                    
                    while(!queue.isEmpty()){
                        
                        int count = queue.size();
                        int currentSum = 0;
                        
                        while(count>0){
                            
                            final BinaryTreeNode temp = queue.poll();
                            
                            currentSum += temp.getData();
                            
                            if(temp.getLeft() != null){
                                queue.offer(temp.getLeft());
                            }
                            
                            if(temp.getRight() != null){
                                queue.offer(temp.getRight());
                            }
                            
                            count--;
                        }
                        
                        maxSum = Math.max(currentSum, maxSum);
                    }
                    
                    return maxSum;
                }
                
                Stack<BinaryTreeNode> stack = new Stack<BinaryTreeNode>();
                
                public void printPathsOfBT(final BinaryTreeNode root){
                    
                    if(root == null)
                    return;        
                    
                    
                    stack.push(root);
                    
                    printPathsOfBT(root.getLeft());
                    
                    if(root.getLeft() == null && root.getRight() == null){
                        Stack<BinaryTreeNode> stack2 = new Stack<BinaryTreeNode>();
                        stack2.addAll(stack);
                        
                        stack2 = reverseStack(stack2);
                        
                        System.out.print("Path : ");
                        while (!stack2.isEmpty()) {
                            
                            System.out.print(stack2.pop().getData()+" ");
                        }
                        
                        System.out.println();
                    }
                    
                    printPathsOfBT(root.getRight());
                    
                    stack.pop();
                    
                }
                
                public Stack<BinaryTreeNode> reverseStack(final Stack<BinaryTreeNode> stack){
                    
                    final Stack<BinaryTreeNode> temp = new Stack<BinaryTreeNode>();
                    
                    while(!stack.isEmpty())
                    temp.push(stack.pop());
                    
                    return temp;
                }
                
                public BinaryTreeNode mirror(BinaryTreeNode root){
                    
                    if(root != null){
                        mirror(root.getLeft());
                        mirror(root.getRight());
                        
                        BinaryTreeNode temp = root.getLeft();
                        root.setLeft(root.getRight());
                        root.setRight(temp);
                    }
                    
                    return root;
                }
                
                public boolean areMirror(BinaryTreeNode root1, BinaryTreeNode root2){
                    
                    if(root1 == null && root2 == null)
                    return true;
                    
                    if(root1 == null || root2 == null)
                    return false;
                    
                    if(root1.getData() != root2.getData())
                    return false;
                    
                    return areMirror(root1.getLeft(), root2.getLeft()) && areMirror(root1.getRight(), root2.getRight());
                }
                
                
                ///////////////////////////////////////////////// Binary Search Tree Operations //////////////////////////////////////////////////////////////////////////
                
                public BinaryTreeNode getBST1(){
                    
                    /**
                    * Image url : https://miro.medium.com/max/1424/1*F8MmBnUQyOA8-Rajg69nSQ.png
                    */
                    BinaryTreeNode root = new BinaryTreeNode(7);
                    root.setLeft(new BinaryTreeNode(2));
                    root.setRight(new BinaryTreeNode(9));
                    
                    root.getLeft().setLeft(new BinaryTreeNode(1));
                    root.getLeft().setRight(new BinaryTreeNode(5));
                    
                    root.getRight().setRight(new BinaryTreeNode(14));
                    
                    return root;
                }
                
                
                public BinaryTreeNode getBST2(){
                    /**
                    * Image url : https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQRbn2us_QuI5Cflh1brbxl_NsyxYWQ3atIFshB5mn_Wzm__2TT&usqp=CAU
                    */
                    
                    BinaryTreeNode root = new BinaryTreeNode(9);
                    
                    root.setLeft(new BinaryTreeNode(4));
                    root.setRight(new BinaryTreeNode(17));
                    
                    root.getRight().setRight(new BinaryTreeNode(22));
                    root.getRight().getRight().setLeft(new BinaryTreeNode(20));
                    
                    root.getLeft().setLeft(new BinaryTreeNode(3));
                    root.getLeft().setRight(new BinaryTreeNode(6));
                    root.getLeft().getRight().setLeft(new BinaryTreeNode(5));
                    root.getLeft().getRight().setRight(new BinaryTreeNode(7));
                    
                    return root;
                }
                
                public BinaryTreeNode findElementBST(BinaryTreeNode root, int data){
                    
                    if(root == null)
                    return null;
                    
                    if(root.getData() > data)
                    return findElementBST(root.getLeft(), data);
                    
                    if(root.getData() < data)
                    return findElementBST(root.getRight(), data);
                    
                    return root;                   
                }
                
                public BinaryTreeNode findElementBSTNonRecursive(BinaryTreeNode root, int data){
                    
                    if(root == null)
                    return null;
                    
                    BinaryTreeNode temp = root;
                    
                    while(temp !=null){
                        
                        if(data == temp.getData())
                        break;
                        
                        if(data > temp.getData())
                        temp = temp.getRight();
                        
                        else
                        temp = temp.getLeft();
                        
                    }
                    
                    // while(temp != null){
                        
                        //     if(data == root.getData())
                        //     break;
                        
                        //     else if(data > root.getData())
                        //     temp = temp.getRight();
                        
                        //     else 
                        //     temp = temp.getLeft();
                        // }
                        
                        return temp;
                    }
                    
                    public BinaryTreeNode findMinBST(BinaryTreeNode root){
                        
                        if(root == null)
                        return null;
                        
                        else if (root.getLeft() == null)
                        return root;
                        
                        else
                        return findMinBST(root.getLeft());
                    }
                    
                    public BinaryTreeNode findMinBSTNonRecursive(BinaryTreeNode root){
                        
                        if(root == null)
                        return null;
                        
                        while(root != null){
                            
                            if(root.getLeft() == null)
                            break;
                            
                            else
                            root = root.getLeft();
                        }
                        
                        return root;
                    }
                    
                    
                    public BinaryTreeNode findMaxBST(BinaryTreeNode root){
                        
                        if(root == null)
                        return null;
                        
                        if(root.getRight() == null)
                        return root;
                        
                        else
                        return findMaxBST(root.getRight());
                    }
                    
                    public BinaryTreeNode findMaxBSTNonRecursive(BinaryTreeNode root){
                        
                        if(root == null)
                        return null;
                        
                        while(root != null){
                            
                            if(root.getRight() == null)
                            break;
                            
                            else
                            root = root.getRight();
                        }
                        
                        return root;
                    }
                    
                    public BinaryTreeNode insertNodeBST(BinaryTreeNode root, int data){
                        
                        if(root == null)
                        return new BinaryTreeNode(data);
                        
                        else{
                            if(data > root.getData())
                            root.setRight(insertNodeBST(root.getRight(), data));
                            else
                            root.setLeft(insertNodeBST(root.getLeft(), data));
                        }
                        
                        return root;
                    }
                    
                    
                    
                    public BinaryTreeNode deleteNodeBST(BinaryTreeNode root, int data){
                        
                        if(root == null)
                        return root;
                        
                        if(data > root.getData())
                        root.setRight(deleteNodeBST(root.getRight(), data));
                        
                        else if( data < root.getData())
                        root.setLeft(deleteNodeBST(root.getLeft(), data));
                        
                        else{
                            
                            // Deleting node with two child
                            
                            System.out.println("Element : "+root.getData());
                            
                            if(root.getLeft() !=null && root.getRight() != null ){
                                
                                int max_in_left_bst = maxInBinaryTree(root.getLeft());
                                root.setData(max_in_left_bst);
                                root.setLeft(deleteNodeBST(root.getLeft(), max_in_left_bst));
                            }else{
                                
                                // Deleting node with single child
                                
                                if(root.getLeft() == null && root.getRight() != null)
                                root = root.getRight();
                                
                                else if(root.getRight() == null && root.getLeft() != null)
                                root = root.getLeft();
                                
                                else
                                root = null;
                            }
                        }
                        
                        
                        return root;
                    }
                    
                    
                    public BinaryTreeNode nodeLCA_BST(BinaryTreeNode root, BinaryTreeNode a, BinaryTreeNode b){
                        
                        if(root == null)
                        return null;
                        
                        if(Math.max(a.data, b.data) < root.data)
                        return nodeLCA_BST(root.getLeft(), a, b);
                        
                        else if(Math.min(a.data, b.data) > root.data)
                        return nodeLCA_BST(root.getRight(), a, b);
                        
                        else
                        return root;
                        
                    }
                    
                    public boolean isBST(BinaryTreeNode root){
                        
                        if(root == null)
                        return true;
                        
                        if(root.getLeft() != null && maxInBinaryTree(root.getLeft()) > root.getData())
                        return false;
                        
                        if(root.getRight() != null && maxInBinaryTree(root.getRight()) < root.getData())
                        return false;
                        
                        if(!isBST(root.getLeft()) || !isBST(root.getRight()))
                        return false;
                        
                        return true;
                        
                    }


                    // region Convert BST to DLL

                    BinaryTreeNode head, tail = null;
                    // Using in order traversal of BST

                    public void convertBSTToDLL(BinaryTreeNode root){

                        if(root == null)
                        return;

                        convertBSTToDLL(root.getLeft());

                        if(head == null)
                        // Both head and tail will point to root node
                        head = tail = root;

                        else {
                            tail.setRight(root);
                            root.setLeft(tail);

                            tail = root;
                        }

                        convertBSTToDLL(root.getRight());
                    }

                    public void displayDLL(){

                        if(head == null){

                            System.out.println("List is empty.");
                            return;
                        }

                        BinaryTreeNode current = head;

                        System.out.print("DLL List : head");
                        while(current != null){

                            System.out.print(" <==> "+current.getData());
                            current = current.getRight();
                        }

                        System.out.println(" <==> null");
                    }

                    // endregion;

                    public BinaryTreeNode sortedArraysToBST(int a[], int start , int end){
                        BinaryTreeNode node;

                        if(start > end)
                        return null;
                        
                        else{
                            if(start != end){
                                int midPoint = start + (end-start)/2;
                                node = new BinaryTreeNode(a[midPoint]);
                                node.setLeft(sortedArraysToBST(a, start, midPoint-1));
                                node.setRight(sortedArraysToBST(a, midPoint+1, end));
                            }else{
                                node = new BinaryTreeNode(a[start]);
                            }
                        }

                        return node;
                    }


                    public int kthSmallestElementBST(BinaryTreeNode root, int k){

                        if(root == null)
                        return k;

                        else{

                            k = kthSmallestElementBST(root.getLeft(), k);
                            k--;
                            
                            if(k == 0)
                            System.out.println("Kth smallest element : "+root.getData());
                             
                            k = kthSmallestElementBST(root.getRight(), k);

                            return k;
                        }
                    }

                    public void printElementsInRangeBST(BinaryTreeNode root, int min, int max){

                        if(root == null) return;

                        if(root.getData() >= min)
                        printElementsInRangeBST(root.getLeft(), min, max);

                        if(root.getData() >= min && root.getData() <= max)
                        System.out.println(root.getData());

                        if(root.getData() <= max)
                        printElementsInRangeBST(root.getRight(), min, max);
                    }

                    public void printElementsInRangeBSTNonRecursive(BinaryTreeNode root, int min, int max){

                        if(root == null) return;

                        Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
                        queue.offer(root);

                        while (!queue.isEmpty()){

                            BinaryTreeNode temp = queue.poll();

                            if(temp.getData() >= min && temp.getData() <= max)
                            System.out.println(temp.getData());

                            if(temp.getLeft() != null && temp.getData() >= min)
                            queue.offer(temp.getLeft());

                            if(temp.getRight() != null && temp.getData() <= max)
                            queue.offer(temp.getRight());
                        }

                    }

                }