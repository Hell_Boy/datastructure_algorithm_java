import java.util.*;

class InfixToPostfix{

	public static void main(String z[]){

		System.out.println("Hello world");

		// String input = "(A+B/C+(D+E)-F)";

		String input = "K+L-M*N+(O^P)*W/U/V*T+Q";

		// String input = "a+b*(c^d-e)^(f+g*h)-i"; 

		// abcd^e-fgh*+^*+i-
		// ab(cde-^)*+(fgh*+)i


		char[] chars = input.toCharArray();

		System.out.println(Arrays.toString(chars));

		Stack<Character> charStack = new Stack<Character>();

		int i = 0;

		// charStack.push(chars[0]);

		while(i < chars.length){
			// System.out.print(chars[i]);

			if(chars[i] == '('){
				System.out.print(chars[i]);
				charStack.push(chars[i]);
				++i;
			}

			else if(chars[i] == ')' && !charStack.isEmpty()){

				char top = charStack.pop();

				while(top != '('){

					System.out.print(top);
					top = charStack.pop();
				}
				System.out.print(chars[i]);
				++i;

			}

			else if(chars[i] == '^'){

				if( !charStack.isEmpty() && charStack.peek() == '^')
					System.out.print(charStack.pop());
				else{
					charStack.push(chars[i]);
					++i;
				}
			}

			else if(chars[i] == '*' || chars[i] == '/'){

				if( !charStack.isEmpty() && (charStack.peek() =='^' || charStack.peek() =='*' || charStack.peek() =='/')){
					System.out.print(charStack.pop());
				}
				else{
					charStack.push(chars[i]);
					++i;
				}
				
			}

			else if(chars[i] == '+' || chars[i] == '-'){
				if( !charStack.isEmpty() && (charStack.peek() =='^' || charStack.peek() =='+' || charStack.peek() =='-' || charStack.peek() =='*' || charStack.peek() =='/')){
					System.out.print(charStack.pop());
				}
				else{
					charStack.push(chars[i]);
					++i;
				}

			}

			else{
				System.out.print(chars[i]);
				++i;
			}
		}

		while(!charStack.isEmpty()){
			System.out.print(charStack.pop());
		}

		System.out.println();
	}
}