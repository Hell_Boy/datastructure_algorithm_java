import java.util.*;

class ZeroOneKnapSackProblem{

	public static void main(String z[]){

		System.out.println("Zero One Knapsack Problem");

		int capacity = 7;

		int[] wts ={2,3,4,5};

		int[] val = {1,2,5,6};		


		int[][] t = new int[wts.length+1][capacity+1];

		// for(int i=0; i<7; ++i){

		// 	for(int j =0; j< wts.length+1; ++j){
		// 		if(i ==0 || j ==0)
		// 			t[i][j] = 0;

		// 		else if(wts[j-1] < i){
		// 			t[i][j] = Math.max( (val[j-1]+t[i-1][i-wts[j-1]]) , t[i-1][j]);
		// 		}else{
		// 			t[i][j] = t[i-1][j];
		// 		}
		// 	}
		// }

		for(int i =0; i < wts.length+1; ++i)
			for(int j =0; j<capacity+1; ++j){

				if(i==0 || j ==0)
					t[i][j] = 0;

				else if (i == 1)
					t[i][j] = val[i-1];

				else if (wts[i-1] < j){
					t[i][j] = Math.max((val[i-1]+t[i-1][j-wts[i-1]]), t[i-1][j]);
				}
				else
					t[i][j] = t[i-1][j];
			}

		for(int[] a : t)
			System.out.println(Arrays.toString(a));
	}
}