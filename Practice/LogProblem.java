import java.util.*;
import java.lang.*;

class LogProblem{

	public static void main(String z[]){

		System.out.println("\nLog problem");

		double u = 687078.1;
		double v = 0;
		double w = -497.49;

		// for u
		String value = String.format("Math.log(%f)=(%f)", u, Math.log(u));
		System.out.println(value);

		// for v
		value = String.format("Math.log(%f)=(%f)", v, Math.log(v));
		System.out.println(value);

		// for w
		value = String.format("Math.log(%f)=(%f)", w, Math.log(w));
		System.out.println(value);
		System.out.println();

		// for ceil
		System.out.println("Ceil value");

		// for u
		value = String.format("Math.ceil(%f)=(%f)", u, Math.ceil(u));
		System.out.println(value);

		// for v
		value = String.format("Math.ceil(%f)=(%f)", v, Math.ceil(v));
		System.out.println(value);

		// for w
		value = String.format("Math.ceil(%f)=(%f)", w, Math.ceil(w));
		System.out.println(value);
		System.out.println();

		// for floor
		System.out.println("Floor value");

		// for u
		value = String.format("Math.floor(%f)=(%f)", u, Math.floor(u));
		System.out.println(value);

		// for v
		value = String.format("Math.floor(%f)=(%f)", v, Math.floor(v));
		System.out.println(value);

		// for w
		value = String.format("Math.floor(%f)=(%f)", w, Math.floor(w));
		System.out.println(value);
		System.out.println();

		// for round
		System.out.println("Round value");

		// for u
		value = String.format("Math.round(%f)=(%d)", u, Math.round(u));
		System.out.println(value);

		// for v
		value = String.format("Math.round(%f)=(%d)", v, Math.round(v));
		System.out.println(value);

		// for w
		value = String.format("Math.round(%f)=(%d)", w, Math.round(w));
		System.out.println(value);
		System.out.println();

		

	}
}