import java.util.*;

class MergeSortedArray{

	public static void main(String z[]){

		System.out.println("Merge Sorted arrays");

		int[] a = {2,3,4,5,63,2,3,1,4};
		int[] b = {2,4,5,1,6,6,3,7,8};

		Arrays.sort(a);
		Arrays.sort(b);

		int[] c = new int[(a.length+b.length)];

		int i = 0, j = 0, k = 0;


		while( i < a.length && j < b.length){

			if(a[i] < b[j]){
				c[k] = a[i];
				++i;
			}else{
				c[k] = b[j];
				++j;
			}

			++k;
		} 

		while(i < a.length){
			c[k] = a[i];
			++i;
			++k;
		}

		while(j < b.length){
			c[k] = b[j];
			++j;
			++k;
		}

		System.out.println("Merged sorted arrays : "+Arrays.toString(c));
	}
}