
class IncrementProblem{

	public static void main(String z[]){

		int a = 200;

		String result = "";

		result = String.format("%s + %s = %d", "++a", "++a", (++a + ++a));
		System.out.println(result);

		a = 200;

		result = String.format("%s + %s = %d", "a++", "a++", (a++ + a++));
		System.out.println(result);

		a = 200;


		result = String.format("%s + %s = %d", "++a", "a++", (++a + a++));
		System.out.println(result);

		a = 200;	

		result = String.format("%s + %s = %d", "a++", "++a", (a++ + ++a));
		System.out.println(result);

	}
}