import java.util.*;

class TwoSum{

	public static void main(String z[]){

		System.out.println("Two Sum ...");

		int target = 47;
		int[] elements = {1, 1, 2, 45, 46, 46};

		Arrays.sort(elements);

		int start = 0, end = elements.length-1;

		while(start < end){

			int temp = target - elements[start];

			if(temp < elements[end])
				++start;
			else if(temp > elements[end])
				--end;
			else{

				System.out.println("Start : "+elements[start]+ " end : "+elements[end]);
				++start;
				--end;
			}
		}
	}
}