import java.util.*;

class ClassComparatorPractice{

	public static void main(String z[]){

		System.out.println("Hello world");

		List<Job> jobs = new ArrayList<Job>();

		jobs.add(new Job(1,5,5));
		jobs.add(new Job(5,8,3));
		jobs.add(new Job(9,12,3));
		jobs.add(new Job(13,16,3));
		jobs.add(new Job(17,22,5));
		jobs.add(new Job(23,25,2));

		for(Job j : jobs){
			System.out.println(j.start);
		}

		// Sort by duration

		System.out.println("Sorted By Duration");

		Collections.sort(jobs, (j1,j2)-> j2.duration > j1.duration ? 1 : -1);

		for(Job j : jobs){
			System.out.println(j.duration);
		}


	}
}

class Job{

	int start;
	int end;
	int duration;

	public Job(int start, int end, int duration){
		this.start = start;
		this.end = end;
		this.duration = duration;
	}
}