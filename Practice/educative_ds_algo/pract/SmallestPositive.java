
import java.util.Arrays;

class SmallestPositive{

	public static void main(String z[]){

		// int[] a = {0,10,2,-10,-20};
		int[] a = { 1, 4, 2, -1, 6, 5 };

		SmallestPositive sp = new SmallestPositive();

		int firstPositive = sp.partition(a);

		System.out.println(Arrays.toString(a));

		System.out.println("First positive : "+firstPositive);

		int missing = sp.findMissing(a, firstPositive);

		System.out.println("First positive missing : "+missing);
	}

	private int findMissing(int[] a , int start){


		for(int i = start; i < a.length; ++i){

			int val = Math.abs(a[i]);

			if(val > 0 && val < a.length-1 && a[val] > 0)
				a[a[val]] = -a[val];
		}

		System.out.println(Arrays.toString(a));

		for(int i = start; i < a.length; ++i)
			if(a[i] > 0)
				return start-1;

		return -1;
	}

	private int partition(int[] a){

		int i = 0, prev = 0;

		while(i < a.length){

			if(a[i] < 0){

				int temp = a[prev];
				a[prev] = a[i];
				a[i] = temp;
				++prev;
			}

			++i;
		}

		return prev;
	}
}