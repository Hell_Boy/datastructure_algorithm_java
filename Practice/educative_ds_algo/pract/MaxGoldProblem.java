
import java.util.Arrays;

class MaxGoldProblem{


	public static void main(String z[]){

		System.out.println("Max Gold Collection");

		int[][] grid = {
			{0,6,0},
			{5,8,7},
			{0,9,0}
		};

		Solution solution = new Solution();
		int maxGold = solution.getMaximumGold(grid);

		System.out.println("Max gold : "+maxGold);
	}

}

class Solution {
    int rows;
    int cols;
    int globalSum;
    
    public int getMaximumGold(int[][] grid) {
        rows = grid.length;
        cols = grid[0].length;
        globalSum = 0;
        
        for(int i = 0; i < rows; ++i) {
            for(int j = 0; j < cols; ++j) {
                dfs(grid, new boolean[rows][cols], i, j, 0);
            }
        }
        
        return globalSum;
    }
    
    private void dfs(int[][] grid, boolean[][] visited, int row, int col, int sum) {
        if(row < 0 || row >= grid.length || col < 0 || col >= grid[0].length) {
            return;
        }
        
        if(visited[row][col] || grid[row][col] == 0) {
            return;
        }
    
        visited[row][col] = true;
        sum += grid[row][col];
        globalSum = Math.max(globalSum, sum);
        
        dfs(grid, visited, row - 1, col, sum);
        dfs(grid, visited, row, col + 1, sum);
        dfs(grid, visited, row + 1, col, sum);
        dfs(grid, visited, row, col - 1, sum);
        
        visited[row][col] = false;
    }
}
