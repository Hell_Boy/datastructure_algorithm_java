
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

class NearestMissingPositiveNumber{

	public static void main(String z[]){

		System.out.println("Hello world");

		Map<int[], Integer> test = new HashMap<int[], Integer>();

		test.put(new int[] {1,2,0}, 3);
		test.put(new int[] {3,4,-1,1}, 2);
		test.put(new int[] {7,8,9,11,12}, 1);
		test.put(new int[] {1}, 2);
		test.put(new int[] {1,1}, 2);


		int count = 1;

		for(Map.Entry<int[], Integer> entry : test.entrySet()){


			int result = new NearestMissingPositiveNumber().solution(entry.getKey());

			if(result == entry.getValue())
				System.out.println("Test : "+count+" passed: result => "+result);
			else
				System.out.println("Test : "+count+" passed: result => "+result);

			++count;
		}

	}

	private int solution(int[] a){

		int size = a.length;
		boolean[] b = new boolean[size+1];


		for(int i =0; i < size; ++i){


			if(a[i] > 0 && a[i] <= size)
			b[a[i]] = true;	

		}

		for(int i =1; i < size+1; ++i)
			if(!b[i])
				return i;

		return size+1;
	}
}