
import java.util.Arrays;

class CoinExchangeProblem{

	public static void main(String z[]){

		System.out.println("Hello world");

		int[] coins = {1,2,3,4};
		int amount = 7;

		CoinExchangeProblem coinExchange = new CoinExchangeProblem();

		coinExchange.coinChange(coins, amount);
	}


	private int coinChange(int[] coins, int amt){

		int n = coins.length;
		int[][] dp = new int[n+1][amt+1];

		// for(int i =0; i < n+1; ++i){

		// 	dp[0][i] = Integer.MAX_VALUE;
		// 	dp[i][0] = 0;
		// }

		for(int i =0; i < n+1; ++i)
			for(int j =0; j < amt+1; ++j){

				if(j == 0)
					dp[i][j] = 0;

				else if(i == 0)
					dp[i][j] = Integer.MAX_VALUE;

				else if(j < coins[i-1])
					dp[i][j] = dp[i-1][j];
				else
					dp[i][j] = Math.min(dp[i-1][j], 1 + dp[i][j-coins[i-1]]);
			}

		for(int[] a : dp)
			System.out.println(Arrays.toString(a));

		return dp[n][n];
	}
}