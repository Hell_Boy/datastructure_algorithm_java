
import java.util.Stack;

class InfixToPostfix{

	private int priority(char c){

		switch(c){

			case '+':
			case '-':
				return 1;

			case '*':
			case '/':
				return 2;

			case '^':
				return 3;

			default:
				return -1;
		}
	}

	public static void main(String z[]){

		System.out.println("Hello world");


		InfixToPostfix infixToPostfix = new InfixToPostfix();

		String infix = "a+b*(c^d-e)^(f+g*h)-i";

		System.out.println("Infix : "+infix);

		String postfix = "";

		Stack<Character> st = new Stack<Character>();

		for(char c : infix.toCharArray()){

			// System.out.println("Char : "+c+" isLetterOrDigit : "+(Character.isLetterOrDigit(c)));

			if(Character.isLetterOrDigit(c)){
				postfix +=c;
			}
			else if(c == '('){
				st.push(c);
			}
			else if(c == ')'){

				while(!st.isEmpty() && st.peek() != '(')
					postfix += st.pop();

				st.pop();
			}
			else{

				while(!st.isEmpty() && infixToPostfix.priority(c) <= infixToPostfix.priority(st.peek()))
					postfix += st.pop();
				st.push(c);
			}
		}

		while(!st.isEmpty())
			postfix += st.pop();

		System.out.println("Postfix : "+postfix);
	}
}