
class RatInMaze{

	public static void main(String z[]){

		System.out.println("Hello world");

		int[][] maze = {
			{1, 0, 0, 0},
			{1, 0, 0, 0},
			{1, 1, 1, 1},
			{1, 1, 1, 1},
		};

		boolean[][] visited = new boolean[maze.length][maze[0].length];

		String path = "";

		new RatInMaze().explorePath(maze, visited, 0, 0, path);
	}

	private void explorePath(int[][] maze, boolean[][] visited, int row, int col, String path){

		if(row < 0 || col < 0 || row > (maze.length-1)|| col > (maze[0].length-1) || maze[row][col] == 0 || visited[row][col])
			return;

		
		visited[row][col] = true;

		if(row == maze.length-1 && col == maze[0].length-1){
			System.out.println("Path exists : "+path);
			// return;
		}

		// bottom
		path+='D';
		explorePath(maze, visited, row+1, col, path);

		// left
		path = path.substring(0, path.length()-1);
		path+='L';
		explorePath(maze, visited, row, col-1, path);
		
		// right
		path = path.substring(0, path.length()-1);
		path+='R';
		explorePath(maze, visited, row, col+1, path);
		
		path = path.substring(0, path.length()-1);
		visited[row][col] = false;
	}
}