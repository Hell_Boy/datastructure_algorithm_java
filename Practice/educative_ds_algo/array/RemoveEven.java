import java.util.Arrays;
import java.util.List;
import java.util.LinkedList;

class RemoveEven{

	public static void main(String z[]){
		int[] a = {1,2,4,5,10,6,3};
		System.out.println(Arrays.toString(a));

		List<Integer> ll = new LinkedList<Integer>();

		for(int i : a){

			if(i%2 != 0)
				ll.add(i);
		}


		System.out.println(ll);
	}
}