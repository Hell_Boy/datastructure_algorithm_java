import java.util.Arrays;
import java.util.Queue;
import java.util.List;
import java.util.LinkedList;

class Graph{


	static List<LinkedList<Integer>> ll = new LinkedList<LinkedList<Integer>>(){{
		add(new LinkedList<Integer>(){{
			add(1);
		}});

		add(new LinkedList<Integer>(){{
			add(2);
		}});
		
		add(new LinkedList<Integer>(){{
			add(0);
		}});
		
		add(new LinkedList<Integer>(){{
			add(2);
		}});
		

	}};
		
	static boolean[] llVisit = new boolean[ll.size()];


	public static void main(String z[]){


		int[][] g = {
			{0,1,0,0},
			{0,0,1,0},
			{1,0,0,0},
			{0,0,1,0}
		};

		int len = g.length;
		boolean[] visit = new boolean[len];

		// DFS Adjacency Matrix

		System.out.println("DFS using adjacency matrix");
		new Graph().dfs(0, len, g, visit);

		// BFS Adjacency Matrix
		System.out.println("BFS using adjacency matrix");
		new Graph().bfs(g, new boolean[g.length]);


		//// Adjacency List
		System.out.println("Adjacency List : \n");
		System.out.println("DFS : ");
		new Graph().dfsll(0, ll);

		System.out.println("\nBFS : ");
		new Graph().bfsll(0, ll);

	}

	private void bfsll(int start, List<LinkedList<Integer>> ll){


		boolean[] visited = new boolean[ll.size()];

		Queue<Integer> q = new LinkedList<Integer>();

		q.offer(start);

		while(!q.isEmpty()){

			Integer top =  q.poll();
			
			System.out.print(top+" ");

			visited[top] =true;

			for(int i : ll.get(top))
				if(!visited[i])
					q.offer(i);
		}
		System.out.println();
	}

	private void dfsll(int p, List<LinkedList<Integer>> ll){

		System.out.println("Visited : "+p);
		llVisit[p] = true;
		System.out.println(Arrays.toString(llVisit));

		List<Integer> l = ll.get(p);
		for(int i : l){

			if(!llVisit[i]){

				dfsll(i, ll);
			}
		}
	}

	private void dfs(int i, int len, int[][] g, boolean[] visit){

		// System.out.print(i+" => ");

		visit[i] = true;

		for(int j = 0; j < len; ++j){

			if(i != j && g[i][j] == 1 && !visit[j])
				dfs(j, len, g, visit);
		}
	}

	private void bfs(int[][] g, boolean[] visit){

		Queue<Integer> q = new LinkedList<Integer>();

		q.offer(g[0][0]);

		while(!q.isEmpty()){

			int top = q.poll();

			if(visit[top])
				break;

			visit[top] = true;

			System.out.println("Visited : "+top);

			for(int i =0; i < g[top].length; ++i){

				if(top != i && g[top][i] == 1)
					q.offer(i);
			}

		}
	}
}