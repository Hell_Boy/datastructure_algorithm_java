import java.util.Arrays;

class NextGreatest{

	public static void main(String z[]){

		int[] a = {5,3,4,9,7,6};

		int i = 0;

		for(i = a.length-2; i > 0; --i)
			if(a[i] < a[i+1])
				break;

		if(i == 0){
			System.out.println("Not possible");
			return;
		}

		int min = i+1;
		int x = a[i];

		for(int j = i+1; j < a.length; ++j){

			if(a[j] > x && a[j] < a[min])
				min = j;

		}

		
		int temp = a[i];
		a[i] = a[min];
		a[min] = temp;

		Arrays.sort(a, i, a.length);

		System.out.println(Arrays.toString(a));
	}
}