
import java.util.List;
import java.util.LinkedList;
import java.util.Queue;
import java.util.PriorityQueue;
import java.util.Stack;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

class TopologicalSorting{

	public static void main(String z[]){

		List<LinkedList<Integer>> ll = new LinkedList<LinkedList<Integer>>(){{
			add(new LinkedList<Integer>(){{
				add(3);
				add(1);
			}});

			add(new LinkedList<Integer>(){{
				add(2);
			}});
			
			add(new LinkedList<Integer>(){{
				add(3);
			}});


			add(new LinkedList<Integer>(){{
				add(4);
			}});
			
			add(new LinkedList<Integer>(){{
				add(5);
				add(6);
			}});
			
			add(new LinkedList<Integer>(){{
				add(6);
			}});

			add(new LinkedList<Integer>());		
		}};

		TopologicalSorting ts = new TopologicalSorting();

		System.out.print("DFS Iterative : ");
		ts.dfs(ll);


		System.out.print("DFS Recursive : ");
		ts.dfsRecursive(ll, 0, new boolean[ll.size()]);

		System.out.print("\n\ntopological sorting using iterative dfs : \n");

		ts.sort(ll);

		System.out.print("\ntopological sorting using recursive dfs : ");

		Stack<Integer> st = new Stack<Integer>();

		ts.topologicalSortDFSRecursive(ll, 0, new boolean[ll.size()], st);

		List<Integer> l = new LinkedList<Integer>();

		while(!st.isEmpty())
			l.add(st.pop());

		System.out.println(l);
	}

	private void topologicalSortDFSRecursive(List<LinkedList<Integer>> ll, int index, boolean[] visited, Stack<Integer> st){

		visited[index] = true;

		for(int adj : ll.get(index))
			if(!visited[adj])
				topologicalSortDFSRecursive(ll, adj, visited, st);

		st.push(index);	
	}

	private void sort(List<LinkedList<Integer>> ll){


		// Calculate indegrees

		int[] indegrees = new int[ll.size()];
		boolean[] visited = new boolean[ll.size()];

		Stack<Integer> st = new Stack<Integer>();
		st.push(0);

		while(!st.isEmpty()){
			int top = st.pop();

			visited[top] = true;

			for(int adj : ll.get(top))
				if(!visited[adj]){
					st.push(adj);
					++indegrees[adj]; 
				}
		}

		System.out.println("Indegrees : "+Arrays.toString(indegrees));

		int start = Integer.MAX_VALUE;

		for(int i =0; i < indegrees.length; ++i)
			if(indegrees[i] == 0){
				start = i;
				break;
			}

		// Calculate order 
		Queue<Integer> order = new LinkedList<Integer>();
		// order.offer(start);

		Queue<Integer> q = new LinkedList<Integer>();
		q.offer(start);
		boolean[] tempVisited = new boolean[ll.size()]; 

		while(!q.isEmpty()){
			int top = q.poll();
			tempVisited[top] = true;
			order.offer(top);

			for(int adj : ll.get(top)){

				if(tempVisited[adj])
					break;

				--indegrees[adj];

				if(indegrees[adj] == 0)
					q.offer(adj);

			}
		}

		System.out.println("sortVal : "+order);
	}

	private void dfsRecursive(List<LinkedList<Integer>> ll, int index, boolean[] visited){

		visited[index] = true;

		System.out.print(" "+index);

		for(int adj : ll.get(index))
			if(!visited[adj])
				dfsRecursive(ll, adj, visited);
	}

	private void dfs(List<LinkedList<Integer>> ll){


		boolean[] visit = new boolean[ll.size()];

		Stack<Integer> st = new Stack<Integer>();

		st.push(0);

		// Map<Integer, Integer> map = new HashMap<Integer, Integer>();

		// map.put(0, -1);

		while(!st.isEmpty()){

			int top = st.pop();
			visit[top] = true;

			System.out.print(top+ " ");
			for(int adj : ll.get(top))
				if(!visit[adj])
					st.push(adj);
		}

		System.out.println();

	}
}