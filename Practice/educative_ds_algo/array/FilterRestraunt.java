import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;


class FilterRestraunt{

	public static void main(String z[]){

		// Restaurant 1 [id=1, rating=4, veganFriendly=1, price=40, distance=10]

		int veganFriendly=1, price=40, distance=10;

		int[][] res = {
			{1,4,1,40,10},
			{2,8,0,50,5},
			{3,8,1,30,4},
			{4,10,0,10,3},
			{5,1,1,15,1}
		};


		Arrays.sort(res, (a,b)->{
			if(a[1] == b[1])
				return b[0]-a[0];

			else
				return b[1]-a[1];
		});

		List<int[]> filteredList = new ArrayList<int[]>();
		for(int[] i : res){

			if((i[2] == veganFriendly || veganFriendly == 0) && i[3] < price+1 && i[4] < distance+1)
			filteredList.add(i);
		}

		for(int[] val : filteredList)
			System.out.println(Arrays.toString(val));

	}
}