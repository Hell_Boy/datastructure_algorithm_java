import java.util.Arrays;

class MergeTwoSortedArray{

	public static void main(String z[]){

		System.out.println("Hello world");

		int[] a1 = {1,3,4,5};
		int[] a2 = {2,6,7,8};
		int[] a3 = new int[a1.length+a2.length];

		int i=0, j=0, k =0;

		while(i < a1.length && j < a2.length){

			if(a1[i] < a2[j]){
				a3[k] = a1[i];
				++i;
			}else{
				a3[k] = a2[j];

				++j;
			}

			++k;
		}


		while(i < a1.length){
			a3[k] = a1[i];
			++k;
			++i;
		}

		while(j < a2.length){
			a3[k] = a2[j];
			++k;
			++j;
		}

		System.out.println(Arrays.toString(a3));
	}
}