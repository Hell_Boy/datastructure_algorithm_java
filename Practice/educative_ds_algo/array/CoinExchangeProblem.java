import java.util.Arrays;

class CoinExchangeProblem{

	public static void main(String z[]){

		System.out.println("Hello world");

		int[] coins = {1,3,5};

		int value = 7;

		int[][] dp = new int[coins.length][value+1];

		for(int i =0; i < coins.length; ++i){

			for(int j =0; j < value+1; ++j){
				
				if(i == 0)
					dp[i][j]= 1;

				else if(coins[i] < j  && coins[i] % j == 0)
					dp[i][j] = 1;

				else if(coins[i] > j )
					dp[i][j] = dp[i-1][j];

				else{
					dp[i][j] = dp[i-1][j]+ dp[i][j-coins[i]];

					// for minimum ways
					// Math.min(dp[i][j-coins[i]]+1,dp[i-1][j]);
				}


			}
		}

		for(int[] row : dp)
			System.out.println(Arrays.toString(row));


	}
}