
import java.util.Arrays;


class LongestIncreasingSubArray{

	public static void main(String z[]){

		System.out.println("Hello world");

		int[] a = {3, 10, 2, 1, 20};

		int[] temp = new int[a.length];

		Arrays.fill(temp, 1);

		
		int j = 0, i =0;

		for(i= j+1; i < a.length; ++i){

			for(j = 0; j < i; ++j)
				if(a[j] < a[i] && temp[i] < temp[j]+1)
					temp[i] = temp[j]+1;
		}

		System.out.println(Arrays.toString(temp));

		int maxVal = Integer.MIN_VALUE, maxIndex = -1;

		for(int k = 0; k < temp.length; ++k){

			if(maxVal < temp[k]){
				maxVal = temp[k];
				maxIndex = k;
			}
		}

		System.out.println("max val : "+maxVal);
		System.out.println("max ind : "+maxIndex);


		int[] res = new int[maxVal];
		res[maxVal-1] = a[maxIndex];

		--maxVal;
		--maxIndex;
		while(maxVal > -1 && maxIndex > -1){
			if(maxVal == temp[maxIndex]){
				res[maxVal-1] = a[maxIndex];		
				--maxVal;
			}
			--maxIndex;

		}

		System.out.println(Arrays.toString(res));
	}
} 