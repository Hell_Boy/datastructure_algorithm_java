
import java.util.List;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

class GraphRevision{

	public static void main(String z[]){

		GraphRevision gr = new GraphRevision();

		// System.out.println("Hello world");

		// Adjacency list
		System.out.println("\nAdjacency List");

		List<LinkedList<Integer>> ll = new LinkedList<LinkedList<Integer>>(){{
			add(new LinkedList<Integer>(){{
				add(1);
			}});

			add(new LinkedList<Integer>(){{
				add(2);
			}});
			
			add(new LinkedList<Integer>(){{
				add(0);
			}});
			
			add(new LinkedList<Integer>(){{
				add(2);
			}});

		}};


		gr.bfsll(ll);
		gr.dfsll(ll);


		// Adjacency Matrix
		System.out.println("\nAdjacency Matrix");
		int[][] adjMatrix = {
			{0,1,0,0},
			{0,0,1,0},
			{1,0,0,0},
			{0,0,1,0},
		};

		gr.bfsadj(adjMatrix);
		gr.dfsadj(adjMatrix);

	}

	private void bfsadj(int[][] g){

		System.out.print("BFS ad : ");

		Queue<Integer> q = new LinkedList<Integer>();
		boolean[] visited = new boolean[g.length];

		q.offer(0);

		while(!q.isEmpty()){

			int top = q.poll();
			visited[top] = true;

			System.out.print(top+ " ");

			for(int adj = 0; adj < g[top].length; ++adj){
				if(g[top][adj] == 1 && !visited[adj]){
					q.offer(adj);
				}
			}
		}

		System.out.println();
	}

	private void dfsadj(int[][] g){

		System.out.print("DFS ad : ");

		Stack<Integer> st = new Stack<Integer>();

		boolean[] visited = new boolean[g.length];

		st.push(0);

		while(!st.isEmpty()){

			int top = st.pop();
			visited[top] = true;

			System.out.print(top+" ");

			for(int adj = 0; adj < g[top].length; ++adj)
				if( g[top][adj] == 1 && !visited[adj])
					st.push(adj);
		}
		System.out.println();
	}

	private void bfsll(List<LinkedList<Integer>> ll){

		System.out.print("BFS ll : ");

		Queue<Integer> q = new LinkedList<Integer>();

		boolean[] visited = new boolean[ll.size()];

		q.add(0);

		while(!q.isEmpty()){

			int top = q.poll();
			visited[top] = true;
			System.out.print(top+" ");

			for(int adj : ll.get(top))
				if(!visited[adj])
					q.offer(adj);
		}

		System.out.println();
	}

	private void dfsll(List<LinkedList<Integer>> ll){

		System.out.print("DFS ll : ");

		Stack<Integer> st = new Stack<Integer>();
		boolean[] visited = new boolean[ll.size()];

		st.push(0);

		while(!st.isEmpty()){

			int top = st.pop();
			visited[top] = true;

			System.out.print(top+" ");

			for(int adj : ll.get(top))
				if(!visited[adj])
					st.push(adj);
		}

		System.out.println();
	}
}
