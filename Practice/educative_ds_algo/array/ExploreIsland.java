
import java.util.Arrays;

class ExploreIsland{

	public static void main(String z[]){

		char[][] grid = {
		  {'1','1','0','0','0'},
		  {'1','1','0','0','0'},
		  {'0','0','1','0','0'},
		  {'0','0','0','1','1'}
		};

		int islands = new ExploreIsland().numIslands(grid);

		for(char[] c : grid)
			System.out.println(Arrays.toString(c));

		System.out.println("Islands : "+islands);



	}

	private void explore(char[][] grid, int i, int j){

		if(i > grid.length-1 || j > grid[0].length-1 || i < 0 || j < 0 || grid[i][j] != '1')
			return;

		grid[i][j] = '2';

		explore(grid, i+1, j);
		explore(grid, i-1, j);
		explore(grid, i, j+1);
		explore(grid, i, j-1);
	}

	private int numIslands(char[][] grid){

		int isLandCount = 0;

		for(int i =0; i < grid.length; ++i)
			for(int j =0; j < grid[0].length; ++j){

				if(grid[i][j] == '1'){
					explore(grid, i, j);
					++isLandCount;
				}
			}

			return isLandCount;
	}
}