import java.util.Map;
import java.util.HashMap;
import java.util.Queue;
import java.util.PriorityQueue;
import java.util.Arrays;

class TopKthElem{

	public static void main(String z[]){

		System.out.println("hello world");

		int[] elems = {1,1,1,2,2,3};
		int k = 2;

		Map<Integer,Integer> map = new HashMap<Integer,Integer>();

		Queue<Map.Entry<Integer,Integer>> pq = new PriorityQueue<Map.Entry<Integer,Integer>>((a,b)-> b.getValue()-a.getValue());

		for(int elem : elems){

			int freq = map.getOrDefault(elem, 0);
			map.put(elem,++freq);
		}


		for(Map.Entry<Integer,Integer> entry : map.entrySet())
			pq.offer(entry);

		int[] ans = new int[k];

		for(int i=0; i < ans.length; ++i)
			ans[i] = pq.poll().getKey();

		System.out.println(Arrays.toString(ans));
		
	}
}