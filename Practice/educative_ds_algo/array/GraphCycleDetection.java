
import java.util.List;
import java.util.LinkedList;

class GraphCycleDetection{

	public static void main(String z[]){

		List<LinkedList<Integer>> ll = new LinkedList<LinkedList<Integer>>(){{
			add(new LinkedList<Integer>(){{
				add(1);
			}});

			add(new LinkedList<Integer>());

			add(new LinkedList<Integer>(){{
				add(1);
				// add(3);
			}});
			add(new LinkedList<Integer>(){{
				// add(4);
			}});
			add(new LinkedList<Integer>(){{
				add(0);
				add(2);
			}});
			
		}};

		GraphCycleDetection gc = new GraphCycleDetection();
		System.out.println("Deadlock present : "+gc.deadlockDetection(ll));


		
	}

	private boolean deadlockDetectionUtil(int curr, List<LinkedList<Integer>> ll, boolean[] visited){

		// check deadlock present
		if(visited[curr])
			return true;

		visited[curr] = true;

		for(int i : ll.get(curr)){

			boolean value = deadlockDetectionUtil(i, ll, visited);

			if(value)
				return true;
		}

		return false;

	}

	private boolean deadlockDetection(List<LinkedList<Integer>> ll){

		boolean[] visited = new boolean[ll.size()];

		for(int i =0; i < ll.size(); ++i){

			visited[i] = true;

			for(int j : ll.get(i)){

				boolean value = deadlockDetectionUtil(j, ll, visited);

				if(value)
					return true;
			}

			visited[i] = false;
		}

		return false;
	}
}