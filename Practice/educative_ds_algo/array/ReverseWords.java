import java.util.Arrays;


class ReverseWords{

	public static void main(String z[]){

		System.out.println("Reverse words");

		String s = "Hello world";

		s = new ReverseWords().reverseWord(s);

		System.out.println(s);
	}

	private String reverseWord(String s){

		int start =0;
		int end;
		int len = s.length();
		String res = null;

		while(start < len){

			while(start < len && s.charAt(start) ==' ')
				++start;

			if(start == len)
				break;

			end = start +1;

			while(end < len && s.charAt(end)!=' ')
				++end;

			res = res == null ? s.substring(start, end) : s.substring(start, end)+" "+res;

			start = end+1;
		}

		return res;
	}
}