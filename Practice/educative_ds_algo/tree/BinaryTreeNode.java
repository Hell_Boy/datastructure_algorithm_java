
class BinaryTreeNode {

	public int data;
	public BinaryTreeNode left, right;

	BinaryTreeNode(int data){
		this.data = data;
	}

	BinaryTreeNode(int data, BinaryTreeNode left, BinaryTreeNode right){
		this.data = data;
		this.left = left;
		this.right = right;
	}



}