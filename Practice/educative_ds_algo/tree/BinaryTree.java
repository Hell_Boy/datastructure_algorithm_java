
import java.util.Queue;
import java.util.List;
import java.util.LinkedList;
import java.util.Stack;

class BinaryTree{

	public static void main(String z[]){

		// System.out.println("Hello world");

		BinaryTreeNode root = new BinaryTreeNode(1);
		root.left = new BinaryTreeNode(2);
		root.left.left = new BinaryTreeNode(4);
		root.left.right = new BinaryTreeNode(5);

		root.right = new BinaryTreeNode(3);
		root.right.left = new BinaryTreeNode(6);
		root.right.right = new BinaryTreeNode(7);

		BinaryTree tree = new BinaryTree();

		// Levelorder traversal (each node on a level)
		System.out.println("==> Levelorder traversal : \n");
		tree.levelOrderTraversal(root);
		System.out.println();

		// Inorder Traversal (LDR)
		System.out.print("==> Inorder Traversal (LDR) \n");
		System.out.print("Recursive : ");
		tree.inOrderRecursive(root);
		System.out.print("\nIterative : ");
		tree.inOrderIterative(root);


		// Preorder Traversal (DLR)
		System.out.print("==> Preorder Traversal (DLR) : \n");
		System.out.print("Recursive : ");
		tree.preOrderRecursive(root);
		System.out.println();
		System.out.print("Iterative : ");
		tree.preOrderIterative(root);

		// Postorder Traversal (RLD)
		System.out.print("==> Postorder Traversal (RLD) : ");
		tree.postOrderRecursive(root);
		System.out.println();


		// Size of a binary tree
		System.out.println("Size : "+tree.size(root));

		// Height of binary Tree
		System.out.println("\nHeight : ");
		System.out.println("Recursive : "+ tree.heightRecursive(root));
		System.out.println("Iterative : "+tree.heightIterative(root));


		// Diameter of tree
		tree.diameterUsingHeight(root);
		System.out.println("\nDiameter method 1 : "+diameter);

		System.out.println("Diameter method 2 : "+(tree.diameterUsingHeight(root)-1));


		// Tree Constructin from PreOrder and InOrder traversal
		int[] inOrder = {4,2,5,1,6,3};
		int[] preOrder = {1,2,4,5,3,6};
		BinaryTreeNode root2 = tree.buildBinaryTreeNode(preOrder, inOrder);
		System.out.println("\n==> Tree :");
		tree.levelOrderTraversal(root2);
		System.out.println();

	}

	private BinaryTreeNode leastCommonAncestor(BinaryTreeNode root, BinaryTreeNode a, BinaryTreeNode b){

		if(root == null)
			return null;

		if(root.data == a || root.data == b)
			return root;

		BinaryTreeNode left = leastCommonAncestor(root.left, a, b);
		BinaryTreeNode right = leastCommonAncestor(root.right, a, b);

		if(left !=null && right !=null)
			return root;

		else
			left == null ? right : left;
	}

	private BinaryTreeNode buildBinaryTreeNode(int[] preOrder, int[] inOrder){

		if(preOrder.length == 0 || preOrder.length != inOrder.length)
			return null;

		return buildBT(preOrder, 0, preOrder.length-1, inOrder, 0, inOrder.length-1);
	}

	private BinaryTreeNode buildBT(int[] preOrder, int preStart, int preEnd, int[] inOrder, int inStart, int inEnd){

		if(preStart > preEnd || inStart > inEnd)
			return null;

		int data = preOrder[preStart];
		BinaryTreeNode cur = new BinaryTreeNode(data);

		int offset = inStart;

		while(offset < inEnd){

			if(inOrder[offset] == data)
				break;
			++offset;
		}

		cur.left = buildBT(preOrder, preStart+1, preStart+offset-inStart, inOrder, inStart, offset-1);
		cur.right = buildBT(preOrder, preStart+offset-inStart+1, preEnd, inOrder, offset+1, inEnd);

		/**
		 * From PostOrder and inOrder
		 * 
		 * cur.left = buildBT(preOrder, preStart, preStart+offset-inStart-1, inOrder, inStart, offset-1);
		 * cur.right = buildBT(preOrder, preStart+offset-inStart, preEnd-1, inOrder, offset+1, inEnd);
		 * **/

		return cur;
	}

	private int diameterUsingHeight(BinaryTreeNode root){

		if(root == null)
			return 0;

		int len1 = heightRecursive(root.left) + heightRecursive(root.right);
		int len2 = Math.max(diameterUsingHeight(root.left), diameterUsingHeight(root.right));

		return Math.max(len1, len2);
	}


	 static int diameter = Integer.MIN_VALUE;
    // Working fine tested on leetcode
    private int diameterCalc(BinaryTreeNode root){
        
        if(root == null)
            return 0;
        
        int left = diameterCalc(root.left);
        int right = diameterCalc(root.right);
        
        diameter = Math.max(diameter, left+right);
        
        return Math.max(left, right)+1;
        
    }

	private int heightRecursive(BinaryTreeNode root){

		if(root == null)
			return 0;

		int left = heightRecursive(root.left);
		int right = heightRecursive(root.right);

		return Math.max(left, right)+1;

	}

	private int heightIterative(BinaryTreeNode root){

		int count = 1;

		Queue<BinaryTreeNode> q = new LinkedList<BinaryTreeNode>();

		q.offer(root);
		q.offer(null);

		while(!q.isEmpty()){

			BinaryTreeNode top = q.poll();

			if(top != null){

				if(top.left != null)
					q.offer(top.left);

				if(top.right != null)
					q.offer(top.right);
			}else{

				if(!q.isEmpty()){
					++count;
					q.offer(null);
				}

			}
		}

		return count;
	}

	private int size(BinaryTreeNode root){

		int leftCount = root.left == null ? 0 : size(root.left);
		int rightCount = root.right == null ? 0 : size(root.right);

		return 1 + leftCount + rightCount;
	}

	private void inOrderRecursive(BinaryTreeNode root){

		if(root == null)
			return;

		inOrderRecursive(root.left);

		System.out.print(root.data+" ");

		inOrderRecursive(root.right);

	}

	private void inOrderIterative(BinaryTreeNode root){

		List<Integer> result = new LinkedList<Integer>();

		Stack<BinaryTreeNode> st = new Stack<BinaryTreeNode>();

		BinaryTreeNode currentNode = root;

		boolean done = false;

		while(!done){

			if(currentNode != null){

				st.push(currentNode);

				currentNode = currentNode.left;
			}else{

				if(st.isEmpty())
					done = true;
				
				else{

					currentNode = st.pop();

					result.add(currentNode.data);

					currentNode = currentNode.right;
				}
			}
		
		}

		for(int i : result)
			System.out.print(i+" ");

		System.out.println("\n");
	}

	private void preOrderRecursive(BinaryTreeNode root){

		if(root == null)
			return;

		System.out.print(root.data+" ");
		
		preOrderRecursive(root.left);

		preOrderRecursive(root.right);
	}

	private void preOrderIterative(BinaryTreeNode root){

		List<Integer> result = new LinkedList<Integer>();

		Stack<BinaryTreeNode> st = new Stack<BinaryTreeNode>();

		st.push(root);

		while(!st.isEmpty()){

			BinaryTreeNode top = st.pop();

			result.add(top.data);

			if(top.right != null)
				st.push(top.right);

			if(top.left != null)
				st.push(top.left);

		}
		
		for(int i : result)
			System.out.print(i+" ");

		System.out.println();
	}

	private void postOrderRecursive(BinaryTreeNode root){

		if(root == null)
			return;

		postOrderRecursive(root.left);
		
		postOrderRecursive(root.right);

		System.out.print(root.data+" ");
	}

	private void levelOrderTraversal(BinaryTreeNode root){

		Queue<BinaryTreeNode> q = new LinkedList<BinaryTreeNode>();

		q.offer(root);
		q.offer(null);


		int level = 1;
		List<Integer> levelNodes = new LinkedList<Integer>();

		while(!q.isEmpty()){

			BinaryTreeNode top = q.poll();

			if(top == null){

				System.out.println("level "+level+" : "+levelNodes);

				levelNodes.clear();
				++level;

				if(!q.isEmpty())
					q.offer(null);
			}else{

				levelNodes.add(top.data);

				if(top.left != null)
					q.offer(top.left);

				if(top.right != null)
					q.offer(top.right);
			}
		}

	}

}