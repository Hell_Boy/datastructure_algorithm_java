
import java.util.*;

class TextJustification{


	public static void main(String z[]){

		String[] str = {"word", "wrap","problem","solved","by", "Joey'sTech"};
		int width = 15;

		int[] cost = new int[str.length];
		Arrays.fill(cost, Integer.MAX_VALUE);

		int[] result = new int[str.length];
		Arrays.fill(result, Integer.MAX_VALUE);

		int[][] matrix = new int[str.length][str.length];

		for(int[] tmp : matrix)
			Arrays.fill(tmp, Integer.MAX_VALUE);

		for(int i =0; i < matrix.length; ++i){

			int len = str[i].length();
			for(int j = i ; j < matrix[i].length; ++j){

				int diff;

				diff = width - str[j].length();

				if(i != j){

					diff -= len+1;
					len = len +1 +str[j].length(); 	
				}
				

				if(diff < 0)
					break;
				matrix[i][j] = diff*diff;
			}
		}

		for(int[] tmp : matrix)
			System.out.println(Arrays.toString(tmp));
		

		for(int i = str.length-1; i >-1; i--){
			for(int j = str.length-1; j >= i; j--){

				if((i == str.length-1 || j == str.length-1) && matrix[i][j] != Integer.MAX_VALUE){

					cost[i] = matrix[i][j];
					result[i] = str.length;
					
				}else{

					if(j-1 >-1){
					long tmpCost =(long) cost[j]+matrix[i][j-1];

					if(cost[i] > tmpCost){
							cost[i] = (int) tmpCost;
							result[i] = j;
					}
				}
				}
			}
		}

		System.out.println("\n\n");
		System.out.println("str : "+Arrays.toString(str));
		System.out.println("Cst : "+Arrays.toString(cost));
		System.out.println("Rst : "+Arrays.toString(result));

		int current = 0, cursor = result[0];

		List<String> para = new ArrayList<String>();

		StringBuilder sb = new StringBuilder();
		
		do{

			sb.append(str[current]);

			if(current < cursor)
				sb.append(" ");

			++current;

			if( current == cursor){
				
				para.add(sb.toString());
				sb.setLength(0);

				if(current != str.length)
					cursor = result[current];
			}

			// System.out.println("Cursor : "+cursor+" current : "+current);

		}while(current < cursor);

		System.out.println(para);
	}
}