
class CapturingWater{

  public static void main(String z[]){
    System.out.println("Capturing water");

    int[] a = {0,1,0,2,1,0,1,3,2,1,2,1};

    int curr=0, next=0, localSum=0, globalSum=0;

    for(int i =0; i < a.length; ++i){

      if(curr == 0){
        curr = a[i];
        next = curr;
      }else{
        next = a[i];
        if(curr < next){
          globalSum +=localSum;
          localSum = 0;
          curr = next;
        }else{
          localSum += curr-next;
        }

      }
    }


    System.out.println("Captured water : "+globalSum);
  }
}
