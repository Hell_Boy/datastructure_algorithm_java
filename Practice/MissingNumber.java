import java.util.*;

class MissingNumber{

	public static void main(String z[]){


		System.out.println("Missing number... ");

		int a[] = {3,7,1,2,8,4,5};

		int missing = 0;

		Arrays.sort(a);

		for(int i =1; i < a[a.length-1]; ++i)
			missing ^= i;

		for(int i =0; i < a.length-1; ++i)
			missing ^= a[i];


		System.out.println("Missing number : "+missing);
	}
}