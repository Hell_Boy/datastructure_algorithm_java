import java.util.*;
import java.lang.Integer;

class GroupAnagrams{

  public static void main(String z[]){

    System.out.println("Group anagrams");

    List<String> strs = Arrays.asList(new String[]{"eat","tea","tan","ate","nat","bat"});

    System.out.println(strs);


    Map<Integer, List<String>> map = new HashMap<Integer, List<String>>();

       for(String s : strs){
           int temp = 0;
           for(char c : s.toCharArray())
               temp+=c;


           if(map.containsKey(Integer.valueOf(temp))){

             List<String> tempStr = map.get(temp);
             tempStr.add(s);
             map.replace(temp, tempStr);
           }
           else{
             List<String> tempStr =  new ArrayList<String>();
             tempStr.add(s);
             map.put(temp, tempStr);
           }
       }



       List<List<String>> result = new ArrayList<List<String>>();

       for(Map.Entry<Integer, List<String>> entry: map.entrySet())
        result.add(entry.getValue());

         System.out.println("Final map : "+result);
  }
}
