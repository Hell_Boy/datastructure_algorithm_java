import java.util.*;

class AutoScaling{

	public static void main(String z[]){

		System.out.println("Autoscaling...");

		// int instances = 2;
		// int[] avgUtil =  {25, 23, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 76, 80};
		int instances = 1;
		int[] avgUtil =  {25, 23, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 76, 80};
		int maxInstanceLimit = (int) Math.pow(10,8);
		maxInstanceLimit *= 2;

		for(int time : avgUtil){

			if(instances > 1 && time < 25)
				instances = (int) Math.ceil(instances/2);

			if(time > 60 && instances < maxInstanceLimit)
				instances *=2;

			String log = String.format("%d time instance : %d", time, instances);

			System.out.println(log);
		}

		System.out.println("Active Instances : "+instances);

	}
}