import java.util.Arrays;
import java.util.ArrayList;

class ThreeNumberSum{

	public static void main(String z[]){

		System.out.println("Three number sum");

		int[] a = {-1,0,1,2,-1,-4};

		Arrays.sort(a);
		System.out.println(Arrays.toString(a));

		ArrayList<ArrayList<Integer>> threeNumberCombination = new ArrayList<ArrayList<Integer>>();

		int[] twoNums = new int[2];
		for(int i =0; i < a.length-2; ++i){


			twoNums = new ThreeNumberSum().zeroSum(a,a[i],(i+1),(a.length-1) );

			if(twoNums[0] != Integer.MIN_VALUE && twoNums[1] != Integer.MIN_VALUE && (a[i] !=twoNums[0] || a[i] !=twoNums[1])){

				ArrayList<Integer> tempList = new ArrayList<Integer>();
				tempList.add(a[i]);
				tempList.add(twoNums[0]);
				tempList.add(twoNums[1]);
				threeNumberCombination.add(tempList);
			}
		}

		System.out.println(threeNumberCombination.toString());

	}

	private int[] zeroSum(int[] a, int n , int start, int end){

		int[] t = new int[2];
		t[0] = Integer.MIN_VALUE;
		t[1] = Integer.MIN_VALUE;
		
		int sum = Integer.MIN_VALUE;


		while(start < end){

			sum = n+a[start]+a[end];
			System.out.println(n+" "+a[start]+" "+a[end]);
			System.out.println("Sum : "+sum);

			 if(sum < 0)
			++start;

			else if(sum > 0)
			--end;

			else{
				t[0] = a[start];
				t[1] = a[end];
				break;
			}
		}

		return t;
	}
}