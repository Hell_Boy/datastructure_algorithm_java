
class ListNode {
  int val;
  ListNode next;

  ListNode() {
  }

  ListNode(int val) {
    this.val = val;
  }

  ListNode(int val, ListNode next) {
    this.val = val;
    this.next = next;
  }
}

class MergeTwoSortedLinkedList {

  public static void main(String z[]) {

    System.out.println("Merge two sorted linkedlist");

    ListNode l1 = new ListNode(1);
    l1.next = new ListNode(2);
    l1.next.next = new ListNode(4);

    ListNode l2 = new ListNode(1);
    l2.next = new ListNode(3);
    l2.next.next = new ListNode(4);

    // new MergeTwoSortedLinkedList().printList(l1);
    // new MergeTwoSortedLinkedList().printList(l2);

    ListNode mergedList = new MergeTwoSortedLinkedList().mergeTwoLists(l1,l2);

    // System.out.println("Merged list : "+mergedList);
    new MergeTwoSortedLinkedList().printList(mergedList);

  }

  private ListNode mergeTwoLists(ListNode l1, ListNode l2) {

    ListNode list = new ListNode();
    ListNode head = list;

    while (l1 != null && l2 != null) {

      if (l1.val < l2.val) {

        list.next = new ListNode(l1.val);

        l1 = l1.next;
      } else {

        list.next = new ListNode(l2.val);
        l2 = l2.next;
      }

      list = list.next;
    }

    while (l1 != null) {
      list.next = new ListNode(l1.val);
      list = list.next;
      l1 = l1.next;
    }

    while (l2 != null) {

      list.next = new ListNode(l2.val);
      list = list.next;
      l2 = l2.next;
    }

    head = head.next;
    return head;
  }

  private void printList(ListNode l) {

    while (l != null) {

      System.out.println(l.val);
      l = l.next;
    }

    System.out.println();
  }
}
