import java.util.*;

class MaximumSizeSubMatrixWithAll1{

	public static void main(String z[]){

		System.out.println("Hello world");

		int[][] matrix = {
			{0,1,1,0,1},
			{1,1,0,1,0},
			{0,1,1,1,0},
			{1,1,1,1,0},
			{1,1,1,1,1},
			{0,0,0,0,0}
		};

		System.out.println("Rows : "+matrix.length);
		System.out.println("Columns : "+matrix[0].length);

		int i = 0, j = 0;

		for(i = 0; i < matrix.length; ++i){

			for(j =0; j < matrix[i].length; ++j)
				System.out.print(" "+matrix[i][j]+ " ");

			System.out.println();
		}

		System.out.println("\nCalculating Submatrix...\n");

		int[][] matrix2 = new int[matrix.length][matrix[0].length];

		for(i =0; i < matrix2.length; ++i)
			for(j =0; j < matrix2[0].length; ++j){

				if(i==0 || j ==0)
					matrix2[i][j]=0;

				else if(matrix[i-1][j-1] == 1)
					matrix2[i][j] = Math.min(Math.min(matrix2[i-1][j], matrix2[i][j-1]), matrix2[i-1][j-1])+1;
				else
					matrix2[i][j]= 0;
			}

		for(i = 0; i < matrix2.length; ++i){

			for(j =0; j < matrix2[i].length; ++j)
				System.out.print(" "+matrix2[i][j]+ " ");

			System.out.println();
		}
	}
}