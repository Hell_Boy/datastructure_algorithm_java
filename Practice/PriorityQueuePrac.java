
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.Arrays;

class PriorityQueuePrac{
	
	public static void main(String z[]){

		System.out.println("Priority queue");

		// int[] a = {1,3,4,2,3,4,5,3,24,4,55,6,6,4,3,5,3};

		// PriorityQueue<Integer> pq1 = new PriorityQueue<Integer>();

		// for(int elem : a)
		// 	pq1.add(elem);

		// System.out.println("\n\tPriority Queue Integer\n");
		// while(!pq1.isEmpty())
		// 	System.out.println("Priority Queue Integer : "+pq1.poll());

		// System.out.println("\n\n\tPriority Queue Reversed\n");

		// PriorityQueue<Integer> pq2 = new PriorityQueue<Integer>((i1,i2)-> i2-i1);

		// for(int elem : a)
		// 	pq2.add(elem);

		// while(!pq2.isEmpty())
		// 	System.out.println("Priority Queue Reversed : "+pq2.poll());

		int n = 5, k = 0;

		int[][] edges = {
			
			{0,2,1},
			{2,1,2},
			{0,1,4},
			{2,3,4},
			{1,4,4},
			{3,4,4}
		};


		boolean[] visited = new boolean[n];
		int[] distance = new int[n];

		Arrays.fill(distance, 9999);

		Map<Integer, ArrayList<Edge>> graph = new HashMap<Integer,ArrayList<Edge>>();

		for(int[] e : edges){

			if(!graph.containsKey(e[0]))
				graph.put(e[0], new ArrayList<Edge>());

			if(!graph.containsKey(e[1]))
				graph.put(e[1], new ArrayList<Edge>());
			
			graph.get(e[0]).add(new Edge(e[0], e[1], e[2]));
		}

		PriorityQueue<int[]> pq = new PriorityQueue<int[]>((x,y)-> x[1]-y[1]);
		pq.add(new int[]{k,0});
		visited[k] = true;
		distance[k] = 0;

		while(!pq.isEmpty()){

			int[] top = pq.poll();

			
			for(Edge adj: graph.get(top[0])){

				if(!visited[adj.v]){

					visited[adj.v] = true;
					pq.add(new int[]{adj.v,adj.w});
				}

				int temp = distance[top[0]]+adj.w;

				distance[adj.v] = distance[adj.v] < temp ? distance[adj.v] : temp;
			}
		}

	}


}

	class Edge{

		int u;
		int v;
		int w;

		public Edge(int u, int v, int w){
			this.u = u;
			this.v = v;
			this.w = w;
		}
	}
  