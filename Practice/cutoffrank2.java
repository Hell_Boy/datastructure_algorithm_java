import java.util.*;

class Cuttoffrank2{

	public static void main(String z[]){
		System.out.println("Hello world");

		int cuttoff = 3;
		int num = 4;
		int[] scores = {100,50,25,50};

		// int cuttoff = 4;
		// int num = 5;
		// int[] scores = {2,2,3,4,5};

		Arrays.sort(scores);
		System.out.println("Sorted scores : "+Arrays.toString(scores));

		int count = 0, rank = 0, prev = Integer.MAX_VALUE, pointer = num-1;

		while(pointer > -1 && rank < cuttoff+1){

			if(prev > scores[pointer]){
				++rank;
				prev = scores[pointer];
			}

			++count;
			--pointer;

			System.out.println("Rank : "+rank);
		}

		System.out.println("Count : "+count);
	}
}