import java.util.Arrays;

class KArrayString{

  public static void main(String z[]){

    System.out.println("K array string\n");

    int n = 4;
    char[] k = {'a','v','c','d'};
    char[] l = new char[k.length];

    new KArrayString().generate(n, k, l);
  }

  private void generate(int n, char[] k, char[] l){

    if(n <=0){
      System.out.println("String : "+Arrays.toString(l));
    }

    for(int j = 0; j < n; ++j){
      l[n-1] = k[j];
      generate(n-1, k, l);
    }
  }
}
