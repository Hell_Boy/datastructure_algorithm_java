
class TowerOfHanoi{

  public static void main(String z[]){

    System.out.println("Tower of hanoi\n");

    int disk = 3;

    new TowerOfHanoi().movement(disk, 'A', 'B', 'C');

  }

  private void movement(int n, char from, char to, char aux){

    if(n == 1){

      System.out.println("Move from "+from+" to "+to);
      return;
    }

    movement(n-1, from, aux, to);

    System.out.println("Move from "+from+" to "+to);

    movement(n-1, aux, to, from);

  }
}
