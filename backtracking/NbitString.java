
import java.util.Arrays;

class NbitString{

  public static void main(String z[]){

    System.out.println("N bit string\n");

    int n = 10;
    int[] bits = new int[n];

    new NbitString().generate(n, bits);
  }

  private void generate(int n, int[] b){

    if(n <= 0){
      System.out.println(Arrays.toString(b));
    }else{

      b[n-1] = 0;
      generate(n-1, b);

      b[n-1] = 1;
      generate(n-1, b);
    }

  }
}
