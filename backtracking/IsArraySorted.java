import java.util.Arrays;

class IsArraySorted{

  public static void main(String z[]){

    System.out.println("Is array sorted?\n");

    int[] arr = {1,2,3,2,4,5,5,1,2,3};

    System.out.println("Is sorted : "+new IsArraySorted().isSorted(arr, arr.length));

    Arrays.sort(arr);

    System.out.println("Sorted array : "+Arrays.toString(arr));

    System.out.println("Is sorted : "+new IsArraySorted().isSorted(arr, arr.length));
  }

  boolean isSorted(int[] arr, int len){

    if(len == 1 || len == 0)
    return true;

    return arr[len-1] < arr[len-2] ? false : isSorted(arr,len-1);
  }
}
