class Factorial{

  public static void main(String z[]){

    System.out.println("Factorial\n");

    int num = 4;

    String format = "%s factorial : %d \nExecution time : %d ns\n";

    long factorial = 0, startTime = 0, stopTime = 0;

// Recursive
    startTime = System.nanoTime();
    factorial = new Factorial().recursive(num);
    stopTime = System.nanoTime();
    System.out.println(String.format(format, "Recursvie", factorial, (stopTime-startTime)));

// Iterative
  startTime = System.nanoTime();
  factorial = new Factorial().iterative(num);
  stopTime = System.nanoTime();
  System.out.println(String.format(format, "Iterative", factorial, (stopTime-startTime)));

  }

  public int recursive(int n){

    if(n < 0)
    return -1;

    if(n == 1)
    return 1;

    return n*recursive(n-1);
  }

  public int iterative(int n){

    if(n < 1)
    return -1;

    if(n == 1)
    return n;

    int fact = 1;

    for(int i = 2; i < n+1; ++i)
    fact *=i;

    return fact;
  }
}
