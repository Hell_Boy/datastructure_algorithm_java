import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Queue;
import java.util.LinkedList;
import java.util.Set;

class TopologicalSorting{

	public static void main(String z[]){

		System.out.println("Topological Sorting");

		Map<Integer,ArrayList<Integer>> graph = new HashMap<Integer,ArrayList<Integer>>();

		graph.put(7, new ArrayList<Integer>(){{add(11); add(8);}});
		graph.put(5, new ArrayList<Integer>(){{add(11);}});
		graph.put(3, new ArrayList<Integer>(){{add(8); add(10);}});
		graph.put(11, new ArrayList<Integer>(){{add(2); add(9); add(10);}});
		graph.put(8, new ArrayList<Integer>(){{add(9);}});
		graph.put(2, new ArrayList<Integer>());
		graph.put(9, new ArrayList<Integer>());
		graph.put(10, new ArrayList<Integer>());

		Map<Integer, Integer> indegrees = new HashMap<Integer, Integer>();


		for(Map.Entry<Integer, ArrayList<Integer>> set : graph.entrySet()){

			if(!indegrees.containsKey(set.getKey())){
				indegrees.put(set.getKey(),0);
			}

			for(int elem : set.getValue()){


				if(indegrees.containsKey(elem)){
					int temp = indegrees.get(elem);
					++temp;
					indegrees.replace(elem, temp);
				}else{
					indegrees.put(elem, 1);
				}

				

				
			}

		}

		System.out.println("\n----- Indegrees ------\n");

		Queue<Integer> q = new LinkedList<Integer>();

		for(Map.Entry<Integer, Integer> deg : indegrees.entrySet()){
			System.out.println(deg.getKey()+" : "+deg.getValue());

			if(deg.getValue() == 0)
				q.add(deg.getKey());
		}
		Queue<Integer> topologicalSort  = new LinkedList<Integer>();

		while(!q.isEmpty()){

			int top = q.poll();

			if(indegrees.get(top) == 0)
				topologicalSort.offer(top);
			else
				q.offer(top);

			for(int l: graph.get(top)){

				int temp = indegrees.get(l);
				--temp;
				indegrees.replace(l, temp);

				if(temp == 0)
				q.offer(l);
			}
		}		

		System.out.println("Topological sort : "+topologicalSort);
	}
}