
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

class TopologicalSorting2{


	private Map<Character, ArrayList<Character>> build(){
		Map<Character, ArrayList<Character>> graph = new HashMap<Character, ArrayList<Character>>();

		graph.put('a', new ArrayList<Character>(){
			{
				add('d');
			}
		});

		graph.put('b', new ArrayList<Character>(){
			{
				add('d');
			}
		});

		graph.put('c', new ArrayList<Character>());

		graph.put('d', new ArrayList<Character>(){
			{
				add('c');
			}
		});

		graph.put('e', new ArrayList<Character>(){
			{
				add('d');
			}
		});

		graph.put('f', new ArrayList<Character>(){
			{
				add('b');
				add('a');
			}
		});

		for(Map.Entry<Character, ArrayList<Character>> entry : graph.entrySet())
			System.out.println("Vertex : "+entry.getKey()+", Edges : "+entry.getValue());

		return graph;
	}

	public static void main(String z[]){

		System.out.println("Topological sorting from Cracking the coding interview");

		Map<Character, ArrayList<Character>> graph = new TopologicalSorting2().build();

		Map<Character, Integer> indegrees = new HashMap<Character,Integer>();

		for(Map.Entry<Character,ArrayList<Character>> row : graph.entrySet()){

			if(!indegrees.containsKey(row.getKey()))
				indegrees.put(row.getKey(), 0);

			for(Character elem : row.getValue()){

				if(indegrees.containsKey(elem)){
					int temp = indegrees.get(elem);
					++temp;
					indegrees.replace(elem, temp);
				}else{
					indegrees.put(elem,1);
				}
			}
		}	


		System.out.println("\n Indegrees \n");

		Queue<Character> q = new LinkedList<Character>();

		for(Map.Entry<Character,Integer> deg : indegrees.entrySet()){
			System.out.println(deg.getKey()+" : "+deg.getValue());

			if(deg.getValue() == 0)
				q.offer(deg.getKey());
		}

		Queue<Character> topologicalSort = new LinkedList<Character>();

		while(!q.isEmpty()){

			char top = q.poll();

			topologicalSort.offer(top);

			for(char c : graph.get(top)){

				int temp = indegrees.get(c);
				--temp;

				if(temp == 0)
					q.offer(c);
				else
					indegrees.replace(c, temp);
			}
		}


		System.out.println("\nTopological sort : "+topologicalSort);
	}
}