
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.Queue;

class ShortestPathUndirectedGraph{

	private Map<Character, ArrayList<Character>> build(){
		Map<Character, ArrayList<Character>> graph = new HashMap<Character, ArrayList<Character>>();

		graph.put('A', new ArrayList<Character>(){{add('B'); add('D');}});
		graph.put('B', new ArrayList<Character>(){{add('D'); add('E');}});
		graph.put('C', new ArrayList<Character>(){{add('A'); add('F');}});
		graph.put('D', new ArrayList<Character>(){{add('F'); add('G');}});
		graph.put('E', new ArrayList<Character>(){{add('G');}});
		graph.put('F', new ArrayList<Character>(){{add('G');}});
		graph.put('G', new ArrayList<Character>());


		for(Map.Entry<Character, ArrayList<Character>> entry : graph.entrySet())
			System.out.println(entry.getKey()+" : "+entry.getValue());
		
		return graph;
	}


	public static void main(String z[]){

		System.out.println("Shortest path undirected graph");

		Map<Character, ArrayList<Character>> graph = new ShortestPathUndirectedGraph().build();

		System.out.println("Vertex : "+graph.size());
		Map<Character, Boolean> visited = new HashMap<Character, Boolean>(); 

		Queue<Character> q = new LinkedList<Character>();
		q.offer('A');
		visited.put('A', true);


		List<Character> ll = new ArrayList<Character>();

		while(!q.isEmpty()){

			char c = q.poll();
			ll.add(c);

			
			for(char chars : graph.get(c)){

				if(!visited.containsKey(chars)){

					visited.put(chars, true);
					q.offer(chars);
				}
			}
			
		}


		System.out.println("Path : "+ll);
	}
}