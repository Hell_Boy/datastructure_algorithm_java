
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;


class DijkastraPrac1{

	public static void main(String z[]){

		System.out.println("Dijkastra prac");

		// int[][] graph = {
		// 	{0,5,19,0,0},
		// 	{5,0,3,11,0},
		// 	{19,3,0,2,0},
		// 	{0,11,2,0,3},
		// 	{0,0,19,3,0}
		// };

	 int[][] graph = { { 0, 4, 0, 0, 0, 0, 0, 8, 0 },
                     { 4, 0, 8, 0, 0, 0, 0, 11, 0 },
                     { 0, 8, 0, 7, 0, 4, 0, 0, 2 },
                     { 0, 0, 7, 0, 9, 14, 0, 0, 0 },
                     { 0, 0, 0, 9, 0, 10, 0, 0, 0 },
                     { 0, 0, 4, 14, 10, 0, 2, 0, 0 },
                     { 0, 0, 0, 0, 0, 2, 0, 1, 6 },
                     { 8, 11, 0, 0, 0, 0, 1, 0, 7 },
                     { 0, 0, 2, 0, 0, 0, 6, 7, 0 } };

		int[] distance = new int[graph.length];
		int[] reached = new int[graph.length];
		
		for(int i =0; i <distance.length; ++i){
			distance[i] = Integer.MAX_VALUE;
		}
	
		distance[0] = 0;

		for(int i =0; i <distance.length; ++i)

			// for undirected graph start j from 0
			for(int j =i; j <distance.length; ++j){

				if(graph[i][j] > 0){

					int temp = distance[i]+graph[i][j];

					distance[j] = distance[j] < temp ? distance[j] : temp;
					
				}

			}


		System.out.println("Distance : "+Arrays.toString(distance));
	}
}