
/**
 * StackImplementation
 */
public class StackImplementation {

    public static void main(String[] args) {
        
        ListNode head = new ListNode();
        ListNode second = new ListNode();
        ListNode third = new ListNode();
        ListNode fourth = new ListNode();
        

        head.setData(1);
        head.setNext(second);
        
        second.setData(2);
        second.setNext(third);
        
        third.setData(3);
        third.setNext(fourth);
        
        fourth.setData(4);
        fourth.setNext(null);

        System.out.println("List : ");
        new ListNode().iterate(head);
        System.out.print("\n\n\n");
        
        System.out.println("Push : ");
        ListNode temp = new ListNode().push(head, 5);
        new ListNode().iterate(temp);
        System.out.print("\n\n\n");
        
        System.out.println("Pop : ");
        ListNode temp_1 = new ListNode().pop(head);
        new ListNode().iterate(temp_1);
        System.out.print("\n\n\n");
        
        
    }
}