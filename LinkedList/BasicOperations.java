
/**
 * BasicOperations
 */
public class BasicOperations {

    public static void main(String[] args) {
        // int listElems [] = {1,2,3,4,5,6,7};
        // ListNode head = new ListNode().arrayToLinkedList(listElems);

        // ListNode temp = head;

        // Iterate Linked List
        // while (temp.getNext() != null) {
        // int data = temp.getData();
        // System.out.println("Data : " + data);
        // temp = temp.getNext();
        // }

        // Length of List
        // int length = new ListNode().length(head);
        // System.out.println("Length : " + length);

        // System.out.print("\n\n");

        // Insert at head
        // ListNode newHead = new ListNode().insertAtHead(head, 5);
        // ListNode temp1 = newHead;
        // do{
        // System.out.println("Data1 : "+ temp1.getData());
        // temp1 = temp1.getNext();
        // } while(temp1 != null);

        // System.out.println("\n\n");

        // Insert at end
        // ListNode newHead1 = new ListNode().insertAtEnd(head, 6);
        // new ListNode().iterate(newHead1);
        // System.out.println("\n\n");

        // Stack Implementation
        // Push node
        // ListNode list = new ListNode().push(head, 5);
        // new ListNode().iterate(list);

        // Pop node
        // ListNode list = new ListNode().pop(head);
        // new ListNode().iterate(list);

        // Nth node if not present sends integer min value
        // ListNode elem = new ListNode().nThNodeFromStart(head,5);
        // System.out.println(elem.getData());

        // Nth node from last
        // ListNode elem = new ListNode().nThNodeFromEnd(head, 3);
        // System.out.println(elem.getData());

        // Detecting loop
        // Creating loop in linked list
        // ListNode loop = new ListNode().createLoop(head);
        // String loopStatus = (new ListNode().detectLoopByMap(loop)) ? "Loop present." : "No Loop present.";
        // String loopStatus = (new ListNode().detectLoopFloyd(loop)) ? "Loop present." : "No Loop present.";
        // System.out.println(loopStatus);


        // Circle Start Point
        // ListNode loop = new ListNode().createLoop(head);
        // ListNode start = new ListNode().circleStartPoint(loop);
        // System.out.println("Start Point : "+start.getData());

        // Loop length
        // ListNode loop = new ListNode().createLoop(head);
        // int loopLength = new ListNode().lengthOfLoop(loop);
        // System.out.println("Loop length : "+loopLength); 
        

        // Print in reverse
        // new ListNode().printInReverse(head);


        // Intersecting Nodes
        // int list1[] = {1,2,3,4,5,6,7};
        // ListNode head1 = new ListNode().arrayToLinkedList(list1);
        // int list2[] = {11,10,9,8,5,6,7};
        // ListNode head2 = new ListNode().arrayToLinkedList(list2);
        // ListNode intersectingNodes = new ListNode().intersectingNodes(head1, head2);
        // new ListNode().iterate(intersectingNodes);   
        // System.out.println(intersectingNodes);

        // Middle Nodes
        int list1[] = {1,2,3,4,5,6,7};
        ListNode head1 = new ListNode().arrayToLinkedList(list1);
        ListNode middleNode = new ListNode().middleNode(head1);
        System.out.println("Middle Node : "+middleNode.getData());
    
   }
}