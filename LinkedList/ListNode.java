import java.util.*;

class ListNode {
    
    private int data;
    private ListNode next;
    //    public ListNode(){}
    
    // public ListNode(int data){
        //     this.data = data;
        // }
        
        public void setData(int data){
            this.data = data;
        }
        
        public int getData(){
            return this.data;
        }
        
        public void setNext(ListNode next){
            this.next = next;
        }
        
        public ListNode getNext(){
            return this.next;
        }
        
        public ListNode arrayToLinkedList(int data[]) {
        
            ListNode prevous = null;
            ListNode head = prevous;
            
            for (int i : data) {
                
                ListNode temp = new ListNode();
                temp.setData(i);
                temp.setNext(null);
        
                if(prevous != null){
                    prevous.setNext(temp);
                }else{
                    head = temp;
                }
        
                prevous = temp;
            }
        
            return head;
        }

        public int length(ListNode head){
            int length = 1;
            
            if(head == null)
            return 0;
            
            ListNode temp = head;
            while(temp.getNext() != null){
                length++;
            temp = temp.getNext();
        }

        return length;
    }

    public ListNode insertAtHead(ListNode head, int data){
        ListNode newNode = new ListNode();
        newNode.setData(data);
        newNode.setNext(head);

        return newNode;
    }

    public ListNode deleteFromHead(ListNode head){

        if(head.getNext() != null){
            head = head.getNext();
        }else{
            head = null;
        }
        return head;
    }


    public ListNode deleteFromEnd(ListNode head){

        // Todo : Not yet completed
        if(head.getNext() != null){

            do{
                head = head.getNext();
            }while(head.getNext() != null);

        }else{
            head = null;
        }

        return head;
    }


    public ListNode insertAtEnd(ListNode head, int data){
        ListNode newNode = new ListNode();
        newNode.setData(data);
        newNode.setNext(null);
        
        ListNode temp = head;
        if(temp.getNext() != null){
            while(temp.getNext()!= null){
                temp = temp.getNext();
            }
        }

        temp.setNext(newNode);

        return head;
    }

    public void iterate(ListNode head){

        ListNode temp = head;

        do{
            System.out.println("---> "+temp.getData());
            temp = temp.getNext();
        }while(temp != null);
    }

    public ListNode push(ListNode head,int data){
        return insertAtHead(head, data);
    }

    public ListNode pop(ListNode head){

        return deleteFromHead(head);
    }

    public ListNode nThNodeFromStart(ListNode head, int n){
      
        do{
            if(n-1 == 0)
            return head;

            head = head.getNext();
            n--;

        }while(head != null);

        ListNode default_Node = new ListNode();
        default_Node.setData(Integer.MIN_VALUE);
        return default_Node;
    }

    public ListNode nThNodeFromEnd(ListNode head, int n){

        int length = length(head);
        int position = (length+1) - n;

        if(position < 0){
            ListNode default_Node = new ListNode();
            default_Node.setData(Integer.MIN_VALUE);
            return default_Node;
        }

        return nThNodeFromStart(head, position);

    }

    public ListNode createLoop(ListNode head){
        // Creating loop in linked list
        head = insertAtEnd(head, Integer.MAX_VALUE);
        // temp.setNext(head);

        ListNode temp = head;

        while(temp.getNext() != null){
            temp = temp.getNext();
        }

        temp.setNext(head);

        return head;
    }

    public boolean detectLoopByMap(ListNode head){
        HashMap<ListNode, Boolean> map = new HashMap<ListNode, Boolean>();

        ListNode temp = head;
        while(temp != null){
            
            if(map.get(temp) != null){
                return true;
            }

            map.put(temp, true); 
            temp = temp.getNext();         
        }

        return false;
    }

    public boolean detectLoopFloyd(ListNode head) {
        ListNode fastPtr = head;
        ListNode slowPtr = head;

        while(fastPtr != null && fastPtr.getNext() != null){

            fastPtr = fastPtr.getNext().getNext();
            slowPtr = slowPtr.getNext();

            if(fastPtr == slowPtr){
                return true;
            }
        }

        return false;
    }

    public ListNode circleStartPoint(ListNode head){
        
        ListNode fastPtr = head;
        ListNode slowPtr = head;

        boolean loopPresent = false;

        while(fastPtr != null && fastPtr.getNext() != null){

            fastPtr = fastPtr.getNext().getNext();
            slowPtr = slowPtr.getNext();

            if(fastPtr == slowPtr){
                loopPresent = true;
                break;
            }
        }

        if(loopPresent){

            slowPtr = head;
            
            while (slowPtr != fastPtr) {
                slowPtr = slowPtr.getNext();
                fastPtr = fastPtr.getNext();
            }

            return fastPtr;
        }else{
            return null;
        }
    }

    public int lengthOfLoop(ListNode head){

        ListNode fastPtr = head;
        ListNode slowPtr = head;

        boolean loopExist = false;

        while(fastPtr != null && fastPtr.getNext() !=null){
            fastPtr = fastPtr.getNext().getNext();
            slowPtr = slowPtr.getNext();

            if(fastPtr == slowPtr){
                loopExist = true;
                break;
            }
        }


        if(loopExist){

            int length = 0;

            do{

                slowPtr = slowPtr.getNext();
                length++;
            }while(slowPtr != fastPtr);

            return length;

        }else{
            return 0;
        }
    }

    public void printInReverse(ListNode head){

        if(head == null)
        return;

        printInReverse(head.getNext());
        System.out.println("---> "+head.getData());
    }

    public ListNode intersectingNodes(ListNode list1, ListNode list2) {

        int list1_length = new ListNode().length(list1);
        int list2_length = new ListNode().length(list2);

        int diff = list1_length > list2_length ? list1_length-list2_length : list2_length - list1_length;

        ListNode head1 = list1;
        ListNode head2 = list2;

        for (int i = 0; i < diff; i++) {

            if(list1_length > list2_length){
                head1 = head1.getNext();
            }else{
                head2 = head2.getNext();
            }
        } 

        while (head1 != null && head2 != null) {
            
            if(head1.getData() == head2.getData())
            return head1;

            head1 = head1.getNext();
            head2 = head2.getNext();
        }

        return null;

    }

    public ListNode middleNode(ListNode head){

        ListNode fastPtr = head;
        ListNode slowPtr = head;

        while(fastPtr != null && fastPtr.getNext() != null){
            fastPtr = fastPtr.getNext().getNext();
            slowPtr = slowPtr.getNext();
        }

        return slowPtr;
    }

}