import java.lang.Math;

class BasicBitManipulation{

	public static void main(String z[]){


		System.out.println("Basic bit manipulation");

		long n1 = 75;
		long n2 = 21;

		// N1
		System.out.println("N1 : "+Long.toBinaryString(n1));

		// N2
		System.out.println("N2 : "+Long.toBinaryString(n2));

		// AND
		System.out.println("And : "+Long.toBinaryString(n1 & n2));

		// OR
		System.out.println("Or : "+Long.toBinaryString(n1 | n2));

		// XOR
		System.out.println("Xor : "+Long.toBinaryString(n1 ^ n2));

		// Left Shift ( << 2)
		long temp = n1;
		System.out.println("Left Shift : "+Long.toBinaryString(temp << 2));

		// Right Shift ( >> 2)
		temp = n1;
		System.out.println("Right Shift : "+Long.toBinaryString(temp >> 2));


		// Negate
		temp = n1;
		System.out.println("Negate : "+Long.toBinaryString(~temp));


		// Reverse
		System.out.println("Reverse : "+Long.toBinaryString(Long.reverse(n1)));
	}

	
}