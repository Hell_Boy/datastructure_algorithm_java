import java.util.Arrays;

class BooleanParenthesization{

  public static void main(String z[]){

    System.out.println("Boolean parenthesization");

    String symbolSequence = "TTFT";
    String operatorSequence = "|&^";

    int noOfWays = new BooleanParenthesization().waysToParenthesize(symbolSequence, operatorSequence);

    System.out.println("No of ways : "+noOfWays);
  }

  private void printMatrix(int[][] m){

    for(int[] row : m)
      System.out.println(Arrays.toString(row));
  }

  private int waysToParenthesize(String symbols, String operators){

    int n = symbols.length();

    int[][] T = new int[n][n];
    int[][] F = new int[n][n];

    // Base Case

    for(int i =0; i < n; ++i)
      if(symbols.charAt(i) == 'T')
        T[i][i] = 1;
      else
        F[i][i] = 1;

    for(int len=2; len< n+1; ++len)
      for(int i=0; i< n+1-len; ++i){

        int j = i+len-1;

        T[i][j] = 0;
        F[i][j]= 0;

        for(int k =i; k <j; ++k){
          int waysik = T[i][k] + F[i][k];
          int wayskj = T[k+1][j] + F[k+1][j];

          if(operators.charAt(k)== '&'){
            T[i][j] += T[i][k]*T[k+1][j];
            F[i][j] += (waysik*wayskj - T[i][k]*T[k+1][j]);
          }
          else if(operators.charAt(k) == '|'){
            F[i][j] += F[i][k]*F[k+1][j];
            T[i][j] += (waysik*wayskj - F[i][k]*F[k+1][j]);
          }else{
            T[i][j] += F[i][k]*T[k+1][j] + T[i][k]*F[k+1][j];
            F[i][j] += T[i][k]*T[k+1][j] + F[i][k]*F[k+1][j];
          }
        }

      }

      System.out.println("True");
      printMatrix(T);

      System.out.println("\nFalse");
      printMatrix(F);

    return T[0][n-1];
  }
}
