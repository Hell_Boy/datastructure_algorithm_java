import java.util.Arrays;

class EditDistance{

  public static void main(String z[]){

    System.out.println("Edit distance");

    String s1 = "hello";
    String s2 = "world";


    int[][] T = new int[s1.length()+1][s2.length()+1];

    for(int i =1; i < s1.length()+1; ++i)
      for(int j = 1; j < s2.length()+1; ++j){

        if(s1.charAt(i-1) == s2.charAt(j-1))
          T[i][j] = T[i-1][j-1];
        else
         T[i][j] = 1+ Math.min(T[i-1][j-1], Math.min(T[i-1][j], T[i][j-1]));
      }

    for(int[] row : T)
      System.out.println("Cost : "+Arrays.toString(row));
  }
}
