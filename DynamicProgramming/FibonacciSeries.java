

class FibonacciSeries{

  public static void main(String z[]){

    int n = 20;
    long start = 0, stop= 0;

    start = System.nanoTime();
    int recursiveSum = new FibonacciSeries().fibonacciSeriesSum(n);
    System.out.println("Recursive Sum : "+recursiveSum);
    stop = System.nanoTime();
    System.out.println("Execution time : "+(stop-start));
    System.out.println("-----------------");

    start = System.nanoTime();
    int memoizationSum = new FibonacciSeries().memoization(n, new int[n+1]);
    System.out.println("\nMemoization Sum : "+memoizationSum);
    stop = System.nanoTime();
    System.out.println("Execution time : "+(stop-start));
    System.out.println("-----------------");


    start = System.nanoTime();
    int dynamicProgrammingSum = new FibonacciSeries().dynamicProgramming(n);
    System.out.println("\nDynamic Programming Sum : "+dynamicProgrammingSum);
    stop = System.nanoTime();
    System.out.println("Execution time : "+(stop-start));
    System.out.println("-----------------");

    start = System.nanoTime();
    int twoNumSums = new FibonacciSeries().twoNum(n);
    System.out.println("\nTwo Num Sum : "+twoNumSums);
    stop = System.nanoTime();
    System.out.println("Execution time : "+(stop-start));
    System.out.println("-----------------");


  }

  public int fibonacciSeriesSum(int n){

    if(n == 0 || n == 1)
    return n;

    return fibonacciSeriesSum(n-1) + fibonacciSeriesSum(n-2);
  }

  public int memoization(int n, int[] m){

    if(n < 2)
    return n;

    if(m[n] == 0)
      m[n] = memoization(n-1, m) + memoization(n-2, m);

    return m [n];
  }

  public int dynamicProgramming(int n){

    if(n < 2)
    return n;

    int[] m = new int[n+1];

    m[0] = 0;
    m[1] = 1;

    for(int i =2; i < n+1; ++i)
      m[i] = m[i-1] + m[i-2];

    return m[n];
  }

  public int twoNum(int n){

    if(n < 2)
    return n;

    int a =0, b = 1, c = 0;

    for(int i = 2; i < n+1; ++i){
      c = a+b;
      a = b;
      b = c;
    }

    return b;
  }
}
