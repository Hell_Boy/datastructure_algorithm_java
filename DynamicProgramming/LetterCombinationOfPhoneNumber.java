import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;

class LetterCombinationOfPhoneNumber {

  public static void main(String z[]) {

    System.out.println("Letter combination of phone number");

    long start, stop;

    start = System.nanoTime();
    ArrayList<String> result = new LetterCombinationOfPhoneNumber().generateCombination("234");
    stop = System.nanoTime();

    System.out.println("Result : " + result + "\nsize : " + result.size()+"\nduration : "+(stop-start));
  }

  private ArrayList<String> generateCombination(String digit) {

    Map<String, String> keyMap = new HashMap<String, String>();
    keyMap.put("2", "abc");
    keyMap.put("3", "def");
    keyMap.put("4", "ghi");
    keyMap.put("5", "jkl");
    keyMap.put("6", "mno");
    keyMap.put("7", "pqrs");
    keyMap.put("8", "tuv");
    keyMap.put("9", "wxyz");

    ArrayList<String> list = new ArrayList();

    if (digit.length() == 0)
      return list;

    for (char c : keyMap.get(digit.charAt(0) + "").toCharArray())
      list.add(c + "");

    if (digit.length() == 1)
      return list;

    ArrayList<String> list2 = new ArrayList<String>();
    for (int k =1; k <digit.length(); ++k) {
      String s = keyMap.get(digit.charAt(k) + "");
      for (char c2 : s.toCharArray()) {
        for (int i = 0; i < list.size(); ++i) {
          String temp = list.get(i);
          temp += c2 + "";
          list2.add(temp);
        }
      }

      list.clear();
      list.addAll(list2);
      list2.clear();
    }

    return list;

  }

}
