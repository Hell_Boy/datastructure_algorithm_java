import java.util.ArrayList;

class PowerSet{

  public static void main(String z[]){

    System.out.println("Power set");

    ArrayList<Integer> elems = new ArrayList<Integer>();
    elems.add(1);
    elems.add(2);
    elems.add(3);

    // System.out.println(elems);

    ArrayList<ArrayList<Integer>> powerSet = new PowerSet().methodOne(elems, 0);
    System.out.println(powerSet);
  }

  private ArrayList<ArrayList<Integer>> methodOne(ArrayList<Integer> list, int index){

    ArrayList<ArrayList<Integer>> allSubsets;

    if(index == list.size()){
      allSubsets = new ArrayList<ArrayList<Integer>>();
      allSubsets.add(new ArrayList<Integer>());
    }else{

      allSubsets = methodOne(list, index+1);
      int item = list.get(index);
      ArrayList<ArrayList<Integer>> moreSubsets = new ArrayList<ArrayList<Integer>>();

      for(ArrayList<Integer> subs : allSubsets){

        ArrayList<Integer> newSubs = new ArrayList<Integer>();
        newSubs.addAll(subs);
        newSubs.add(item);
        moreSubsets.add(newSubs);
      }


      allSubsets.addAll(moreSubsets);
      // System.out.println("subs : "+moreSubsets);
      // System.out.println("all sets : "+allSubsets);
    }

    return allSubsets;
  }
}
