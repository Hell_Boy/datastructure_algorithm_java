import java.util.Arrays;

class PallindromicSubsequence{

  public static void main(String z[]){

    System.out.println("Pallindromic subsequence");

    // String s = "agtcmctga";
    // String s = "abcd";
    String s = "euazbipzncptldueeuechubrcourfpftcebikrxhybkymimgvldiwqvkszfycvqyvtiwfckexmowcxztkfyzqovbtmzpxojfofbvwnncajvrvdbvjhcrameamcfmcoxryjukhpljwszknhiypvyskmsujkuggpztltpgoczafmfelahqwjbhxtjmebnymdyxoeodqmvkxittxjnlltmoobsgzdfhismogqfpfhvqnxeuosjqqalvwhsidgiavcatjjgeztrjuoixxxoznklcxolgpuktirmduxdywwlbikaqkqajzbsjvdgjcnbtfksqhquiwnwflkldgdrqrnwmshdpykicozfowmumzeuznolmgjlltypyufpzjpuvucmesnnrwppheizkapovoloneaxpfinaontwtdqsdvzmqlgkdxlbeguackbdkftzbnynmcejtwudocemcfnuzbttcoew";
    // String s = "bbbab";
    // String s = "acggggca";
    // String s = "aaa";

    int len = 0;


    len = new PallindromicSubsequence().iterative(s);

    System.out.println(len);

    // len = new PallindromicSubsequence().longestPalindromeSubseq(s);
    //
    // System.out.println(len);


  }

  public int iterative(String s){

    char[] A = s.toCharArray();
    int n = A.length;

    int[][] L = new int[n][n];
    int i, k, j;

    for(i =0; i < n; ++i)
    L[i][i] = 1;

    for( k =2; k <= n ; ++k)
      for( i = 0; i < n-k+1; ++i){

         j = i+k-1;

        if(k == 2 && A[i] == A[j])
          L[i][j] = 2;

        else if(A[i] == A[j])
          L[i][j] = L[i+1][j-1]+2;

        else
           L[i][j] = Math.max(L[i+1][j], L[i][j-1]);
      }

    // Log

    for(int[] l : L)
      System.out.println(Arrays.toString(l));

    return L[0][n-1];
  }

  public int longestPalindromeSubseq(String s) {
    int n = s.length();
    Integer[][] dp = new Integer[n][n];
    return explore(0, n-1, s, dp);
  }

  public int explore(int i, int j, String s, Integer[][] dp) {
    if(i > j) return 0;
    if(i == j) return 1;
    if(dp[i][j] != null) return dp[i][j];
    char c1 = s.charAt(i);
    char c2 = s.charAt(j);

    if(c1 == c2) {
      int res = 2 + explore(i+1, j-1, s, dp);
      dp[i][j] = res;
      return res;
    }

    int res1 = explore(i+1, j, s, dp);
    int res2 = explore(i, j-1, s, dp);

    int max = Math.max(res1, res2);
    dp[i][j] = max;
    return max;
  }
}
