import java.util.Arrays;

class TripleSteps{

  static int[] nums;

  public static void main(String z[]){

    System.out.println("Triple Steps");

    long start, stop;
    int n = 30, ways;

    nums = new int[n+1];

    Arrays.fill(nums, -1);

    start = System.nanoTime();
    ways = new TripleSteps().solution1(n);
    stop = System.nanoTime();
    System.out.println("Elapsed  : "+(stop-start));
    System.out.println("No. of ways : "+ways);

    start = System.nanoTime();
    ways = new TripleSteps().solution2(n);
    stop = System.nanoTime();
    System.out.println("Elapsed  : "+(stop-start));
    System.out.println("No. of ways : "+ways);


  }

  private int solution1(int n){

    if(n < 0)
    return 0;

    if(n == 0)
    return 1;

    return solution1(n-1)+solution1(n-2)+solution1(n-3);

  }

  private int solution2(int n){

    if(n < 0)
    return 0;

    if(n == 0)
    return 1;


    if(nums[n] > 0)
    return nums[n];

    nums[n] = solution2(n-1)+solution2(n-2)+solution2(n-3);

    return nums[n];
  }
}
