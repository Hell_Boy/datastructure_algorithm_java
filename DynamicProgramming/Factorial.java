
class Factorial{

  public static void main(String z[]){

    System.out.println("Factorial\n");

    int n = 10, fact;
    long start = 0, stop = 0;
    String format = "%s fact : %d\nExecution time : %d\n";


    start = System.nanoTime();
    fact = new Factorial().recursion(n);
    stop = System.nanoTime();
    System.out.println(String.format(format, "Recursion", fact, (stop-start)));

    start = System.nanoTime();
    fact = new Factorial().memoization(n, new int[n+1]);
    stop = System.nanoTime();
    System.out.println(String.format(format, "Memoization", fact, (stop-start)));

    start = System.nanoTime();
    fact = new Factorial().iterative(n);
    stop = System.nanoTime();
    System.out.println(String.format(format, "Iterative", fact, (stop-start)));

  }

  private int recursion(int n){

    if(n < 2)
    return 1;

    return n * recursion(n-1);

  }

  private int memoization(int n , int[] m){

    if(n < 2)
    return 1;

    if(m[n] == 0)
      m[n] = n* memoization(n-1, m);

    return m[n];
  }

  private int iterative(int n){

    if(n < 2)
    return 1;

    int fact = 1;
    for(int i = 2; i < n+1; ++i)
      fact *=i;

    return fact;
  }
}
