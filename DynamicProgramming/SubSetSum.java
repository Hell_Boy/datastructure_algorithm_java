import java.util.Arrays;

class SubSetSum{

  public static void main(String z[]){

    System.out.println("Subset sum\n");

    // int sum = 17;
    // int[] a = {3,2,4,19,3,7,13,10,6,11};

    int sum = 11;
    int[] a = {1,5,11,5};
    long start, stop;

    SubSetSum s = new SubSetSum();
    String exec = "Code execution time : %d";

    start = System.nanoTime();
    s.method1(a, sum);
    stop = System.nanoTime();
    System.out.println(String.format(exec, stop-start));

    System.out.println("\n");
    start = System.nanoTime();
    s.method2(a, sum);
    stop = System.nanoTime();
    System.out.println(String.format(exec, stop-start));


    System.out.println("\n");
    start = System.nanoTime();
    s.method3(a, sum);
    stop = System.nanoTime();
    System.out.println(String.format(exec, stop-start));


  }

  private void method1(int[] a, int sum){
    boolean[][] m = new boolean[a.length+1][sum+1];

    for(int i =0; i < a.length+1; ++i)
      m[i][0] = true;


    for(int i =1; i < a.length+1; ++i)
      for(int j = 1; j < sum+1; ++j){

        if(j < a[i-1])
          m[i][j] = m[i-1][j];
        else
          m[i][j] = m[i-1][j-a[i-1]];
      }

    // for(boolean[] t : m)
    //   System.out.println(Arrays.toString(t));

    System.out.println("\nSubset sum exists : "+m[a.length][sum]);
  }

  private void method2(int[] a, int b){

    int sum = 0;

    for(int val : a)
      sum+=val;

    boolean[] T = new boolean[sum+1];
    T[0] = true;

    for(int i =1; i < a.length+1; ++i)
      for(int j = sum-a[i-1]; j >-1; --j)
        if(T[j])
          T[j+a[i-1]] = true;

    // for(int k=0; k < T.length; ++k)
    //   System.out.println(k +" => "+T[k]);

    if(T[b])
      System.out.println("Sum of number "+b+" can be formed from : "+Arrays.toString(a));

  }

  private void method3(int[] a, int b){

    int sum = 0;

    for(int val : a)
      sum+= val;

    boolean[] T = new boolean[sum+1];
    T[0] = true;

    int R = 0;

    for(int i =0; i < a.length; ++i)
      for(int j = R; j > -1; --j){

        if(T[j])
          T[j+a[i]] = true;

        R = Math.min(sum/2, R+a[i]);
      }

    // for(int k =0; k < T.length; ++k)
    //   System.out.println(k+" value : "+T[k]);

    System.out.println((sum/2)+" sum exists : "+T[sum/2]);

  }
}
