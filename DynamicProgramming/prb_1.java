
class prb_1{

  public static void main(String z[]){

    System.out.println("Problem 1\n");

    int n = 15;
    long start, stop;

    start = System.nanoTime();
    int solVal = new prb_1().sol(n);
    stop = System.nanoTime();
    System.out.println("Solution val : "+solVal);
    System.out.println("Execution time : "+(stop-start));


    start = System.nanoTime();
    solVal = new prb_1().dynamicProgramming(n, new int[n+1]);
    stop = System.nanoTime();
    System.out.println("Solution val : "+solVal);
    System.out.println("Execution time : "+(stop-start));


    start = System.nanoTime();
    solVal = new prb_1().optimization_3(n);
    stop = System.nanoTime();
    System.out.println("Solution val : "+solVal);
    System.out.println("Execution time : "+(stop-start));
  }

  private int sol(int n){

    int sum = 0;

    if(n == 0 || n == 1)
    return 2;

    for(int i = 1; i < n; ++i)
      sum +=2 * sol(i) * sol(i-1);
    return sum;
  }


  private int dynamicProgramming(int n, int[] mem){

    int sum = 0;

    if(n==0 || n ==1)
    return 2;

    for(int i = 1; i < n; ++i){

      if(mem[i] == 0){
        sum += 2* sol(i) * sol(i-1);
        mem[i] = sum;
      }

      sum = mem[i];
    }
    return sum;
  }

  private int optimization_3(int n){

    int[] mem = new int[n+1];
    mem[0] = mem[1] = 2;
    mem[2] = 2 * mem[0] * mem[1];

    for(int i = 3; i <= n ; ++i)
    mem[i] = mem[i-1] + 2 * mem[i-1] * mem[i-2];

    return mem[n];
  }
}
