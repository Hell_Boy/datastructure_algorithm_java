import java.util.Arrays;

class UniquePaths{

  static int count = 0;

  public static void main(String z[]){

    System.out.println("Unique paths");

    int m = 3, n = 3;
    // int m = 3, n = 2;

    // boolean[][] visited = new boolean[m][n];
    //
    // new UniquePaths().paths(m-1,n-1,visited);
    //
    // System.out.println("Count : "+ count);


    long start, stop;

    int[][] dp = new int[m][n];
    //   for(int i = 0; i <= m; i++)
    //       Arrays.fill(dp[i], -1);
    //
    // start = System.nanoTime();
    // count= new UniquePaths().helper(0, 0, dp, m, n);
    // stop = System.nanoTime();

    for(int i =0; i < m; ++i)
      Arrays.fill(dp[i], Integer.MIN_VALUE);

    start = System.nanoTime();
    count = new UniquePaths().pathsSec(m-1,n-1,dp);
    stop = System.nanoTime();
    System.out.println("Ways : "+count);
    System.out.println("Time Elapsed : "+(stop-start));

  }

  private int pathsSec(int r, int c, int[][] memo){

    if(r < 0 || c <0)
    return 0;

    if(r == 0 && c == 0)
    return 1;

    if(memo[r][c]==Integer.MIN_VALUE)
      memo[r][c] =  pathsSec(r-1, c, memo) + pathsSec(r,c-1,memo);


    return memo[r][c];
  }

  private boolean paths(int r, int c, boolean[][] visited){

    if(r < 0 || c < 0)
    return false;

    if(r == 0 && c == 0){
      count +=1;
      return true;
    }

    if(visited[r][c]){
      return visited[r][c];
    }
    visited[r][c]= paths(r-1, c,  visited) || paths(r, c-1, visited);

    return visited[r][c];
  }


  private int helper(int i, int j, int[][] dp, int m, int n){
    if(i == m-1 && j == n-1) return 1;
    if(i >= m || j>= n || i < 0 || j < 0) return 0;
    if(dp[i][j] != -1) return dp[i][j];

    dp[i][j] = helper(i+1, j, dp, m, n) + helper(i, j+1, dp, m, n);

    return dp[i][j];
}
}
