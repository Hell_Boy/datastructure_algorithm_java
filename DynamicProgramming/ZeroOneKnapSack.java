import java.util.Arrays;

class ZeroOneKnapSack{

  public static void main(String z[]){

    System.out.println("Zero one knapsack\n");

    int[] v = {1,2,5,6};
    int[] wt = {2,3,4,5};

    int w = 8;

    int[][] dp = new int[v.length+1][w+1];

    for(int i = 0; i < dp.length; ++i)
      for(int j = 0; j < dp[0].length; ++j){

        if(i == 0 || j == 0)
        dp[i][j] = 0;

        else if(wt[i-1] < j+1){
          // calculation
          dp[i][j] = Math.max(dp[i-1][j], dp[i-1][j-wt[i-1]]+v[i-1]);
        }

        else
          dp[i][j] = dp[i-1][j];

      }

      for(int[] val : dp)
        System.out.println(Arrays.toString(val));


    // Objects taken

    for(int i = dp.length-1, j = dp[0].length-1; i > 0 && j >0;){

      if(dp[i][j] == dp[i-1][j]){
        // System.out.println("item with weight "+wt[i-1]+" not included");

      }else{
          System.out.println("item with weight "+wt[i-1]+" is included");
          j -= wt[i-1];
      }
      --i;
    }
  }
}
