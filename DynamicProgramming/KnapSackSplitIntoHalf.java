// https://leetcode.com/problems/partition-equal-subset-sum/
import java.util.Arrays;

class KnapSackSplitIntoHalf{

  public static void main(String z[]){

    System.out.println("KnapSack split into half");

    int[] P = {1,5,11,5};

    boolean value = new KnapSackSplitIntoHalf().solution(P);

    System.out.println("Can split : "+value);

  }

  private boolean solution(int[] P){

    int sum  =0, i =0, j = 0;

    for(int k : P)
      sum +=k;

    if(sum%2 != 0)
      return false;

    sum /=2;

    int[][] dp = new int[P.length+1][sum+1];

    for(i = 0; i < P.length+1; ++i)
      for(j = 0; j < sum+1; ++j){

        if(i == 0 || j == 0)
          dp[i][j] = 0;

        else if(P[i-1] < j+1)
          dp[i][j] = Math.max(dp[i-1][j], dp[i-1][j-P[i-1]] + P[i-1]);

        else
          dp[i][j] = dp[i-1][j];
      }

    for(int[] row : dp)
      System.out.println(Arrays.toString(row));

    System.out.println("Last elem : "+dp[i-1][j-1]);

    return dp[i-1][j-1] == sum;
  }
}
