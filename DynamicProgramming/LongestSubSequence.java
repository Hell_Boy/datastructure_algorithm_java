import java.util.Arrays;
import java.lang.StringBuilder;

class LongestSubSequence{

  public static void main(String z[]){

    System.out.println("Longest Subsequence\n");

    // String a = "abcbdab", b = "bdcaba";
    String a = "abcde", b = "ace";

    long start, stop;

    start = System.nanoTime();
    int subSeqLen = new LongestSubSequence().maxLen(a,b, a.length(), b.length(), 0, 0);
    System.out.println("Subsequence len : "+subSeqLen);
    stop = System.nanoTime();
    System.out.println("Execution time : "+(stop-start)+"\n");

    start = System.nanoTime();
    String subSeq = new LongestSubSequence().subSeqString(a, b, 0, 0);
    System.out.println("Subsequence string : "+subSeq);
    stop = System.nanoTime();
    System.out.println("Execution time : "+(stop-start)+"\n");

    start = System.nanoTime();
    new LongestSubSequence().dynamicProgramming(a, b);
    stop = System.nanoTime();
    System.out.println("Execution time : "+(stop-start)+"\n");

  }

  private int maxLen(String a, String b, int alen, int blen, int m, int n){

    if(alen == m || blen == n || alen == 0 || blen == 0)
    return 0;

    if(a.charAt(m) == b.charAt(n))
      return 1+maxLen(a, b, alen, blen, m+1, n+1);

    return Math.max(maxLen(a, b, alen, blen, m+1, n), maxLen(a, b, alen, blen, m, n+1));
  }

  private String subSeqString(String a, String b, int i, int j){

    if(a.length() == 0 || b.length() == 0 || a.length() == i || b.length() == j)
    return "";

    if(a.charAt(i) == b.charAt(j))
    return a.charAt(i) + subSeqString(a, b, i+1, j+1);

    String x = subSeqString(a, b, i+1, j);
    String y = subSeqString(a, b, i, j+1);

    return x.length() > y.length() ? x : y;
  }

  private void dynamicProgramming(String a, String b){

    int[][] map = new int[a.length()][b.length()];
    int i, j;

    for(i =0; i < map.length; ++i)
          if(a.charAt(i) == b.charAt(0))
            map[i][0] = 1;
          else if(i > 0)
              map[i][0] = map[i-1][0];


        for(j =0; j < map[0].length; ++j)
          if(a.charAt(0) == b.charAt(j))
            map[0][j] = 1;
          else if(j > 0)
              map[0][j] = map[0][j-1];


        for(i = 1; i < map.length; ++i)
          for(j = 1; j < map[0].length; ++j){

            if(a.charAt(i) == b.charAt(j))
              map[i][j] = map[i-1][j-1]+1;

            else
              map[i][j] = Math.max(map[i-1][j], map[i][j-1]);
          }

    for(int[] row : map)
      System.out.println(Arrays.toString(row));

    i = map.length-1;
    j = map[0].length-1;

    StringBuilder sb = new StringBuilder();

    while(i > -1 && j > -1){

      if(i-1 > -1 && j - 1 > -1 ){

        if(map[i][j-1] != map[i][j]){
          sb.append(b.charAt(j));
          --i;
        }

      }else if(map[i][j] > 0){
        sb.append(b.charAt(j));
      }

      --j;
    }

    System.out.println(sb.reverse());

  }

}
