
class CatlanNumber{

  public static void main(String z[]){

    System.out.println("Catlan number\n");

    int n = 13, counts = 0;

    long start, stop;

    start = System.nanoTime();
    counts = new CatlanNumber().recursive(n);
    stop = System.nanoTime();
    System.out.println("Recursive  : "+counts);
    System.out.println("Execution Time : "+(stop-start));


    start = System.nanoTime();
    int[] m = new int[n+1];
    m[0] = 1;
    counts = new CatlanNumber().dynamicProgramming(n,m);
    stop = System.nanoTime();
    System.out.println("Dynamic Programming  : "+counts);
    System.out.println("Execution Time : "+(stop-start));

  }

  private int recursive(int n){

    if(n  == 0)
      return 1;

    int count = 0;

    for(int i = 1; i < n+1; ++i)
        count += recursive(i-1) * recursive(n-i);

    return count;
  }

  private int dynamicProgramming(int n, int[] m){

    if(m[n] != 0)
      return m[n];

    int count = 0;
    for(int i = 1; i < n+1; ++i)
      count += dynamicProgramming(i-1, m) * dynamicProgramming(n-i, m);

    m[n] = count;

    return m[n];
  }
}
