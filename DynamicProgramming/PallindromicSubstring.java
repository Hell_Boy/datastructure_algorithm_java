import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Arrays;

class PallindromicSubstring{

  public static void main(String z[]) throws IOException{

    System.out.println("Pallindromic substring");

    // BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    //
    // String input = br.readLine();

    // System.out.println(input);

    String s = "bbbab";

    int len = new PallindromicSubstring().longestPallindrome(s);

    System.out.println("Len : "+len);
  }

  private int longestPallindrome(String s){

    char[] A = s.toCharArray();
    int n = A.length;
    int[][] L = new int[n][n];

    int max = 1, i, j, k;

    for(i =0; i < n; ++i)
      L[i][i] = 1;


    for(k = 2; k <= n; ++k)
      for(i = 0; i < n-k+1; ++i){

        j = i+k-1;

        if(A[i] == A[j] && L[i+1][j-1] == 1){
          L[i][j] = 1;
          max = k;
        }

      }

      // if(i+1 < n && A[i] == A[i+1]){
      //   L[i][i+1] = 1;
      //   max = 2;
      // }else{
      //   L[i][i] = 0;
      // }


    for(int[] l : L)
      System.out.println(Arrays.toString(l));



    return max;
  }
}
