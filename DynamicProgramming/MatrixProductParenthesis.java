import java.util.Arrays;

class MatrixProductParenthesis{

  public static void main(String z[]){

    System.out.println("Matrix product parenthesis");

    int[] P = {4, 10, 3, 12, 20, 7};
    // new MatrixProductParenthesis().matrixChainOrder(P);
    new MatrixProductParenthesis().matrixChainOrder2(P);
  }

  private void matrixChainOrder2(int[] P){

    int n = P.length;
    int[][] M = new int[n][n];
    int[][] S = new int[n][n];

    for(int l =2; l <n; ++l){

      for(int i = 1; i < n-l+1; ++i){

        int j = i+l-1;

        M[i][j] = Integer.MAX_VALUE;

        for(int k = i; k < j; ++k){

          int count = M[i][k] + M[k+1][j] + P[i-1] * P[k] * P[j];

          if(count < M[i][j]){
            M[i][j] = count;
            S[i][j] = k;
          }
        }
      }
    }


    System.out.println("\nM");
    for(int[] m : M)
      System.out.println(Arrays.toString(m));

    System.out.println("\nN");
    for(int[] s: S)
      System.out.println(Arrays.toString(s));
  }

  private void matrixChainOrder(int[] P){
    int n = P.length;
    int[][]M = new int[n][n];
    int[][]S = new int[n][n];

    // for(int i = 1; i <n; ++i)
    //   M[i][i] = 0;

    for(int l =2; l <n; ++l){

      for(int i =1; i <=n-l; ++i){

        int j = i+l-1;
        M[i][j] = Integer.MAX_VALUE;

        for(int k = i; k <= j-1; ++k){

          System.out.println("l : "+l+" i : "+i+" j : "+j+" k : "+k);

          int cost = M[i][k] + M[k+1][j] + P[i-1] * P[k] * P[j];

          if(cost < M[i][j]){
            M[i][j] = cost;
            S[i][j] = k;
          }
        }
      }
    }

    System.out.println("\nM");
    for(int[] m : M)
      System.out.println(Arrays.toString(m));

    System.out.println("\nN");
    for(int[] s: S)
      System.out.println(Arrays.toString(s));
  }
}
