import java.util.Arrays;

class MinimumNumberOfCoins{

  public static void main(String z[]){

    System.out.println("Minimum number of coins exchange");

    int n = 8;
    int[] coins = {1,5,10};
    int[] t = new int[n+1];

    Arrays.fill(t, -1);
    t[0] = 0;

    int reqCoins = new MinimumNumberOfCoins().minCoins(coins, coins.length, n, t);
    System.out.println("Req coins : "+reqCoins);
  }


  private int minCoins(int[] coins, int m, int V, int[] t){

    if (t[V] != -1) return t[V];

    int res = Integer.MAX_VALUE;

    for (int i=0; i<m; i++)
    {
      if (coins[i] <= V)
      {
          int sub_res = minCoins(coins, m, V-coins[i], t);

          if (sub_res != Integer.MAX_VALUE && sub_res + 1 < res)
             res = sub_res + 1;
      }
    }

    t[V] = res;
    return t[V];
  }
}
