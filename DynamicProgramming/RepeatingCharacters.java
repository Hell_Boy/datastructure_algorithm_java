import java.util.Arrays;


class RepeatingCharacters{

  public static void main(String z[]){

    System.out.println("Repeating characters");

    String s1 = "ab";
    // char[] c1 = s1.toCharArray();

    String s2 = "abadcb";
    // char[] c2 = s2.toCharArray();

    int len = new RepeatingCharacters().charLen(s1, s2);
    System.out.println("Len : "+len);
  }

  private int charLen(String s1, String s2){

    char[] c1 = s1.toCharArray();
    char[] c2 = s2.toCharArray();

    if(c2.length == 0)
    return 0;

// Common empty string
    if(c1.length == 0)
    return 1;

    int[][] L = new int[c1.length+1][c2.length+1];

    for(int i =1; i < c2.length+1; ++i)
      L[0][i] = 1;

    for(int i =1; i < c1.length+1; ++i)
      for(int j =1; j < c2.length+1; ++j){

        if(c1[i-1] == c2[j-1]){
          L[i][j] = Math.max((L[i-1][j-1]+L[i][j-1]), L[i][j-1]);
        }else{
          L[i][j] =Math.max(L[i-1][j], L[i][j-1]);
        }
      }

    for(int[] l : L)
      System.out.println(Arrays.toString(l));

    return L[c1.length][c2.length];
  }
}
