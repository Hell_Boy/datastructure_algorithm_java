
class MaximumContigiousSum{

  public static void main(String z[]){

    System.out.println("Maximum contigious sum\n");

    // int[] a = {-2, 11, -4, 13, -5, 2};
    int[] a = {1,2,3,1};
    int maxSum =0;

    long start, stop;

    start = System.nanoTime();
    maxSum = new MaximumContigiousSum().bruteForce(a);
    stop = System.nanoTime();
    System.out.println("Max sum : "+maxSum);
    System.out.println("Execution time : "+(stop-start));

    start = System.nanoTime();
    maxSum = new MaximumContigiousSum().dynamicProgramming(a);
    stop = System.nanoTime();
    System.out.println("Max sum : "+maxSum);
    System.out.println("Execution time : "+(stop-start));

    start = System.nanoTime();
    maxSum = new MaximumContigiousSum().dp_optimised(a);
    stop = System.nanoTime();
    System.out.println("Max sum : "+maxSum);
    System.out.println("Execution time : "+(stop-start));



    start = System.nanoTime();
    maxSum = new MaximumContigiousSum().skipping_two_continuous(a);
    stop = System.nanoTime();
    System.out.println("Max sum : "+maxSum);
    System.out.println("Execution time : "+(stop-start));


    start = System.nanoTime();
    maxSum = new MaximumContigiousSum().skipping_three_continuous(a);
    stop = System.nanoTime();
    System.out.println("Max sum : "+maxSum);
    System.out.println("Execution time : "+(stop-start));



  }

  private int bruteForce(int[] a ){

    int maxSum = 0;

    for(int i =0; i < a.length; ++i){

      int localSum = 0;

      for(int j =i; j < a.length; ++j){
        localSum +=a[j];
        maxSum = Math.max(localSum, maxSum);
      }
    }

    return maxSum;
  }

  private int dynamicProgramming(int[] a){

    int maxSum = a[0], localMax = 0;

    int[] temp = new int[a.length];
    temp[0] = maxSum;

    for(int i =1; i < temp.length; ++i){
      temp[i] = Math.max(temp[i-1]+a[i], a[i]);
      maxSum = Math.max(maxSum, temp[i]);
    }
    return maxSum;
  }

  private int dp_optimised(int[] a){

    int maxSum = a[0], sum = a[0];

    for(int i = 1; i < a.length; ++i){

      sum = Math.max(sum+a[i], a[i]);
      maxSum = Math.max(maxSum, sum);
    }
    return maxSum;
  }

  private int skipping_two_continuous(int[] a ){

    if(a.length == 1)
    return a[0];

    if(a.length == 2)
    return Math.max(a[0],a[1]);

    int maxSum_1 = Math.max(a[0], a[1]), maxSum_2 = a[0], temp = 0;

    for(int i = 2; i < a.length; ++i){
      temp = maxSum_1;
      maxSum_1 = Math.max(maxSum_1, a[i]+maxSum_2);
      maxSum_2 = temp;
    }

    return maxSum_1;
  }

  private int skipping_three_continuous(int[] a){


    if(a.length == 1)
    return a[0];

    if(a.length == 2)
    return Math.max(a[0]+a[1], Math.max(a[0], a[1]));

    int maxCurrent_2 = Math.max(a[0]+a[1], Math.max(a[0], a[1])), maxCurrent_3 = a[0];

    int maxCurrent_1 = Math.max(maxCurrent_3+a[2], maxCurrent_2), temp1, temp2;

    for(int i = 3; i < a.length; ++i){
      temp1 = maxCurrent_1;
      temp2 = maxCurrent_2;
      maxCurrent_1 = Math.max(a[i-1]+a[i-2]+maxCurrent_3, Math.max(a[i]+maxCurrent_2, maxCurrent_1));
      maxCurrent_2 = temp1;
      maxCurrent_3 = temp2;
    }
    return maxCurrent_1;
  }
}
