
import java.util.Arrays;

class OnesAndZeros{

  public static void main(String z[]){

    System.out.println("Ones and zeros");

    String[] strs = {"10","0001","111001","1","0"};
    // String[] strs = {"10","0","1"};
    int count = new OnesAndZeros().counts(strs, 5, 3);

    System.out.println("Count : "+count);
  }

  private int counts(String[] strs, int m, int n){

    int[] count = new int[strs.length+1];

    int max = Math.max(m,n);
    int p = 1;

    for(String s : strs){

      int tempM = 0, tempN = 0;

        for(int ascii : s.toCharArray()){

        if(ascii == 48)
          ++tempM;
        else
          ++tempN;
      }


      if(tempM+tempN < max+1 && tempM < m+1 && tempN < n+1)
        count[p] = count[p-1] + 1;
      else
        count[p] = count[p-1];
      ++p;
    }

    return count[p-1];
  }
}
