
import java.util.Arrays;

class AllPairShortestPath{

  public static void main(String z[]){

    System.out.println("All pair shortest path");

    int[][] matrix = {
      {0,3,9999,7},
      {8,0,2,9999},
      {5,9999,0,1},
      {2,9999,9999,0}
    };

    new AllPairShortestPath().printMatrix(matrix);

    int len = matrix.length;

    for(int k =0; k < len; ++k){

      for(int i =0; i < len; ++i)
      for(int j =0; j < len; ++j){

        matrix[i][j] = Math.min(matrix[i][j], matrix[i][k]+matrix[k][j]);
      }
    }
    new AllPairShortestPath().printMatrix(matrix);
  }

  private void printMatrix(int[][] matrix){
    // Print matrix
    System.out.println("\n");
    
    for(int[] row : matrix)
      System.out.println(Arrays.toString(row));

    System.out.println("\n");
  }
}
