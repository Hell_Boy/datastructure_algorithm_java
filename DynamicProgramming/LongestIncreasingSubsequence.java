import java.util.Arrays;

class LongestIncreasingSubsequence{

  public static void main(String z[]){
    System.out.println("Longest increasing subsequence");

    // int[] a = {5,6,2,3,4,1,9,9,8,9,5};
    // int[] a = {10,9,2,5,3,7,101,18};
    int[] a = {0,1,0,3,2,3};
    int[] lis = new int[a.length];

    int max = 1;

    Arrays.fill(lis,1);


    for(int i =a.length-1; i > -1; --i)
      for(int j =i+1; j < a.length; ++j){
        if(a[i] < a[j] && lis[i] < lis[j]+1){
          lis[i] = lis[j]+1;
          max = Math.max(max, lis[i]);
        }
      }

    System.out.println("MaxIncreasing Sub : "+max);
    System.out.println("Lis : "+Arrays.toString(lis));


  }
}
