import java.util.Arrays;

class MaximumSumSubArray{



// Wrong implementation check MaximumSumSubMatrix for correct implementation
	public static void main(String z[]){

		System.out.println("Maximum sum subarray");

		int[][] matrix = {
			{1,2,-1,-4,-20},
			{-8,-3,4,2,1},
			{3,8,10,1,3},
			{-4,-1,1,7,-6}
		};

		// Gives 20 where as the correct anwer is 20
		// int[][] matrix = { 
		// 	{ 0, -2, -7, 0 },
	 //        { 9, 2, -6, 2 },
	 //        { -4, 1, -4, 1 },
	 //        { -1, 8, 0, -2 } 
  //       };

		System.out.println("Initial");
		printMatrix(matrix);


		int[][] m = new int[matrix.length][matrix.length];

		int i =0, j =0;

		for( i = 0; i < matrix.length; ++i){
			for( j =0 ;j < matrix.length; ++j){

				if(j == 0)
					m[j][i] = matrix[j][i];

				else
					m[j][i] = matrix[j][i] + m[j-1][i];

			}
		}

		System.out.println("Matrix 2");
		printMatrix(m);

		int maxSoFar = 0, min, subMatrix;

		for(i=0; i <matrix.length; ++i){

			for(j =0; j <matrix.length; ++j){
				min = 0;
				subMatrix =0;
				for(int k =0; k <matrix.length; ++k){

					if(i == 0){
						subMatrix = m[j][k];
					}else{
						subMatrix += m[j][k] - m[i-1][k];
					}

					if(subMatrix < min)
						min = subMatrix;
					if((subMatrix - min)> maxSoFar)
						maxSoFar = subMatrix-min;
				}
			}
		}

		System.out.println("Restul : "+maxSoFar);
	}

	static void printMatrix(int[][] m){

		System.out.println("\n<--start-->\n");
		for(int[] row : m){

			System.out.println(Arrays.toString(row));
		}
		System.out.println("\n<--end-->\n");
	}
}