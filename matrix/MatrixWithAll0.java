import java.util.Arrays;

class MatrixWithAll0{
	
	public static void main(String z[]){

		System.out.println("Matrix with all 0");

		int[][] matrix = {

			{1,1,1,1,1},
			{0,0,0,0,1},
			{0,0,0,1,0},
			{0,1,0,1,1},
			{1,1,1,1,1}
		};

		int[][] temp = new int[matrix.length][matrix[0].length];

		int i =0, j =0, rows = matrix.length, cols = matrix[0].length, val =1;
	
		for(i =0; i <rows; ++i){

			for(j =0; j <cols; ++j){

				if(i == 0 || j == 0){

					temp[i][j] = matrix[i][j] == 1 ? 0 : 1;
				}else{

						
					if(matrix[i-1][j] == 0 && matrix[i][j-1] == 0 && matrix[i-1][j-1] == 0 && matrix[i][j] == 0)
						temp[i][j] = temp[i-1][j-1] + 1;
					else if(matrix[i][j] == 0)
						temp[i][j] = 1;
					else
						temp[i][j] = 0;
				}
			}
		}

		for(int[] row : temp)
			System.out.println(Arrays.toString(row));
	}
}