
import java.util.Arrays;

class MaximumSumSubMatrix{


// Correct implementation
	public static void main(String z[]){


		int[][] matrix = { 
			{ 0, -2, -7, 0 },
            { 9, 2, -6, 2 },
            { -4, 1, -4, 1 },
            { -1, 8, 0, -2 } 
        };


		// int[][] matrix = {
		// 	{1,2,-1,-4,-20},
		// 	{-8,-3,4,2,1},
		// 	{3,8,10,1,3},
		// 	{-4,-1,1,7,-6}
		// };


        printMatrix("Initial Matrix",matrix);

        int rows = matrix.length;
        int cols = matrix[0].length;

        int i =0, j =0, k =0, l =0, m=0, n =0, g =0, ll =0; 

        for(i =0; i <rows; ++i){
        	for(j =0;  j <cols; ++j){
        		for(k = i; k <rows; ++k){
        			for(l = j; l <cols; ++l){

        				ll = 0;

        				for(m = i; m <= k; ++m)
        					for(n = j; n<= l; ++n)
        						ll += matrix[m][n];


        				g = g > ll ? g : ll;

        			}
        		}
        	}
        }

        System.out.println("Global max: "+g);

	}

	private static void printMatrix(String msg,int[][] m){

		System.out.println("\n<--- "+msg+" Start --->\n");

		for(int[] row : m){
        	System.out.println(Arrays.toString(row));
        }
        
        System.out.println("\n<--- "+msg+" End --->\n");
	}
}