import java.util.Arrays;

class ZeroMatrix{

	public static void main(String z[]){


		int[][] matrix = {
			{1,2,3,6,3},
			{0,3,1,4,0},
			{2,3,0,4,5},
			{1,2,9,4,2},
			{3,5,2,7,8}
		};

		boolean[] rows = new boolean[matrix.length];
		boolean[] cols = new boolean[matrix[0].length];

		int i =0, j =0;

		for(i =0; i <rows.length; ++i)
			for(j =0; j <cols.length; ++j)
				if(matrix[i][j] == 0){
					rows[i] = true;
					cols[j] = true;
				}


		for(int[] row : matrix)
			System.out.println(Arrays.toString(row));


		for(i =0; i <rows.length; ++i)
			for(j =0; j <cols.length; ++j)
				if(rows[i]){
					matrix[i][j] = 0;
				}

		for(i =0; i <cols.length; ++i)
			for(j =0; j <rows.length; ++j)
				if(cols[i]){
					matrix[j][i] = 0;
				}

		System.out.println();
		for(int[] row : matrix)
			System.out.println(Arrays.toString(row));

	}
}