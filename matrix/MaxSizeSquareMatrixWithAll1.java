import java.math.*;
import java.util.Arrays;

class MaxSizeSquareMatrixWithAll1{


	public static void main(String z[]){

		int[][] matrix = {
			{0,1,1,0,1},
			{1,1,0,1,0},
			{0,1,1,1,0},
			{1,1,1,1,0},
			{1,1,1,1,1},
			{0,0,0,0,0}
		};

		int[][] l = new int[matrix.length][matrix[0].length];


		for(int i =0; i <matrix.length; ++i){
			for(int j =0; j <matrix[0].length; ++j){

				if(i ==0 || j ==0)
					l[i][j] = matrix[i][j];

				else if(matrix[i][j] == 1){

					l[i][j] = Math.min(Math.min(l[i-1][j], l[i][j-1]),l[i-1][j-1])+1;
				}
			}
		}

		for(int[] elem : l){

			System.out.println(Arrays.toString(elem));
		}
	}
}