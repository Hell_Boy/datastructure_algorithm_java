
class BinaryTreeNode<T>{

	private T data;
	private BinaryTreeNode<T> left;
	private BinaryTreeNode<T> right;

	BinaryTreeNode(T data){

		this.data = data;
	}

	public T getData(){
		return this.data;
	}

	public void setData(T data){
		this.data = data;
	}

	public BinaryTreeNode<T> getLeft(){
		return this.left;
	}

	public void setLeft(BinaryTreeNode<T> left){
		this.left = left;
	}

	public BinaryTreeNode<T> getRight(){
		return this.right;
	}

	public void setRight(BinaryTreeNode<T> right){
		this.right = right;
	}

	
}