
class CreateBinaryTree{
	
	public static void main(String z[]){

		char[] inOrder = {'D','B','E','A','F','C'};
		char[] preOrder = {'A','B','D','E','C','F'};
		char[] postOrder = {'D', 'E', 'B', 'F', 'C' ,'A'};

		BinaryTreeNode<Character> tree = new CreateBinaryTree().buildBinaryTree(preOrder,inOrder);

		System.out.print("Inorder traversal : ");
		new CreateBinaryTree().inOrderTraversal(tree);
		System.out.println();

		System.out.print("Preorder traversal : ");
		new CreateBinaryTree().preOrderTraversal(tree);
		System.out.println();

		System.out.print("Postorder traversal : ");
		new CreateBinaryTree().postOrderTraversal(tree);
		System.out.println();
	}

	private void inOrderTraversal(BinaryTreeNode<Character> head){
		if(head == null)
			return;

		inOrderTraversal(head.getLeft());
		System.out.print(head.getData()+" ");
		inOrderTraversal(head.getRight());
	}

	private void preOrderTraversal(BinaryTreeNode<Character> head){

		if(head == null)
			return;

		System.out.print(head.getData()+" ");
		preOrderTraversal(head.getLeft());
		preOrderTraversal(head.getRight());
	}

	private void postOrderTraversal(BinaryTreeNode<Character> head){

		if(head == null)
			return;

		postOrderTraversal(head.getLeft());
		postOrderTraversal(head.getRight());
		System.out.print(head.getData()+" ");
	}


	private BinaryTreeNode<Character> buildBinaryTree(char[] preOrder, char[] inOrder){

		if(preOrder.length == 0 || preOrder.length != inOrder.length)
			return null;

		return buildPostOrder(preOrder, 0, preOrder.length-1, inOrder, 0, inOrder.length-1);
		// return build(preOrder, 0, preOrder.length-1, inOrder, 0, inOrder.length-1);
	}

	private BinaryTreeNode<Character> buildPostOrder(char[] postOrder, int postStart, int postEnd, char[] inOrder, int inStart, int inEnd){

		if(postStart > postEnd || inStart > inEnd)
			return null;

		char data = postOrder[postStart];

		BinaryTreeNode<Character> curr = new BinaryTreeNode<Character>(data);

		int offset = inStart;

		for(; offset<inEnd; ++offset){
			if(inOrder[offset] == data)
				break;
		}

		curr.setLeft(buildPostOrder(postOrder, postStart, postStart+offset-inStart-1, inOrder, inStart, offset-1));
		curr.setRight(buildPostOrder(postOrder, postStart+offset-inStart, postEnd-1, inOrder, offset+1, inEnd));
		return curr;
	}

	private BinaryTreeNode<Character> build(char[] preOrder, int preStart, int preEnd, char[] inOrder, int inStart, int inEnd){

		if(preStart > preEnd || inStart > inEnd)
			return null;

		char data = preOrder[preStart];

		BinaryTreeNode<Character> curr = new BinaryTreeNode<Character>(data);

		int offset = inStart;

		for(;offset < inEnd; ++offset){
			if(inOrder[offset] == data)
				break;
		}

		curr.setLeft(build(preOrder, preStart+1, preStart+offset-inStart, inOrder, inStart, offset-1));
		curr.setRight(build(preOrder, preStart+offset-inStart+1, preEnd, inOrder, offset+1, inEnd));

		return curr;

	}
}