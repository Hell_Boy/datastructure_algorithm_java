
import java.util.List;
import java.util.ArrayList;
import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;
import java.math.*;

class BinaryTreeMain{
	
	public static void main(String z[]){

		// Tree : https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/

		BinaryTreeNode<Integer> head = new BinaryTreeNode<Integer>(1);
		
		BinaryTreeNode<Integer> l = new BinaryTreeNode<Integer>(2);
		BinaryTreeNode<Integer> r = new BinaryTreeNode<Integer>(3);
		
		head.setLeft(l);
		head.setRight(r);

		BinaryTreeNode<Integer> ll = new BinaryTreeNode<Integer>(4);
		BinaryTreeNode<Integer> lr = new BinaryTreeNode<Integer>(5);
		
		l.setLeft(ll);
		l.setRight(lr);

		System.out.println("Binary Tree main");

		System.out.println("\n\nInOrder recursive traversal LDR\n");
		new BinaryTreeMain().inOrderTraversalRecur(head);

		System.out.println("\n\nPreOrder recursive traversal DLR\n");
		new BinaryTreeMain().preOrderTraversalRecur(head);

		System.out.println("\n\nPostOrder recursive traversal LRD\n");
		new BinaryTreeMain().postOrderTraversalRecur(head);

		System.out.println("\n\nBFS\n");
		new BinaryTreeMain().BFS(head);

		System.out.println("\n\nLevel order traversal\n");
		new BinaryTreeMain().levelOrderTraversal(head);

		System.out.println("\n\nMaxVal : "+new BinaryTreeMain().maxValInBinaryTree(head));

		System.out.println("\n\nElem in BT : "+new BinaryTreeMain().findInBinaryTree(head, 5));

		System.out.println("\n\nInsert level order BT : "+new BinaryTreeMain().insertInBTLevelOrder(head, 6));

		System.out.println("\n\nSize recur of binary tree : "+new BinaryTreeMain().sizeRecur(head));

		System.out.println("\n\nSize itr of binary tree : "+new BinaryTreeMain().sizeItr(head));

		System.out.println("\n\nReverse level order binary tree : "+new BinaryTreeMain().levelOrderReverse(head));

		System.out.println("\n\nMax depth : "+new BinaryTreeMain().maxDepth(head));

		System.out.println("\n\nDiameter : "+new BinaryTreeMain().diameter(head));

		System.out.println("\n\nMax Sum level order : "+new BinaryTreeMain().maxSumLevelOrder(head));	

		System.out.println("\n\nRoot to leaf : \n");
		new BinaryTreeMain().rootToLeaf(head);

		System.out.println("\n\nHas pathSum : "+new BinaryTreeMain().hasPathSum(head, 7));

		System.out.println("\n\nAddBT : "+new BinaryTreeMain().addBT(head));

		System.out.println("\n\nBefore mirror binary tree");
		new BinaryTreeMain().preOrderTraversalRecur(head);

		System.out.println("\n\nAfter mirror binary tree");
		BinaryTreeNode<Integer> mirrorTree = new BinaryTreeMain().mirror(head);
		new BinaryTreeMain().preOrderTraversalRecur(mirrorTree);

		System.out.println("\n\nIs mirror tree : "+new BinaryTreeMain().isMirrorTree(head, mirrorTree));

		System.out.println();	
	}

	private boolean isMirrorTree(BinaryTreeNode<Integer> root1, BinaryTreeNode<Integer> root2){

		if(root1 == null && root2 == null)
			return true;

		if(root1 == null || root2 == null)
			return false;

		if(root1.getData() != root2.getData())
			return false;

		return isMirrorTree(root1.getLeft(), root2.getRight()) && isMirrorTree(root1.getRight(), root2.getLeft());
	}

	private BinaryTreeNode<Integer> mirror(BinaryTreeNode<Integer> head){

		if(head != null){

			mirror(head.getLeft());
			mirror(head.getRight());

			BinaryTreeNode<Integer> temp = head.getLeft();
			head.setLeft(head.getRight());
			head.setRight(temp);
		}

		return head;
	}

	private int addBT(BinaryTreeNode<Integer> head){

		if(head == null)
			return 0;

		return (head.getData() + addBT(head.getLeft()) + addBT(head.getRight()));
	}

	private boolean hasPathSum(BinaryTreeNode<Integer> head, int data){

		if(head == null)
			return false;

		if(head.getLeft() == null && head.getRight() == null && data == head.getData())
			return true;

		data -= head.getData();

		return hasPathSum(head.getLeft(), data) || hasPathSum(head.getRight(), data);
	}

	Stack<Integer> stack = new Stack<Integer>();
	private void rootToLeaf(BinaryTreeNode<Integer> head){

		if(head == null)
			return;

		stack.push(head.getData());

		if(head.getLeft() == null && head.getRight() == null)
			System.out.println(stack);

		rootToLeaf(head.getLeft());
		rootToLeaf(head.getRight());

		stack.pop();
	}

	private int maxSumLevelOrder(BinaryTreeNode<Integer> head){

		int localSum = 0, maxSum = 0;

		if(head == null)
			return maxSum;

		Queue<BinaryTreeNode<Integer>> q = new LinkedList<BinaryTreeNode<Integer>>();
		q.offer(head);
		q.offer(null);

		while(!q.isEmpty()){


			BinaryTreeNode<Integer> top = q.poll();

			if(top != null){

				localSum += top.getData();
				if(top.getLeft() != null)
					q.offer(top.getLeft());

				if(top.getRight() != null)
					q.offer(top.getRight());
			}else{

				maxSum = maxSum < localSum ? localSum : maxSum;
				localSum = 0;
				if(!q.isEmpty())
					q.offer(null);
			}
		}

		return maxSum;

	}

	private int diameter(BinaryTreeNode<Integer> head){

		if(head == null)
			return 0;

		int l1 = maxDepth(head.getLeft()) + maxDepth(head.getRight())+1;
		int l2 = Math.max(diameter(head.getLeft()), diameter(head.getRight()));

		return Math.max(l1,l2);

	}

	private int maxDepth(BinaryTreeNode<Integer> head){

		if(head == null)
			return 0;

		int left = maxDepth(head.getLeft());
		int right = maxDepth(head.getRight());

		return left >right ? 1+left : 1+right;
	}

	private ArrayList<Integer> levelOrderReverse(BinaryTreeNode<Integer> head){

		ArrayList<Integer> ll = new ArrayList<Integer>();


		if(head == null)
			return ll;

		Stack<Integer> stack = new Stack<Integer>();
		Queue<BinaryTreeNode<Integer>> q = new LinkedList<BinaryTreeNode<Integer>>();
		q.offer(head);

		while(!q.isEmpty()){

			BinaryTreeNode<Integer> top = q.poll();
			stack.push(top.getData());

			if(top.getLeft() != null)
				q.offer(top.getLeft());

			if(top.getRight() != null)
				q.offer(top.getRight());
		}

		while(!stack.isEmpty())
			ll.add(stack.pop());

		return ll;
	}

	private int sizeItr(BinaryTreeNode<Integer> head){

		int count = 0;

		if(head == null)
			return count;

		Queue<BinaryTreeNode<Integer>> q = new LinkedList<BinaryTreeNode<Integer>>();
		q.offer(head);

		while(!q.isEmpty()){
			++count;

			BinaryTreeNode<Integer> top = q.poll();

			if(top.getLeft() != null)
				q.offer(top.getLeft());

			if(top.getRight() != null)
				q.offer(top.getRight());
		}



		return count;
	}


	private int sizeRecur(BinaryTreeNode<Integer> head){

		int left = head.getLeft() == null ? 0 : sizeRecur(head.getLeft());
		int right = head.getRight() == null ? 0 : sizeRecur(head.getRight());

		return 1 + left + right;
	}

	private boolean insertInBTLevelOrder(BinaryTreeNode<Integer> head, int data){

		if(head == null)
			return false;

		Queue<BinaryTreeNode<Integer>> q = new LinkedList<BinaryTreeNode<Integer>>();
		q.offer(head);

		while(!q.isEmpty()){

			BinaryTreeNode<Integer> top = q.poll();

			if(top.getLeft() == null){
				top.setLeft(new BinaryTreeNode<Integer>(data));
				return true;
			}
			else if(top.getRight() == null){
				top.setRight(new BinaryTreeNode<Integer>(data));
				return true;
			}
			else{
				q.offer(top.getRight());
				q.offer(top.getLeft());
			}
		}

		return false;
	}

	private boolean findInBinaryTree(BinaryTreeNode<Integer> head, int data){

		if(head == null)
			return false;

		if(head.getData() == data)
			return true;

		return findInBinaryTree(head.getLeft(), data) || findInBinaryTree(head.getRight(), data);
	}

	private int maxValInBinaryTree(BinaryTreeNode<Integer> head){

		int maxVal = Integer.MIN_VALUE;

		if(head == null)
			return maxVal;

		Queue<BinaryTreeNode<Integer>> q = new LinkedList<BinaryTreeNode<Integer>>();
		q.offer(head);

		while(!q.isEmpty()){
			BinaryTreeNode<Integer> top = q.poll();

			maxVal = top.getData() > maxVal ? top.getData() : maxVal;

			if(top.getLeft() != null)
				q.offer(top.getLeft());

			if(top.getRight() != null)
				q.offer(top.getRight());
		}

		return maxVal;
	}

	private void levelOrderTraversal(BinaryTreeNode<Integer> head){

		if(head == null)
			return;

		Queue<BinaryTreeNode<Integer>> q = new LinkedList<BinaryTreeNode<Integer>>();
		q.offer(head);

		while(!q.isEmpty()){

			BinaryTreeNode<Integer> top = q.poll();
			System.out.print(top.getData()+" ");

			if(top.getLeft() != null)
				q.offer(top.getLeft());

			if(top.getRight() != null)
				q.offer(top.getRight());
		}
	}

	private void BFS(BinaryTreeNode<Integer> head){

		if(head ==  null)
			return;

		int level = 0;

		Queue<BinaryTreeNode<Integer>> queue = new LinkedList<BinaryTreeNode<Integer>>();
		queue.offer(head);

		while(!queue.isEmpty()){

			int size = queue.size();
			++level;

			while(size-- > 0){
				BinaryTreeNode<Integer> top = queue.poll();

				System.out.println("level "+level+" : "+top.getData());

				if(top.getLeft() != null)
					queue.offer(top.getLeft());

				if(top.getRight() != null)
					queue.offer(top.getRight());
				
			}
		}
	}


	private void inOrderTraversalRecur(BinaryTreeNode<Integer> head){
		// LDR

		if(head == null)
			return;

		inOrderTraversalRecur(head.getLeft());
		System.out.print(head.getData()+" ");
		inOrderTraversalRecur(head.getRight());
	}

	private void preOrderTraversalRecur(BinaryTreeNode<Integer> head){

		// DLR

		if(head == null)
			return;

		System.out.print(head.getData()+" ");
		preOrderTraversalRecur(head.getLeft());
		preOrderTraversalRecur(head.getRight());
	}

	private void postOrderTraversalRecur(BinaryTreeNode<Integer> head){

		// LRD
		if(head == null)
			return;

		postOrderTraversalRecur(head.getLeft());
		postOrderTraversalRecur(head.getRight());
		System.out.print(head.getData()+" ");
	}
}