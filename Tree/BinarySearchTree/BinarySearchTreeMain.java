import java.util.Arrays;
import java.util.LinkedList;

class BinarySearchTreeMain{

	public static void main(String z[]){

		System.out.println("Binary search tree");

		int[] a = {1,2,3,2,4,5,6};

		Arrays.sort(a);

		System.out.println(Arrays.toString(a));

		TreeNode head = new BinarySearchTreeMain().build(a, 0, a.length-1);
		new BinarySearchTreeMain().inOrderTraversal(head);	
		System.out.println();

		boolean exists = new BinarySearchTreeMain().exist(head,1);
		System.out.println("Element found : "+exists);


		int minVal = new BinarySearchTreeMain().findMin(head);
		System.out.println("Minimum element in BST : "+minVal);

		int maxVal = new BinarySearchTreeMain().findMax(head);
		System.out.println("Max element in BST : "+maxVal);

		TreeNode newTree = new BinarySearchTreeMain().insert(head, 18);
		new BinarySearchTreeMain().inOrderTraversal(newTree);	
		System.out.println();

		// TreeNode deletedNewTree = new BinarySearchTreeMain().delete(head, 6);
		// new BinarySearchTreeMain().inOrderTraversal(deletedNewTree);
		// System.out.println();


		TreeNode lca = new BinarySearchTreeMain().leastCommonAncestor(head, 1,4);
		System.out.println("lca : "+lca.data);

		boolean isValidBST = new BinarySearchTreeMain().isBST(head);
		System.out.println("IsBST : "+isValidBST);


		int nthSmallestElem = new BinarySearchTreeMain().nthSmallest(head, 5);
		System.out.println("Nth smallest : "+nthSmallestElem);

	}


	private int nthSmallest(TreeNode head, int k){

		if(head == null)
			return k;

		k = nthSmallest(head.left, k);

		// if(left != null)
		// 	return left;

		if(--k == 0)
			return head.data;
		k= nthSmallest(head.right, k);
		return k;
	}


	private boolean isBST(TreeNode head){

		if(head == null)
			return true;

		if(head.left != null && findMax(head.left) > head.data)
			return false;

		if(head.right != null && findMin(head.right) < head.data)
			return false;

		if(!isBST(head.left) || !isBST(head.right))
			return false;
		return true;
	}

	private TreeNode leastCommonAncestor(TreeNode head, int a, int b){

		if(head == null)
			return head;

		if(head.data == a || head.data == b)
			return head;

		if(Math.max(a, b) > head.data)
			return leastCommonAncestor(head.left, a, b);

		if(Math.min(a,b) < head.data)
			return leastCommonAncestor(head.right, a, b);

		return head;
	}

	private TreeNode delete(TreeNode head, int data){

		if(head == null)
			return null;

		else if(data > head.data)
			 head.right = delete(head.right, data);

		else if(data < head.data)
			head.left = delete(head.left, data);

		else{

			if(head.left != null && head.right != null){

				int max = findMax(head.left);
				head.data = max;
				head.left = delete(head.left, max);
			}

			if(head.left == null)
				head = head.right;

			if(head.right == null)
				head = head.left;
		}

		return head;
	}


	private TreeNode insert(TreeNode head, int data){

		if(head == null){
			head = new TreeNode();
			head.data = data;
		}else{

			if(data > head.data)
				head.right = insert(head.right, data);
			else
				head.left = insert(head.left, data);
		}

		return head;

	}

	private int findMax(TreeNode head){

		if(head == null)
			return Integer.MAX_VALUE;

		if(head.right == null)
			return head.data;

		return findMax(head.right);
	}

	private int findMin(TreeNode head){

		if(head == null)
			return Integer.MIN_VALUE;
		if(head.left == null)
			return head.data;

		return findMin(head.left);
	}

	private boolean exist(TreeNode head, int elem){

		if(head == null)
			return false;

		if(head.data == elem)
			return true;

		if(head.data > elem)
			return exist(head.left, elem);


		return exist(head.right, elem);

	}

	private TreeNode build(int[] a, int left, int right){

		TreeNode node;

		if(left > right)
			return null;

		node = new TreeNode();

		if(node == null){
			System.out.println("Memory Error");
			return null;
		}

		if(left == right)
			node.data = a[left];

		else{

			int mid = left+(right-left)/2;

			node.data = a[mid];
			node.left = build(a, left, mid-1);
			node.right = build(a,mid+1, right);
		}

		return node;
	}

	private void inOrderTraversal(TreeNode head){
		
		if(head == null)
			return;

		inOrderTraversal(head.left);
		System.out.print(head.data+" ");
		inOrderTraversal(head.right);
	}
}