import java.util.Arrays;


class PrefixFunction{

  public static void main(String z[]){

    System.out.println("Hello world");

    // String s = "ababaca";
    String s = "abcabcabc";

    int[] F = new int[s.length()];

    int j = 0;
    int i = 1;

    while(i < s.length()){

      if(s.charAt(i) == s.charAt(j)){

        ++j;
        F[i] = j;
        ++i;
      }else if(j > 0){
        --j;
      }else{
        F[i] = 0;
        ++i;
      }
    }

    System.out.println("Prefix function : "+Arrays.toString(F));
  }
}
