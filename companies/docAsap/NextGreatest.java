import java.util.Arrays;
import java.util.Stack;

class NextGreatest{

  public static void main(String z[]){

    System.out.println("Hello world");

    int[] arr = {18, 7, 6, 12, 10};

    int[] pos = new int[arr.length];

    Arrays.fill(pos, -1);


    Stack<Integer> st = new Stack<Integer>();

    st.push(0);

    for(int i = 1; i < arr.length; ++i){


      if(arr[st.peek()] >= arr[i])
      st.push(i);

      else{

        pos[st.pop()] = arr[i];
      }

    }

    System.out.println(Arrays.toString(pos));
  }
}
