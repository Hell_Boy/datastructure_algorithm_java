import java.util.Arrays;


class QuickSort{


  public static void main(String z[]){
    System.out.println("Hello world");

    int[] a = {50,25,92,16,76,30,43,54,19};

    new Solution(a).sort();
  }
}

class Solution{

  int a[];

  public Solution(int[] a){
    this.a = a;
  }

  public void sort(){

    System.out.println("Input arr : "+Arrays.toString(a));
    quickSort(0, a.length-1);

    System.out.println("Sorted arr : "+Arrays.toString(a));
  }

  private void quickSort(int low, int high){

    if(high > low){
      String log = "Left  : %d, Right : %d";
      System.out.println(String.format(log, low, high));

      int pivotVal = pivot(low, high);
      quickSort(low, pivotVal-1);
      quickSort(pivotVal+1, high);
    }
  }



  private int pivot(int left, int right){


    int low = left, high = right, pivot_item = a[low];

    while(low < high){

      while(a[low] <= pivot_item)
        ++low;

      while(a[high] > pivot_item)
        --high;

      if(low < high){
        // swap
        int temp = a[low];
        a[low] = a[high];
        a[high] = temp;
      }
    }

    a[left] = a[high];
    a[high] = pivot_item;
    return high;
  }
}
