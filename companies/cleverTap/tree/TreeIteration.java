
import utils.BinaryTreeNode;
import java.util.Arrays;
import java.lang.Math;

class TreeIteration {

  public static void main(String z[]) {

    System.out.println("Hello world");
    int[] a = { 1, 2, 3, 4, 5, 6, 7 };

    TreeIteration tree = new TreeIteration();


    // Binary Tree
    // BinaryTreeNode root = tree.createBinaryTree(a, 0, a.length-1);
    // System.out.println("Root : " + root.getData());
    // tree.inOrderTraversal(root);
    // System.out.println();

    // Binary Search Tree
    Arrays.sort(a);
    BinaryTreeNode root = tree.createBST(a, 0, a.length-1);
    tree.inOrderTraversal(root);
    System.out.println();

  }

  // Create Binary Search Tree
  private BinaryTreeNode createBST(int[] a, int left, int right){

    if(left > right)
    return null;

    BinaryTreeNode node;

    if(left == right)
      node = new BinaryTreeNode(a[left]);
    else{

      int mid = left + (right-left)/2;
      node = new BinaryTreeNode(a[mid]);
      node.setLeft(createBST(a, left, mid-1));
      node.setRight(createBST(a, mid+1, right));
    }

    return node;
  }

  // Create Binary Tree
  private BinaryTreeNode createBinaryTree(int[] a, int left, int right) {

    if(left > right)
    return null;

    BinaryTreeNode node = new BinaryTreeNode(a[left]);
    node.setLeft(createBinaryTree(a, 2*left+1, right));
    node.setRight(createBinaryTree(a, 2*left+2, right));

    return node;
  }

  // Inorder traversal

  private void inOrderTraversal(BinaryTreeNode root) {

    if (root != null) {
      inOrderTraversal(root.getLeft());
      System.out.print(root.getData()+" ");
      inOrderTraversal(root.getRight());
    }
  }

}
