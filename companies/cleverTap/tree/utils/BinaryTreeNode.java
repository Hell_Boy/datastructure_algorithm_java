package utils;

public class BinaryTreeNode{

  private int data;
  private BinaryTreeNode left;
  private BinaryTreeNode right;

  public BinaryTreeNode(int data){
    this.data = data;
  }

  public int getData(){
    return this.data;
  }

  public void setLeft(BinaryTreeNode node){
    this.left = node;
  }

  public void setRight(BinaryTreeNode node){
    this.right = node;
  }

  public BinaryTreeNode getLeft(){
    return this.left;
  }

  public BinaryTreeNode getRight(){
    return this.right;
  }
}
