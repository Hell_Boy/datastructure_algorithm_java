
import java.util.Arrays;

class MergeSort{

  public static void main(String z[]){

    System.out.println("hello wordld");

    int[] a = {4,5,6,1,7,3,4};

    new MergeSort().sort(a, 0, a.length-1);
    System.out.println(Arrays.toString(a));
  }

  private void merge(int[] a, int start, int mid, int end){

    int[] result = new int[a.length];
    int left = start, right = mid, k =0;

    while(left <= mid && right < end){

      if(a[left] < a[right]){

        result[k] = a[left];
        ++left;
      }else{

        result[k] = a[right];
        ++right;
      }

      ++k;
    }

    while(left <= mid){

      result[k] = a[left];
      ++left;
      ++k;

    }

    while(right < end){
      result[k] = a[right];
      ++right;
      ++k;
    }

    System.out.println(Arrays.toString(result));
    for(int i =start; i < result.length; ++i)
    a[i] = result[i];
  }

  private void sort(int[] a, int start, int end){

    if(start < end){

      int mid = start+(end-start)/2;
      sort(a, start, mid);
      sort(a, mid+1, end);
      merge(a, start, mid, end);
    }
  }
}
