

/**
Input: nums = [1,1,2]
Output:
[[1,1,2],
 [1,2,1],
 [2,1,1]]
**/

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

class Problem{

  public static void main(String z[]){

    System.out.println("Hello world");

    int[] a = {1,2,1,3};

    Set<Integer> s = new HashSet<Integer>();

    for(int elem : a){
      s.add(elem);
    }

    System.out.println(s);
  }
}
