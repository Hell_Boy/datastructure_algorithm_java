
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.ArrayDeque;
import java.lang.Math;

class MaxSlidingWindow{


  public static void main(String z[]){

    System.out.println("Hello world");

    int[] a = {1,3,-1,-3,5,3,6,7};
    int k = 3;

    int[] result = new MaxSlidingWindow().maxSliding(a, k);

    System.out.println(Arrays.toString(result));
  }

  private int[] maxSliding(int[] a, int k){

    if(k == 1)
    return a;

    int[] result = new int[a.length - k +1];
    ArrayDeque<Integer> aq = new ArrayDeque<Integer>();

    for(int i =0; i < a.length; ++i){

      if(i>=k && a[i-k] == aq.getFirst())
        aq.poll();

      while(!aq.isEmpty() && aq.getLast() < a[i])
        aq.pollLast();

      aq.offer(a[i]);

      if(i >=k-1)
      result[i-k+1] = aq.peek();
    }
    return result;
  }
}
