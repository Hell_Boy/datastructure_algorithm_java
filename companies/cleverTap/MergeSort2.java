import java.util.Arrays;

class MergeSort2{

  private void merge(int[] a, int start, int mid, int end){

    int[] helper = Arrays.copyOfRange(a, 0, a.length);

    int current = start, left = start, right = mid+1;

    while(left <= mid && right <= end){

      if(a[left] < a[right]){
        a[current] = a[left];
        ++left;
      }else{
        a[current] = a[right];
        ++right;
      }
      ++current;
    }

    int remaining = mid - left;

    for(int i =0; i <=remaining; ++i, ++current){
      a[current] = helper[left + i];
    }
  }

  private void sort(int[] a, int start, int end){

    if(start < end){

      int mid = start + (end-start)/2;
      sort(a, start, mid);
      sort(a, mid+1, end);
      merge(a, start, mid, end);
    }
  }

  public static void main(String z[]){

    System.out.println("Hello world");
    // int[] a = { 2, 5, 1, 3, 7 };
    int[] a = {5,2,3,1};

    new MergeSort2().sort(a, 0, a.length-1);

    System.out.println(Arrays.toString(a));
  }
}
