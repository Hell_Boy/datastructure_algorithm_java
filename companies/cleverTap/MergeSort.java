import java.util.Arrays;

class MergeSort {

  private void merge(int[] a, int start, int mid, int end) {

    int[] temp = new int[a.length];

    int left = start, right = mid + 1, k = 0;

    while (left <= mid && right <= end) {

      if (a[left] < a[right]) {
        temp[k] = a[left];
        ++left;
      } else {
        temp[k] = a[right];
        ++right;
      }

      ++k;
    }

    while (left <= mid) {
      temp[k] = a[left];
      ++left;
      ++k;
    }

    while (right <= end) {
      temp[k] = a[right];
      ++right;
      ++k;
    }

    for (int j = start; j <= end; ++j)
      a[j] = temp[j-start];
  }

  private void sort(int[] a, int start, int end) {

    if (start < end) {

      int mid = (start + (end - start) / 2);
      sort(a, start, mid);
      sort(a, mid + 1, end);
      merge(a, start, mid, end);
    }
  }

  public static void main(String z[]) {

    System.out.println("Hello world");

    // int[] a = { 2, 5, 1, 3, 7 };
    int[] a = {5,2,3,1};


    new MergeSort().sort(a, 0, a.length - 1);

    System.out.println(Arrays.toString(a));
  }
}
