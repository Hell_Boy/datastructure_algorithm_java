import java.util.Arrays;

class QuickSort{

  private int pivot(int[] a, int start, int end){

    int pivotItem = a[start];

    int left = start, right = end;

    while(left < right){

      while(left < a.length && a[left] <= pivotItem)
      ++left;

      while(right > -1 &&a[right] > pivotItem)
      --right;

      if(left < right){
        a[left] = a[left]+a[right];
        a[right] = a[left]-a[right];
        a[left] = a[left]-a[right];
      }
    }

    a[start] = a[right];
    a[right] = pivotItem;

    return right;
  }

  private void sort(int[] a, int start, int end){

    if(end > start){

      int pv = pivot(a, start, end);

      sort(a, start, pv-1);
      sort(a, pv+1, end);
    }
  }

  public static void main(String z[]){

    System.out.println("Hello world");

    // int[] a = {2,3,1,5,2,7,9,8};
    int[] a = {5,1,1,2,0,0};

    new QuickSort().sort(a, 0, a.length-1);

    System.out.println(Arrays.toString(a));
  }
}
