import java.util.Arrays;
import java.util.Queue;
import java.util.LinkedList;

class CreateDirectionMatrix{

	public static void main(String z[]){

		String s = "MMLM";

		int size = s.length();

		int[][] d = new int[size][size];

		// 1 = U
		// 2 = L
		// 3 = R
		// 4 = D

		int[] p = {size-1,size-1};
		int n = 1;
		d[size-1][size-1] = 1;

		for(int i =0; i <size; ++i){

			if(s.charAt(i) == 'M'){
				if(n == 1)
					p = new int[]{p[0]-1, p[1]};

				if(n == 2)
					p = new int[]{p[0], p[1]-1};

				if(n == 3)
					p = new int[]{p[0], p[1]+1};

				if(n == 4)
					p = new int[]{p[0]+1, p[1]};

				// if(p[0] >= 0 && p[0] < size && p[1] >=0 && p[1] < size)
				d[p[0]][p[1]] = 1;
			}

			if(s.charAt(i) == 'U')
				n = 1;

			if(s.charAt(i) == 'L')
				n = 2;
			
			if(s.charAt(i) == 'R')
				n = 3;
			
			if(s.charAt(i) == 'D')
				n = 4;
			
		}


		for(int[] e : d)
			System.out.println(Arrays.toString(e));

		System.out.println("Final cordinate : "+Arrays.toString(p));

		// Shortest path


		int[][] dir = {
			{-1,+1},
			{1,1},
			{1,-1},
			{-1,-1},
			{-1,0},
			{0,1},
			{1,0},
			{0,-1}
		};

		Queue<Cord> q = new LinkedList<Cord>();
		q.add(new Cord(size-1, size-1, ""));

		while(!q.isEmpty()){

			int ss = q.size();

			while(ss-- > 0){

				Cord c = q.poll();
				if(c.x == p[0] && c.y == p[1]){

					System.out.println(c.s);
					return;
				}

				for(int i =0; i < dir.length; i++){

					int[] dd = dir[i];
					int rr = c.x + dd[0];
					int cc = c.y + dd[1];

					if(rr >=0 && rr < size && cc >=0 && cc <size){
						q.add(new Cord(rr,cc,c.s+i));
					}
				}
			}
		}

	}
}

class Cord{

	int x;
	int y;
	String s ="";

	public Cord(int x, int y, String s){
		this.x = x;
		this.y = y;
		this.s = s;
	}


}