import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Arrays;

class SortingBasedOn2Params{


	public static void main(String z[]){


		int[][]m = {
			{1,2,1},
			{2,3,1},
			{4,1,2},
			{10,10,10},
			{15,13,10},
			{15,7,5}
		};


		List<Job> jobs = new ArrayList<Job>();

		int maxDeadline = Integer.MIN_VALUE;

		for(int[] e : m){

			jobs.add(new Job(e[0],e[1],e[2]));
			maxDeadline = maxDeadline < e[0] ? e[0] : maxDeadline;
		}


		int maxProfit = 0, i = 0;
		boolean[] slot = new boolean[maxDeadline+1];

		// System.out.println(Arrays.toString(slot));

		Collections.sort(jobs, (j1,j2)-> j2.p > j1.p && j2.t < j1.t ? 0 : -1);


		for(Job j : jobs){

			int t = j.t;
			System.out.println(j.d+" "+j.p+" "+j.t);

			int fSlot = 0;
			for(i =j.d; i > -1; --i){

				// System.out.println("Slot : "+slot[i]);
				if(!slot[i]){
					fSlot = i;
					break;
				}
			}

			// System.out.println("fSlot : "+fSlot);

			if(fSlot-t > -1){
				maxProfit += j.p;

				for(i = fSlot; i > fSlot-t; --i)
					slot[i] = true;
			}
		}

		System.out.println("Max deadline : "+maxDeadline);
		System.out.println("Max profit : "+maxProfit);

	}
}

class Job{

	int d;
	int p;
	int t;

	public Job(int d, int p, int t){
		this.d = d;
		this.p = p;
		this.t = t;
	}
}