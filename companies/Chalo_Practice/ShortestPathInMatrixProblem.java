// Shortest path in matrix BFS

import java.util.Queue;
import java.util.LinkedList;

class ShortestPathInMatrixProblem{

	public static void main(String z[]){

		System.out.println("Shortest path in matrix problem");

		int[][] m = {
			{0,0,0,0},
			{1,1,0,0},
			{1,1,1,0},
			{1,1,1,0},
		};

		if(m[0][0] == 1){
			System.out.println("No init point present");
			return;
		}

		int r = m.length, c = m[0].length, rr, cc;
		int[][] dir = {
			{0,1},
			{0,-1},
			{1,1},
			{1,0},
			{1,-1},
			{-1,-1},
			{-1,0},
			{-1,1}
		};

		int[] p;

		Queue<int[]> q = new LinkedList<int[]>();
		q.add(new int[]{0,0,1});
		m[0][0] = 1;

		while(!q.isEmpty()){

			int size = q.size();

			while(size-- > 0){

				p = q.poll();
				if(p[0] == r-1  && p[1] == c-1){

					System.out.println("Steps : "+p[2]);
					return;
				}

				for(int[] d : dir){

					rr = p[0]+d[0];
					cc = p[1]+d[1];

					if(rr >=0 && rr < r && cc >=0 && cc < c && m[rr][cc] == 0){

						q.add(new int[]{rr,cc,p[2]+1});
						m[rr][cc] = 1;
					}
				}
			}
		}

		System.out.println("No path found");
		return;
	}
}