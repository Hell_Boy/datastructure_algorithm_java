// All possible path
import java.util.List;
import java.util.LinkedList;

class AllPossiblePath{

	public static void main(String z[]){

		System.out.println("All possible path");

		int m[][] = { 
				  { 1, 0, 0, 0, 0 },
                  { 1, 1, 1, 1, 1 },
                  { 1, 1, 1, 0, 1 },
                  { 0, 0, 0, 0, 1 },
                  { 0, 0, 0, 0, 1 } };

        List<String> paths = new PossiblePaths(m).allPaths();
        System.out.println(paths);
	}
	
}

class PossiblePaths{

	List<String> paths;
	String path = "";
	int rows, cols;
	int[][] matrix;
	

	public PossiblePaths(int[][] matrix){
		this.matrix = matrix;
		this.rows = matrix.length;
		this.cols = matrix[0].length;
		paths = new LinkedList<String>();
	}

	private boolean isSafe(int r, int c, boolean[][] visited){

		// System.out.println("\n");
		// System.out.println("r : "+r+" c : "+c);
		
		boolean result = (r >=0 && r < rows && c >=0 && c <cols && matrix[r][c] == 1 && !visited[r][c]);

		return result;
	}


	private void explorePaths(int r, int c, int matrix[][], boolean visited[][]){

		// System.out.println("r : "+r+" c : "+c);
		
		// System.out.println("Safe : "+isSafe(r,c,visited));
		// System.out.println("Rows : "+rows+" Cols : "+cols);

		if(r == rows-1 && c == cols-1){

			System.out.println(path);
			paths.add(path);
			return;
		}

		// if(!isSafe(r,c))
		// 	return;

		visited[r][c] = true;

		// D
		if(isSafe(r+1,c,visited)){

			path+="D";
			explorePaths(r+1,c,matrix, visited);
			path = path.substring(0, path.length()-1);	
		}

		// L
		if(isSafe(r,c-1,visited)){

			path+="L";
			explorePaths(r,c-1,matrix, visited);
			path = path.substring(0, path.length()-1);	
		}

		// R
		if(isSafe(r,c+1,visited)){

			path+="R";
			explorePaths(r,c+1,matrix, visited);
			path = path.substring(0, path.length()-1);	
		}

		// U
		if(isSafe(r-1,c,visited)){

			path+="U";
			explorePaths(r-1,c,matrix, visited);
			path = path.substring(0, path.length()-1);	
		}

		visited[r][c] = false;	
	};

	public List<String> allPaths(){
		boolean[][] visited = new boolean[rows][cols];
		explorePaths(0, 0, matrix, visited);
		return paths;
	}
}