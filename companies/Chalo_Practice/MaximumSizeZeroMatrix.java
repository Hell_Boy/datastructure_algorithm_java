// Maximum Size matrix with all zeros

import java.util.Arrays;
import java.math.*;

class MaximumSizeZeroMatrix{

	public static void main(String z[]){

		System.out.println("Maximum size zero matrix");

		int[][] m = {
			{1,0,0,1,0,0},
			{1,0,0,1,0,0},
			{1,0,0,0,1,1},
			{1,0,0,0,1,1},
			{1,0,0,0,1,1},
			{1,0,0,1,1,0},

		};

		int r = m.length, c = m[0].length, i = 0, j = 0;

		int[][] t = new int[r][c];

		for(i =0; i < r; ++i)
			for(j =0; j <c; ++j){

				if(i ==0 || j ==0)
					t[i][j] = m[i][j] == 0 ? 1 : 0;

				else if (m[i][j] == 1)
					t[i][j] = 0;
				else{
					if(m[i-1][j] == 0 && m[i][j-1] ==0 && m[i-1][j-1] == 0)
						t[i][j] = Math.min(Math.min(t[i-1][j],t[i][j-1]),t[i-1][j-1])+1;
					else 
						t[i][j] = 1;
					
				}
			}

		for(int[] e : t)
			System.out.println(Arrays.toString(e));

	}
}