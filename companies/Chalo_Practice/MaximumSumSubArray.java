// Maximum Sum Subarray

import java.util.Arrays;

class MaximumSumSubArray{
	
	public static void main(String z[]){

		System.out.println("Maximum Sum in sub array");

		int[][] m ={
			{1,3,2,-4,5},
			{2,-3,5,4,5},
			{1,3,2,4,5},
			{3,9,2,4,5},
			{1,3,-6,-4,5}
		};

		int r = m.length, c = m[0].length, gMax = 0, lMax = 0;

		for(int i =0; i <r; ++i)
			for(int j =0; j <c; ++j)
				for(int k =i; k <r; ++k)
					for(int l =j; l <c; ++l){
						lMax = 0;

						for(int o = i; o <=k; ++o)
							for(int n =j; n <=l; ++n){

								lMax+=m[o][n];
							}

						lMax = lMax < 0 ? 0 : lMax;
						gMax = gMax < lMax ? lMax : gMax;
					}

		System.out.println("Max sum : "+gMax);

	}
}