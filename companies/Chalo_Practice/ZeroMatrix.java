// Convert rows and columns to zero with single 0 in row and column

import java.util.Arrays;

class ZeroMatrix{

	public static void main(String z[]){

		System.out.println("Zero matrix");

		int[][] m = {
			{1,2,3,4,0},
			{1,2,3,4,5},
			{1,0,3,4,5},
			{1,2,3,4,5},
			{1,2,3,0,5},
		};

		int r = m.length, c = m[0].length;

		boolean[] row = new boolean[r];
		boolean[] col = new boolean[c];

		int i =0, j = 0;

		for(i=0; i<r; ++i){
			for(j=0; j<c; ++j){

				if(m[i][j] == 0){
					row[i] = true;
					col[j] = true;
				}
			}
		}

		System.out.println("Row : "+Arrays.toString(row));
		System.out.println("Col : "+Arrays.toString(col));


		// Marking zero row
		for(i =0; i <r; ++i){

			if(row[i]){

				for(j =0; j <c; ++j)
					m[i][j] = 0;

			}
		}

		// // Marking zero col
		for(j = 0; j < c; ++j){

			if(col[j]){

				for(i =0; i < r; ++i)
					m[i][j] = 0;
			}
		}

		for(int[] re : m)
			System.out.println(Arrays.toString(re));

	}
}