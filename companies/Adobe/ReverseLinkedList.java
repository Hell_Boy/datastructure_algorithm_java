import java.util.Stack;

class ReverseLinkedList{
	
	public static void main(String z[]){

		int[] a = {1,2};

		ListNode head = new ListNode(a[0]);

		for(int i =1; i< a.length; ++i)
			head.next = new ListNode(a[1]);

		Stack<Integer> stack = new Stack<Integer>();

		while(head != null){
			stack.push(head.val);
			head = head.next;
		}

		ListNode temp = null;

		while(!stack.isEmpty()){

			if(temp == null){
				temp = new ListNode(stack.pop());
				head = temp;
			}
			else{
				temp.next = new ListNode(stack.pop());
				temp = temp.next;
			}
		}

		while(head != null){
			System.out.println(head.val);
			head = head.next;
		}
	}	
}



class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 }