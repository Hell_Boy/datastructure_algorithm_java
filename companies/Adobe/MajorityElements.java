
import java.util.Arrays;

class MajorityElements{
	
	public static void main(String z[]){

		int[] a = {3,2,3};
		// int[] a = {2,2,1,1,1,2,2};

		Arrays.sort(a);

		double lim = a.length/2;

		int lMax = 0, gMax = 1, maxVal = a[0], current = a[0];
		
		for(int i =0; i < a.length; ++i ){

			maxVal = lMax >= gMax ? current : maxVal;

			gMax = lMax > gMax ? lMax : gMax;

			if(a[i] == current)
				++lMax;

			else{				
				current = a[i];
				lMax = 1;
			}
		}


		if(gMax >= lim){

			System.out.println(maxVal);
			return;
		}

		System.out.println("Not found");
	}
}