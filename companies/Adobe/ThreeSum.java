
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.ArrayList;

class ThreeSum{

	public static void main(String z[]){

		System.out.println("3 sum problem");

		int[] a = {-1,0,1,2,-1,-4};
		// int[] a = {0,0,0,0};

		Map<List<Integer>,Boolean> map = new HashMap<List<Integer>, Boolean>();
		List<List<Integer>> result = new LinkedList<>();
		

		Arrays.sort(a);

		int i, l, r =0;		

		for(i =0; i < a.length-2; ++i){

			l = i+1;
			r = a.length-1;

			while(l<r){

				List<Integer> triplet = new ArrayList<Integer>(Arrays.asList(a[i], a[l], a[r]));
				int sum = a[i]+a[l]+a[r];
				if( sum == 0){

					if(!map.containsKey(triplet)){
						map.put(triplet, true);
						result.add(triplet);
					}

					++l;
					--r;
				}

				if(sum > 0)
					--r;
				else
					++l;
			}
		}
		
		System.out.println(map);

		for(List<Integer> f : result)
		System.out.println(f);
	}
}