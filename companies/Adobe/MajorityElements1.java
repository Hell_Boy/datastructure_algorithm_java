import java.util.Map;
import java.util.HashMap;

class MajorityElements1{

	public static void main(String z[]){

		int[] a = {3,2,3};

		Map<Integer,Integer> map = new HashMap<Integer,Integer>();
		int count, max = Integer.MIN_VALUE;

		for(int e : a){


			if(map.containsKey(e)){
				count = map.get(e);
				++count;
				
			}else{

				count = 1;
			}

			map.put(e, count);
		}

		count = 0;

		for(Map.Entry<Integer, Integer> entry : map.entrySet()){
			
			if(count< entry.getValue()){
				count = entry.getValue();
				max = entry.getKey();
			}
		}

		
		if( count >= (a.length/2)){
			System.out.println(max);
			return;
		}

		System.out.println(-1);

	}
}