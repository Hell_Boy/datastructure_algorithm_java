import java.util.Stack;
import java.util.HashMap;
import java.util.Map;
import java.lang.Character;

class ValidParanthesis{
	
	public static void main(String z[]){

		String s = "()}";


        if(s.length() < 1){

        	System.out.println("false");
        	return;
        }

        char[] c = s.toCharArray();
        
        Stack<Character> stack = new Stack<Character>();
        
        for(int i=0; i < c.length; ++i){

        	switch(c[i]){

        		case '}':
        			if( stack.isEmpty() || stack.pop() != '{')
        				System.out.println("false");
        			break;

        		case ']':
        			if( stack.isEmpty() || stack.pop() != '[')
        				System.out.println("false");
        			break;
        		
        		case ')':
        			if( stack.isEmpty() || stack.pop() != '(')
        				System.out.println("false");
        			break;

        		default:
        			stack.push(c[i]);
        	}
        }

        System.out.println(stack.isEmpty());
	}
}