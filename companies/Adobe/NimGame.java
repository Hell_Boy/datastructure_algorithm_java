
import java.util.Stack;

class NimGame{
	

	public static void main(String z[]){

		int n = 149, top = 0, size;

		if(n < 4)
			System.out.println(true);

		Stack<Integer> stack = new Stack<Integer>();
		stack.push(n);

		boolean won = false;

		while(!stack.isEmpty()){

			size = stack.size();

			while(size-- > 0){

			top = stack.pop();

			if(top > 0 && top < 4){
				won = true;
				break;
			}

			else{

				--top;
				if(top < 4){
					won = false;
					break;
				}


				
				stack.push(top-3);
				stack.push(top-2);
				stack.push(top-1);
			}
		}
		}

		System.out.println(won);
	}
}