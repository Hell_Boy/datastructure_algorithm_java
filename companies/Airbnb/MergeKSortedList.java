

class ListNode {
  int val;
  ListNode next;
  ListNode() {}
  ListNode(int val) { this.val = val; }
  ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}

public class MergeKSortedList{

	public static void main(String z[]){

		ListNode[] list = new ListNode[3];

		// ListNode l1 = new ListNode(1);
		// l1.next = new ListNode(4, new ListNode(5));

		


		// ListNode l2 = new ListNode(1);
		// l2.next = new ListNode(3, new ListNode(4));
		

		// ListNode l3 = new ListNode(2, new ListNode(6));

		// list[0] = l1;
		// list[1] = l2;
		// list[2] = l3;

		// int listName = 0;

		// for(ListNode ll : list){

		// 	++listName;
		// 	System.out.println("list name : "+listName);

		// 	new MergeKSortedList().printList(ll);
		// }

		ListNode sortedList = new MergeKSortedList().merge(list, 0, list.length-1);


		new MergeKSortedList().printList(sortedList);
	}

	private ListNode merge(ListNode[] ln, int start, int end){

		if(start> end-1)
			return ln[start];

		int mid = start+(end-start)/2;

		ListNode l = merge(ln, start, mid);
		ListNode r = merge(ln, mid+1, end);

		ListNode result = new ListNode();
		ListNode resultHead = new ListNode(); 
		boolean resultHeadMapped = false;

		int val = 0;

		while(l != null && r != null){
			
			if(l.val < r.val){
				val = l.val;
				l = l.next;
			}
			else{
				val = r.val;
				r = r.next;
			}
			
			if(!resultHeadMapped){
				result = new ListNode(val);
				resultHead = result;
				resultHeadMapped = true;
			}else{

				result.next = new ListNode(val);
				result = result.next;
			}


		}

		while(l != null){
			val = l.val;
			l = l.next;


			if(!resultHeadMapped){
				result = new ListNode(val);
				resultHead = result;
				resultHeadMapped = true;
			}else{

				result.next = new ListNode(val);
				result = result.next;
			}
		}


		while(r != null){
			val = r.val;
			r = r.next;

			
			if(!resultHeadMapped){
				result = new ListNode(val);
				resultHead = result;
				resultHeadMapped = true;
			}else{

				result.next = new ListNode(val);
				result = result.next;
			}
		}

		
		return resultHead;

	}


	private void printList(ListNode list){

		ListNode temp = list;

		while(temp != null){

				System.out.print(temp.val+(temp.next != null ? "->" : ""));
				temp = temp.next;
			}

			System.out.println();
	}
}

