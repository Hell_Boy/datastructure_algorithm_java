import java.lang.StringBuilder;

class AddString{

	public static void main(String z[]){

		System.out.println("Add String");

		// String num1 = "11";
		// String num2 = "123";

		String num1 = "1";
		String num2 = "9";


		char[] num1Chars = num1.toCharArray();
		char[] num2Chars = num2.toCharArray();

		StringBuilder sb = new StringBuilder();

		int carry = 0, l1 = num1Chars.length-1, l2 = num2Chars.length-1, sum = 0;

		while(l1 >-1 && l2 >-1){


			sum = (num1Chars[l1]-48)+(num2Chars[l2]-48)+carry;

			sb.append(sum%10);

			carry = sum/10;

			--l1;
			--l2;
		}



		while(l1 >-1){

			sum = (num1Chars[l1]-48)+carry;

			sb.append(sum%10);

			carry = sum/10;

			--l1;
		}

		while(l2 >-1){

			sum = (num2Chars[l2]-48)+carry;

			sb.append(sum%10);

			carry = sum/10;

			--l2;
		}

		if(carry > 0)
			sb.append(carry);

		System.out.println(sb.reverse());
	}
}