import java.util.Map;
import java.util.HashMap;

class ContainsDuplicate2{

	public static void main(String z[]){

		System.out.println("Contains duplicate 2");

		int k = 1;
		int[] a = {1};

		int length = a.length;
		int windowSize = length;

		if(a.length-1 > k){
			windowSize = k;
			length = length - windowSize;
		}

		System.out.println("Window : "+windowSize);
		System.out.println("Length : "+length);
		
		for(int i =0; i < length; ++i){

			for(int j = i+1; j < i+windowSize+1; ++j){

				if(a[i] == a[j]){
					System.out.println("true");
					return;
				}
			}
		}

		System.out.println("false");

	}
}