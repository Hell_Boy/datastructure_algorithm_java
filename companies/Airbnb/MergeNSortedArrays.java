import java.util.Arrays;

class MergeKSortedArrays{
	
	public static void main(String z[]){

		System.out.println("Merge k sorted arrays");

		int[][] a = {
			{1,4,5},
			{1,3,4},
			{2,6}
		};

		int[] sortedArray = new MergeKSortedArrays().merge(a, 0, a.length-1);

		System.out.println(Arrays.toString(sortedArray));

	}

	private int[] merge(int[][] a , int start, int end){

		if(start >= end)
			return a[ss];

		int mid = start+(end-start)/2;

		int[] l = merge(a, start, mid);
		int[] r = merge(a, mid+1, end);


		int i =0, j =0, k =0;
		int[] temp = new int[l.length+r.length];

		while(i < l.length && j < r.length){

			if(l[i] < r[j]){
				temp[k] = l[i];
				++i;
			}else{
				temp[k] = r[j];
				++j;
			}
			++k;
		}

		while(i < l.length){
			temp[k] = l[i];
			++i;
			++k;
		}

		while(j < r.length){
			temp[k] = r[j];
			++j;
			++k;
		}

		return temp;
		
	}
}