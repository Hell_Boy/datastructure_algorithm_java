import java.util.*;
import java.io.*;

class AllFib{

	
	public static void main(String z[]) throws IOException{

		System.out.println("All Fibonacci");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int n = Integer.parseInt(br.readLine());

		new Fibonacci(n).printFibonacciSeries();
	}

}

class Fibonacci{

	int n;
	int[] m;
	public Fibonacci(int n){
		this.n = n;
		m = new int[n+1];
	}

	public void printFibonacciSeries(){

		for(int i = 0; i < n+1; ++i){

			int fibVal = fib(i);
			String result = String.format("Fibonacci sum of number %d is %d", i, fibVal);
			System.out.println(result);
		}
	}

	private int fib(int n){

		if( n <= 0)
			return 0;

		if(n == 1)
			return 1;

		if(m[n] > 0)
			return m[n];

		m[n] = fib(n-1)+fib(n-2);

		return m[n];
	}
}