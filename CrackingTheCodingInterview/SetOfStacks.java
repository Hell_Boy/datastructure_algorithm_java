import java.util.EmptyStackException;

class SetOfStacks extends MyStack5<Integer>{

	// public static void main(String z[]){

	// 	System.out.println("Set of stacks");

	// }

	ArrayList<MyStack5<Integer>>  al;

	public SetOfStacks(){
		al = new ArrayList<MyStack5<Integer>>();
	}

	int size = 5;
}
 


// Todo : Add size limit
class MyStack5<T>{

	public static class StackNode<T>{

		private T data;
		private StackNode<T> next;

		public StackNode(T data){
			this.data = data;
		}
	}

	private StackNode<T> top;

	public void push(T item){

		StackNode<T> t = new StackNode<T>(item);
		t.next = top;
		top = t;
	}

	public T pop(){
		if(top == null)
			throw new EmptyStackException();

		T item = top.data;
		top = top.next;

		return item;
	}

	public T peek(){

		if(top == null)
			throw new EmptyStackException();
		return top.data;
	}
}