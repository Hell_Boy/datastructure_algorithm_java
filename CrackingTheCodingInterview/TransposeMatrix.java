
import java.util.Arrays;

class TransposeMatrix{

	public static void main(String z[]){

		int[][] matrix = {
			{1,2,3},
			{4,5,6},
			{7,8,9}
		};


		System.out.println("Input matrix : ");

		for(int[] row : matrix){

			System.out.println(Arrays.toString(row));
		}

		int rows = matrix.length;
		int columns = matrix[0].length;

		System.out.println("rows : "+rows+" columns : "+columns);

		int[][] tMatrix = new int[rows][columns];

		int trow = 0;

		for(int i =0; i < columns; ++i){
			for(int j =0; j < rows; ++j){

				tMatrix[i][j] = matrix[j][trow];
			}

			++trow;
		}


		for(int[] row : tMatrix){

			System.out.println(Arrays.toString(row));
		}
	}
}