import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class StringCompression{

	public static void main(String z[]) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Input String : ");
		String input = br.readLine();

		char[] c = input.toCharArray();

		char prev = '\0';

		System.out.println("Prev : "+prev);
		int i =0, count =0;

		StringBuilder sb = new StringBuilder();

		while(i < c.length){

			if(c[i] != prev){

				if(prev !='\0'){
					sb.append(prev);
					sb.append(count);
				}

				prev = c[i];
				count = 1;
			}
			else
			++count;
			++i;
		}

		if(prev != '\0'){
			sb.append(prev);
			sb.append(count);
		}

		System.out.println("Compressed string : "+sb.toString());
	}
}