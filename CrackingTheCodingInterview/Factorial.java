import java.util.*;
import java.io.*;

class Factorial{

	public static void main(String z[]) throws IOException{

		System.out.println("Factorial");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		long n = Long.parseLong(br.readLine());

		long fact = new Factorial().factorial(n);

		String result = String.format("Factorial of number %d is %d", n, fact);

		System.out.println(result);


	}

	private long factorial(long n ){

		if(n < 0)
			return -1;

		if(n ==  0)
			return 1;

		return n * factorial(n-1);
	}
}