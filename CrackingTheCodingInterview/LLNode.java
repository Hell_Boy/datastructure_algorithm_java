
class LLNode{

	LLNode next;
	int data;

	public LLNode(){}

	public LLNode(int data){
		this.data = data;
	}

	public void append(int val){
		LLNode n = this;

		while(n != null){
			System.out.println("itr => "+n.data);
			n = n.next;
		}

		System.out.println("Appending : "+val);
		n = new LLNode(val);
	}
}
