
import java.util.HashMap;
import java.util.Map;

class PallindromicPermutation{

	public static void main(String z[]){

		String input = "tact coa";
		System.out.println("String input : "+input);

		char [] chars = input.toCharArray();


		Map<Character, Integer> map = new HashMap<Character, Integer>();

		for(char c : chars){

			int min = 'a';
			int max = 'z';

			min--;
			max++;

			if(c > min && c < max){

				if(map.containsKey(c)){
					int tempVal = map.get(c);
					tempVal++;
					map.put(c,tempVal);
				}else{
					map.put(c, 1);
				}
			}
		}


		int oddCharacterCount = 0;

		for(Map.Entry<Character, Integer> e : map.entrySet()){

			String log = String.format("Key : %c Value : %d", e.getKey(), e.getValue());
			System.out.println(log);

			if(e.getValue() % 2 != 0)
				++oddCharacterCount;
		}

		if(oddCharacterCount < 2)
			System.out.println("Is a pallindromic permutation");
		else
			System.out.println("Not a pallindromic permutation");
	}
}