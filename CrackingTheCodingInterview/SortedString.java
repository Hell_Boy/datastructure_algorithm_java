import java.util.*;
import java.io.*;

class SortedStrings{

	int numChars = 26;

	public static void main(String z[]) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());

		new SortedStrings().printSortedString(n, "");
	}

	private char ithLetter(int i){
		return (char)(((int)'a')+i);
	}

	private void printSortedString(int remaining, String prefix){


		if(remaining == 0){

			if(new SortedStrings().isInOrder(prefix))
			System.out.println(prefix);
		}

		else{

			for(int i =0; i < 26; ++i){
				char c = new SortedStrings().ithLetter(i);
				printSortedString(remaining-1, prefix+c);
			}
		}
	}

	private boolean isInOrder(String s){

		for(int i =1; i < s.length(); ++i){

			if((int)s.charAt(i-1) > (int)s.charAt(i))
				return false;
		}

		return true;
	}
}