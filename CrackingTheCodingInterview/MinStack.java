
class MinStack{

	public static void main(String z[]) throws Exception{

		System.out.println("Min stack");

		int[] a = {3,5,2,7,0,4,7,8,9};

		MyStack3 stack = new MyStack3();

		for(int elem : a)
			stack.push(elem);

		System.out.println("Top : "+stack.pop());

		System.out.println("Min : "+stack.min());
	}
}

class MyStack3{


	public static class StackNode{

		int data;
		StackNode next;

		public StackNode(int data){
			this.data = data;
		}
	}

	StackNode top = null;
	StackNode min = null;

	public void push(int item){

		StackNode t = new StackNode(item);

		t.next = top;
		top = t;

		if(min == null || item < min.data)
			min = t;
	}

	public int pop() throws Exception {

		if(top == null)
			throw new Exception("Stack is empty");

		StackNode temp = top;
		top = top.next;

		return temp.data;

	}

	public int peek() throws Exception {

		if(top == null)
			throw new Exception("Stack is empty");
		return top.data;
	}

	public int min() throws Exception {

		if(min == null)
			throw new Exception("Stack is empty");
		return min.data;
	}

	public boolean isEmpty(){

		return top == null;
	}

}