import java.util.*;
import java.io.*;

class PrimeNumber{

	public static void main(String z[]) throws IOException{

		System.out.println("Prime number check");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int n = Integer.parseInt(br.readLine());

		for(int i =2; i*i <= n ; ++i){

			if(n%i == 0){

				System.out.println(n+" is not a prime number");
				return;
			}
		}

		System.out.println(n+" is a prime number");
	}
}