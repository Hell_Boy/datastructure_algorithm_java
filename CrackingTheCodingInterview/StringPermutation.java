import java.util.*;
import java.io.*;

class StringPermutation{

	public static void main(String z[]) throws IOException{

		System.out.println("String permutation\n");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String input = br.readLine();

		System.out.println("Input string : "+input);

		System.out.println("\nPermutations");

		new StringPermutation().permutation(input,"");

	}

	private void permutation(String input, String prefix){

		if(input.length() == 0)
			System.out.println(prefix);

		for(int i =0; i < input.length(); ++i){
			String temp = input.substring(0, i )+ input.substring(i+1);
			permutation(temp, (prefix+input.charAt(i)));
		}
	}
}