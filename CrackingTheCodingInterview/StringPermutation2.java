

class StringPermutation2{

	public static void main(String z[]){

		String input = "abc";
		String secondString = "cbd";
		System.out.println("String1 : "+input+" String2 : "+secondString);

		new StringPermutation2().permutation(secondString, input, "");
	}

	private void permutation(String secondString, String prefix, String str){


		if(prefix.length() == 0){
			System.out.println(str);

			// map.put(str, true);

			if(secondString.equals(str))
				System.out.println("Match found");
		}

		else{

			for(int i =0; i < prefix.length(); ++i){
				String temp = prefix.substring(0,i)+prefix.substring(i+1);
				permutation(secondString, temp, (str+prefix.charAt(i)));

			}
		}
	}

}