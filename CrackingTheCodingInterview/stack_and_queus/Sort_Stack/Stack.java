import java.util.EmptyStackException;


class Stack<T>{


	private static class StackNode<T>{

		private T data;
		private StackNode<T> next;

		public StackNode(T data){
			this.data = data;
		}
	}

	private StackNode<T> top;


	public void push(T elem){

		StackNode<T> t = new StackNode<T>(elem);
		t.next = top;

		top = t;
	}


	public T pop(){

		if(top == null)
			throw new EmptyStackException();

		T temp = top.data;
		top = top.next;
		return temp;
	}

	public boolean isEmpty(){
		return top == null;
	}

	public T peek(){

		if(top == null)
			throw new EmptyStackException();

		return top.data;
	}
}