
class SortStack {
	
	public static void main(String z[]){

		int[] a = {2,4,5,2,1,8,9,5};

		Stack<Integer> ss = new Stack<Integer>();

		System.out.println("Unsorted stack : ");


		for(int elem : a){

			System.out.println("elem: "+elem);

			if( ss.isEmpty() ||  ss.peek() < elem)
				ss.push(elem);

			else{

				Stack<Integer> ss1 = new Stack<Integer>();

				while(!ss.isEmpty() && ss.peek() > elem)
					ss1.push(ss.pop());

					ss.push(elem);
				while(!ss1.isEmpty())
					ss.push(ss1.pop());
			}

		}

		System.out.println("\nSorted stack");
		while(!ss.isEmpty())
			System.out.println("elem: "+ss.pop());
	}
}