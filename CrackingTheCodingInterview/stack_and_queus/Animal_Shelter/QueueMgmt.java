import java.util.NoSuchElementException;

class QueueMgmt{

	public CustomQueue<Animal> dog, cat;

	public QueueMgmt(){
		dog = new CustomQueue<Animal>();
		cat = new CustomQueue<Animal>();

	}

	public Animal getAny(){

		if(dog.isEmpty() && cat.isEmpty())
			throw new NoSuchElementException();

		if(dog.isEmpty())
			return cat.remove();

		if(cat.isEmpty())
			return dog.remove();

		Animal selectedAnimal = dog.peek().timeStamp < cat.peek().timeStamp ? dog.remove() : cat.remove();

		return selectedAnimal;
	}


	public void add(boolean isDog){

		if(isDog){
			dog.add(new Animal("dog"));
		}else{
			cat.add(new Animal("cat"));
		}

	}

	public Animal getCat(){
		if(cat.isEmpty())
			throw new NoSuchElementException();
		return cat.remove();
	}

	public Animal getDog(){
		if(dog.isEmpty())
			throw new NoSuchElementException();
		return dog.remove();
	}

}