class ShelterMain{

	public static void main(String z[]){

		System.out.println("Shelter main");

		int[] a = {1,0,1,0,1,0,1,1,1,1,0,0,0,1,0};

		QueueMgmt queue = new QueueMgmt();

		for(int elem : a){

			queue.add((elem == 1));
		}


		System.out.println("Dogs");
		while(!queue.dog.isEmpty()){
			Animal d = queue.dog.remove();
			String result = String.format("%s : %d", d.type, d.timeStamp);
			System.out.println(result);
		}

		System.out.println("Cats");
		while(!queue.cat.isEmpty()){
			Animal c = queue.cat.remove();
			String result = String.format("%s : %d", c.type, c.timeStamp);
			System.out.println(result);
		}

		// Todo : Implement the remaining dequeue feature

	}
}