
import java.util.*;

class ReverseArray{

	public static void main(String z[]){

		int[] a = { 1,2,3,4,5,6};

		System.out.println("Input array : "+Arrays.toString(a));

		int length = a.length;


		for(int i =0; i < (length/2); ++i){

			int other = length-1-i;
			int temp = a[i];
			a[i] = a[other];
			a[other] = temp;
		}

		System.out.println("Reversed array : "+Arrays.toString(a));
	}
}