import java.util.*;
import java.io.*;

class FibonacciSeriesSum{

	public static void main(String z[]) throws Exception{

		System.out.println("Fibonacci series sum");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter number : ");

		int n = Integer.parseInt(br.readLine());

		int sum = new FibonacciSeriesSum().fib(n);

		String result = String.format("Fibonacci series sum for %d is %d", n, sum);

		System.out.println("Result : "+result);


	}

	int fib(int n){

		if(n <= 0 )
			return 0;

		else if(n ==1)
			return 1;

		return fib(n-1)+fib(n-2);
	}
}