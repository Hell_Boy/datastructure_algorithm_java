
class EntryPoint{

	public static void main(String z[]) throws Exception{

		System.out.println("Entry point");

		int [] a = {3,5,2,6,7,8,9,1,3,6};

		// MyStack<Integer> stack = new MyStack<Integer>();

		// for(int elem : a){

		// 	stack.push(elem);
		// }

		// System.out.println("Stack elem : ");

		// while(!stack.isEmpty()){
		// 	int topElem = stack.pop();

		// 	System.out.println("Top : "+topElem);
		// }	


		// SizedStack stack = new SizedStack();

		// for(int elem : a){

		// 	System.out.println("isFull : "+stack.isFull());
		// 	System.out.println("isEmpty : "+stack.isEmpty());

		// 	System.out.println("Pushing : "+elem);
		// 	stack.push(elem);
		// }

		SetOfStack ss = new SetOfStack();

		for(int elem : a){
			ss.push(elem);
		}

		while(!ss.isEmpty()){

			System.out.println("Elem : "+ss.pop());
			System.out.println("SS is empty : "+ss.isEmpty());
		}
	}
}