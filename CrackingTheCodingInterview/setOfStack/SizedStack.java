
import java.util.EmptyStackException;

class SizedStack extends MyStack<Integer>{

	int size = 5;
	int currSize = 0;


	public void push(int data) throws Exception{

		if(currSize < size+1){
			super.push(data);
			++currSize;
		}

		else
			throw new Exception("Stack overflow");
	}

	public Integer pop(){

		if(currSize > 0){
			--currSize;
			return super.pop();
		}else{
			throw new EmptyStackException();
		}

	}

	public boolean isFull(){

		return currSize-1 == size;
	}

	public boolean isEmpty(){

		return currSize == 0;
	}

}