import java.util.ArrayList;
import java.util.List;
import java.util.EmptyStackException;

class SetOfStack {

	List<SizedStack> lss;

	SizedStack ss;

	public SetOfStack(){

		ss = new SizedStack();

		lss = new ArrayList<SizedStack>();
	}


	public void push(int elem) throws Exception{

		System.out.println("Pushing : "+elem);
		if(ss.isFull()){

			SizedStack tss = new SizedStack();
			tss.push(elem);
			lss.add(tss);

			ss = tss;

		}else{

			ss.push(elem);
		}
	}

	public int pop(){

		if(lss == null || lss.isEmpty())
			throw new EmptyStackException();
		
		SizedStack temp = lss.get(lss.size()-1);

		System.out.println("Size : "+lss.size());


		if(!temp.isEmpty()){
			int top = temp.pop();

			System.out.println("temp is empty : "+ss.isEmpty());

			if(temp.isEmpty())
				lss.remove(lss.size()-1);

			return top;
		}

		return Integer.MIN_VALUE;

	}

	public boolean isEmpty(){

		return lss == null || lss.isEmpty();
	}

}