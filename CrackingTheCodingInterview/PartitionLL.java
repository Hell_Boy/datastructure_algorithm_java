
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.Integer;

class PartitionLL{

	public static void main(String z[]) throws IOException{

		System.out.println("Partition linked list");

		int[] a = {10,2,3,4,5,6,10,3,4,2,9,6,3,6};

		Node head = new Node(a[0]);

		for(int i =1; i <a.length; ++i){

			head.append(a[i]);
		}

		System.out.print("Linked list : ");

		Node temp = head;

		while(temp != null){

			System.out.print(temp.data+" ");
			temp = temp.next;
		}

		System.out.print("\nEnter partition val : ");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int pivot = Integer.parseInt(br.readLine());

		Node pivotLeft = null;
		Node pivotRight = null;

		temp = head;

		while(temp != null){

			if(temp.data < pivot){

				if(pivotLeft == null)
					pivotLeft = new Node(temp.data);
				else
					pivotLeft.append(temp.data);

			}else{

				if(pivotRight == null)
					pivotRight = new Node(temp.data);
				else
					pivotRight.append(temp.data);

			}

			temp = temp.next;
		}

		System.out.print("Left LL : ");

		temp = pivotLeft;

		while(temp != null){
			System.out.print(temp.data+" ");
			temp = temp.next;
		}

		System.out.print("\nRight LL : ");

		temp = pivotRight;
		while(temp != null){
			System.out.print(temp.data+" ");
			temp = temp.next;
		}

		System.out.print("\nMerged LL : ");

		while( pivotLeft != null || pivotRight != null){

			if(pivotLeft != null){
				System.out.print(pivotLeft.data+" ");
				pivotLeft = pivotLeft.next;
			}else{

				System.out.print(pivotRight.data+" ");
				pivotRight = pivotRight.next;
			}
		}

		System.out.println();

	}
}

class Node{

	Node next;
	int data;

	public Node(int data){
		this.data = data;
	}


	public void append(int data){

		Node curr = this;
		Node prev = curr;

		while(curr != null){

			prev = curr;
			curr = curr.next;
		}

		prev.next = new Node(data);
	}
}