import java.util.*;
import java.io.*;

class UniqueCharacterInString{


	public static void main(String z[]) throws IOException{

		System.out.print("Enter the string : ");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String input = br.readLine();


		Map<Character, Integer> map = new HashMap<Character, Integer>();

		char[] chars = input.toCharArray();

		for(char c : chars){

			if(map.containsKey(c)){

				System.out.println("Not all characters are unique.");
				return;
			}

			map.put(c, 1);
		}

		System.out.println("Unique characters string.");
	}
}