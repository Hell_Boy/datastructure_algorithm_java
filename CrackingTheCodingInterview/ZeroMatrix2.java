import java.util.Arrays;

class ZeroMatrix2{

	public static void main(String z[]){

		int[][] matrix = {
			{1, 2, 3, 4},
			{5, 0, 6, 7},
			{8, 9, 10, 0},
			{11, 12, 13, 14}
		};

		System.out.println("\nInput : ");
		for(int[] rowElem : matrix)
			System.out.println(Arrays.toString(rowElem));


		int row = matrix.length;
		int col = matrix[0].length;


		for(int i =0; i <row; ++i)
			for(int j =0; j<col; ++j)
				if(matrix[i][j] == 0){
					matrix[i][0] = 0;
					matrix[0][j] = 0;
				}

		// Nullifying row
		for(int i =0; i <row; ++i)
			if(matrix[i][0] == 0)
				for(int j =0; j <col; ++j)
					matrix[i][j] = 0;

		// Nullifying col
		for(int i =0; i <col; ++i)
			if(matrix[0][i] == 0)
				for(int j =0; j <row; ++j)
					matrix[j][i] = 0;

		System.out.println("\nOutput : ");
		for(int[] rowElem : matrix)
			System.out.println(Arrays.toString(rowElem));


	}
}