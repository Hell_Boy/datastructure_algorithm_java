
class ReplaceWhiteSpace{
	
	public static void main(String zp[]){

		String input = "Mr John Smith ";

		System.out.println("String input : "+input);

		char[] chars = input.toCharArray();

		StringBuilder sb = new StringBuilder();

		for(int i =0; i < chars.length; ++i){

			if(chars[i] == ' ')
				sb.append("%20");
			else
				sb.append(chars[i]);
		}

		System.out.println("String output : "+sb.toString());
	}
}