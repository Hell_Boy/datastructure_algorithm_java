
class DeleteMiddleNode{

	public static void main(String z[]){

		int[] a = {3,4,5,6,2,1,4,8,9};

		Node head = new Node(a[0]);

		for(int i =1; i < a.length; ++i){

			head.append(a[i]);
		}

		System.out.print("List : ");

		Node temp = head;

		while(temp != null){

			System.out.print(temp.data+" ");
			temp = temp.next;
		}

		System.out.print("\nMiddle elem : ");

		
		if(head == null){

			System.out.println("No element to delete");
			return;
		}else if(head.next == null){
			System.out.println("Deleting node : "+head.data);
			head = null;
			return;
		}

		Node secondPointer = head;
		Node prev = secondPointer;
		Node firstPointer = head.next;

		while(firstPointer != null && firstPointer.next != null){

			prev = secondPointer;
			secondPointer = secondPointer.next;
			firstPointer = firstPointer.next.next;
		}

		System.out.println("Deleting middle elem : "+secondPointer.data);

		prev.next = secondPointer.next;
		secondPointer = null;

		System.out.print("Updated list : ");

		while(head != null){

			System.out.print(head.data+" ");
			head = head.next;
		}

		System.out.println();

	}
}

class Node{

	Node next;
	int data;

	public Node(int data){
		this.data = data;
	}

	public void append(int data){

		Node curr = this;

		Node prev = curr;

		while(curr != null){
			prev = curr;
			curr = curr.next;
		}

		prev.next = new Node(data);
	}
}