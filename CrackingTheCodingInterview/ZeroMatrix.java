import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;

class ZeroMatrix{

	public static void main(String z[]) throws Exception{

		// BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int[][] matrix = {
			{1, 2, 3, 4},
			{5, 0, 6, 7},
			{8, 9, 10, 0},
			{11, 12, 13, 14}
		};

		int row = matrix.length;
		int col = matrix[0].length;

		boolean[] rows = new boolean[row];
		boolean[] cols = new boolean[col];


		for(int i =0; i <row; ++i)
			for(int j =0; j <col; ++j)

				if(matrix[i][j] == 0){
					rows[i] = true;
					cols[j] = true;
				}

		System.out.println("rows : "+Arrays.toString(rows));
		System.out.println("cols : "+Arrays.toString(cols));


		// Nullifying row
		for(int i =0; i < row; ++i)

			if(rows[i]){

				for(int j= 0; j <col; ++j)
					matrix[i][j] = 0;
			}


		// Nullifying col
		for(int i =0; i < col; ++i)

			if(cols[i]){

				for(int j= 0; j <row; ++j)
					matrix[j][i] = 0;
			}

		for(int[] rowElem : matrix)
			System.out.println(Arrays.toString(rowElem));
		
	}
}