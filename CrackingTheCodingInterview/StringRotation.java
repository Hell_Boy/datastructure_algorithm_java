import java.util.Arrays;
import java.lang.StringBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class  StringRotation{

	public static void main(String z[]) throws IOException{
		// System.out.println("String rotation");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("System In (string) : ");

		char[] inputChars = br.readLine().toCharArray();

		System.out.println("Input : "+Arrays.toString(inputChars));

		System.out.print("System In (pivot) : ");

		int pivot = Integer.parseInt(br.readLine());

		System.out.println("Pivot : "+pivot);

		char c = '\0';

		for(int i =0; i <inputChars.length; ++i){
			c = inputChars[i];

			if(i < pivot){	
				inputChars[i] = inputChars[i+pivot];
				inputChars[i+pivot] = c;
			}else{

				inputChars[i] = inputChars[pivot-1];
				inputChars[pivot-1] = c;
			}
		}

		System.out.println("Rotated chars");

		System.out.println("Input : "+Arrays.toString(inputChars));
	}
}
