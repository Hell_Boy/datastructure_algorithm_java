import java.util.*;
import java.io.*;

class GuessSquareRoot{

	public static void main(String z[]) throws IOException{

		System.out.println("Guess square root");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int n = Integer.parseInt(br.readLine());

		System.out.println(String.format("Square root of number %d is %d", n, new GuessSquareRoot().sqrt(n)));

		System.out.println(String.format("Square root of number %d is %d", n, new GuessSquareRoot().sqrt_2(n)));


	}

	private int sqrt_2(int n){

		for(int i = 1; i*i < n+1; ++i){

			if(i*i == n)
				return i;
		}

		return -1;
	}


	private int sqrt(int n){
		return sqrt_helper(n, 1, n);
	}


	private int sqrt_helper(int n , int min, int max){

		if(max < min) return -1;

		int guess = (min+max)/2;

		int temp = guess*guess;

		if(temp == n)
			return guess;

		else if (temp < n)
			return sqrt_helper(n, guess+1, max);

		return sqrt_helper(n, min, guess-1);
	}
}