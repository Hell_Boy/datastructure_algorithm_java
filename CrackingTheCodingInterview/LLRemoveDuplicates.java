
import java.util.LinkedList;
import java.util.Collections;
import java.util.Iterator;

class LLRemoveDuplicates{

	public static void main(String z[]){

		LinkedList<Integer> ll = new LinkedList<Integer>();

		ll.add(3);
		ll.add(4);
		ll.add(1);
		ll.add(5);
		ll.add(2);
		ll.add(5);
		ll.add(3);
		
		Collections.sort(ll);

		System.out.println(ll.toString());

		LinkedList<Integer> ll2 = new LinkedList<Integer>();

		Iterator it = ll.iterator();

		int prev = Integer.MIN_VALUE;
		if(it.hasNext()){
			prev = (int) it.next();
			ll2.add(prev);
		}

		while(it.hasNext()){

			int temp =(int) it.next();

			if(temp != prev){
				prev = temp;
				ll2.add(prev);
			}
		}


		System.out.println(ll2.toString());

	}
}