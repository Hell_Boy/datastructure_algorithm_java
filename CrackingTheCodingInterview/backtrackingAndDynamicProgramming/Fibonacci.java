
class Fibonacci{


  public static void main(String z[]){

    System.out.println("Fibonacci recursive\n");

    int num = 30, fibSum = 0;

    long startTime = 0, stopTime = 0;

    String format = "%s sum : %d \nExecution Time : %d\n";
//  Recursion
    startTime = System.nanoTime();
    fibSum = new Fibonacci().recursive(num);
    stopTime = System.nanoTime();
    System.out.println(String.format(format, "Recursive", fibSum, (stopTime-startTime)));


// Memoization
    startTime = System.nanoTime();
    fibSum = new Fibonacci().memoization(num, new int[num+1]);
    stopTime = System.nanoTime();
    System.out.println(String.format(format, "Memoization", fibSum, (stopTime-startTime)));

// Dynamic programming
    startTime = System.nanoTime();
    fibSum = new Fibonacci().dp(num);
    stopTime = System.nanoTime();
    System.out.println(String.format(format, "DP", fibSum, (stopTime-startTime)));

// Two numbers
   startTime = System.nanoTime();
   fibSum = new Fibonacci().twoNums(num);
   stopTime = System.nanoTime();
   System.out.println(String.format(format, "Two", fibSum, (stopTime-startTime)));

  }

  public int recursive(int n){

    if(n == 0 || n == 1)
    return n;

    return recursive(n-1) + recursive(n-2);
  }

// Also known as Top Down recursion
  public int memoization(int n, int[] memo){

    if(n == 0 || n == 1)
    return n;

    if(memo[n] == 0)
      memo[n] = memoization(n-1, memo) + memoization(n-2, memo);

    return memo[n];
  }

// Also known as Bottom up recursion
  public int dp(int n){

    if(n == 0 || n == 1)
    return n;

    int[] memo = new int[n+1];
    memo[0] = 0;
    memo[1] = 1;

    for(int i = 2; i < n+1; ++i)
      memo[i] = memo[i-1]+ memo[i-2];

    return memo[n];
  }

// Two numbers
  public int twoNums(int n){

    if(n == 0 || n == 1)
    return n;

    int a = 0, b = 1, c = 0;

    for(int i = 2; i < n+1; ++i){
      c = a+b;
      a = b;
      b = c;
    }

    return b;
  }

}
