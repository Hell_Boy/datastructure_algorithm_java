

class StackTest{

	public static void main(String z[])throws Exception{


		System.out.println("Stack test");

		int[] a = {2,5,6,3,5,0,8,6};

		MyStack2<Integer> stack = new MyStack2<Integer>();

		for(int elem : a){

			stack.push(elem);

		}

		System.out.println(stack.pop());
	}
}

class MyStack2<T>{
	
	public static class StackNode<T>{

		private T data;
		private StackNode<T> next;

		public StackNode(T data){

			this.data = data;
		}
	}

	private StackNode<T> top = null;

	public void push(T item){

		StackNode<T> t = new StackNode<T>(item);

		t.next = top;
		top = t;
	}

	public T pop() throws Exception{

		if(top == null)
			throw new Exception("Empty stack");
		T item = top.data;
		top = top.next;
		return item;
	}

	public T peek() throws Exception{

		if(top == null)
			throw new Exception("Empty stack");
		return top.data;
	}

	public boolean isEmpty(){
		return top == null;
	}
}