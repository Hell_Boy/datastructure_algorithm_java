import java.util.Arrays;
import java.util.Queue;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.math.*;


class BinarySearchTree{
	
	public static void main(String z[]){

		System.out.println("Binary search tree");
		int[] a = {1,3,4,2};

		Arrays.sort(a);

		System.out.println("Sorted array : "+Arrays.toString(a));

		TreeNode tree = new BinarySearchTree().build(a, 0, a.length-1);
		System.out.print("Pre Order : ");
		new BinarySearchTree().preOrderTraversal(tree);
		System.out.println();


		System.out.print("In Order : ");
		new BinarySearchTree().inOrderTraversal(tree);
		System.out.println();


		// Iterative
		new BinarySearchTree().levelOrderTraversal(tree);

		// Reccursive
		ArrayList<LinkedList<TreeNode>> levels = new BinarySearchTree().levelOrderTraversalRecur(tree, new ArrayList<LinkedList<TreeNode>>(), 0);

		int levelCount = 0;
		for(LinkedList<TreeNode>  level : levels){
			++levelCount;

			System.out.print(levelCount+" : ");
			for(TreeNode node : level){
				System.out.print(node.data+" ");
			}

			System.out.println();
		}

		// Height;
		System.out.println("Height : "+new BinarySearchTree().height(tree));

		// IsBalanced
		System.out.println("Is balanced I : "+new BinarySearchTree().isBalancedUsingActualHeight(tree));

		// IsBalance II
		System.out.println("Is balanced II : "+(new BinarySearchTree().isBalancedUsingCheckDiff(tree) != Integer.MIN_VALUE));

		// IsBST
		System.out.println("Is BST : "+new BinarySearchTree().isBST(tree, null, null));

		// Search in BST
		TreeNode node = new BinarySearchTree().searchBST(tree, 1);

		if(node == null)
			System.out.println("No element found");
		else
			System.out.println("Element found");

		// // InOrder Successor
		TreeNode succ = new BinarySearchTree().inOrderSuccessor(tree, node);
		if(succ != null)
			System.out.println("Inorder successor : "+succ.data);
		else
			System.out.println("Successor not found");


		// All possible combination
		// new BinarySearchTree().possibleCombination(a, 0, new int[a.length]);
	}

	// private void possibleCombination(int[] a, int index, int[] b){

	// 	for(int i = index; i < a.length; ++i){

	// 		b = new int[a.length];
	// 		b[0] = a[i];

	// 		int j =1;

	// 		while(j < i){
	// 			b[j] = a[j];
	// 			++j;
	// 		}

	// 		j = i+1;

	// 		while(j < a.length){
	// 			b[j] = a[j];
	// 			++j;
	// 		}

	// 		System.out.println(Arrays.toString(b));

	// 		possibleCombination(a, ++index, b);
	// 	}

	// 	return;
	// }

	private TreeNode searchBST(TreeNode root, int data){

		if(root == null)
			return null;

		if(root.data == data)
			return root;

		if(root.data > data)
			return searchBST(root.left, data);

		return searchBST(root.right, data);
	}

	private TreeNode inOrderSuccessor(TreeNode root, TreeNode node){


		if(root == null || node == null)
			return null;

		TreeNode succ = null;

		if(node.right != null){
			node = node.right;

			while(node.left != null)
				node = node.left;
			succ = node;

		}else{

			while(root != null){


				if(node.data < root.data){

					succ = root;
					root = root.left;
				}else if(node.data > root.data){
					root = root.right;
				}else{
					break;
				}
			}
		}

		return succ;
	}

	private boolean isBST(TreeNode root, Integer min, Integer max){

		if(root == null)
			return true;

		if((min != null && root.data < min) || (max != null && root.data > max)){
			return false;
		}
		if (!isBST(root.left, min, root.data) || !isBST(root.right, root.data, max))
			return false;
		return true;
	}


	private int isBalancedUsingCheckDiff(TreeNode root){

		if(root == null)
			return -1;

		int left = isBalancedUsingCheckDiff(root.left);

		if(left == Integer.MIN_VALUE)
			return Integer.MIN_VALUE;

		int right = isBalancedUsingCheckDiff(root.right);

		if(right == Integer.MIN_VALUE)
			return Integer.MIN_VALUE;


		if(Math.abs(left-right) > 1)
			return Integer.MIN_VALUE;

		return Math.max(isBalancedUsingCheckDiff(root.left), isBalancedUsingCheckDiff(root.right)) + 1;

	}

	private boolean isBalancedUsingActualHeight(TreeNode root){

		if(root == null)
			return true;

		int diff = height(root.left)-height(root.right);

		if(Math.abs(diff) > 1)
			return false;

		return isBalancedUsingActualHeight(root.left) && isBalancedUsingActualHeight(root.right);
	}

	private int height(TreeNode root){

		if(root == null)
			return -1;

		return Math.max(height(root.left), height(root.right))+1;
	}

	private ArrayList<LinkedList<TreeNode>> levelOrderTraversalRecur(TreeNode root, ArrayList<LinkedList<TreeNode>> lists, int level){

		if(root == null)
			return lists;

		LinkedList<TreeNode> list = null;
		if(lists.size() == level){
			list = new LinkedList<TreeNode>();
			lists.add(list);
		}else{
			list = lists.get(level);
		}

		list.add(root);

		++level;
		levelOrderTraversalRecur(root.left, lists, level);
		levelOrderTraversalRecur(root.right, lists, level);

		return lists;
	}

	private void levelOrderTraversal(TreeNode root){

		if(root == null)
			return;

		Queue<TreeNode> q = new LinkedList<TreeNode>();

		q.offer(root);
		q.offer(null);

		TreeNode top;

		List<Integer> levelNodes = new LinkedList<Integer>();
		int level = 0;

		while(!q.isEmpty()){

			top = q.poll();

			if(top != null){

				levelNodes.add(top.data);

				if(top.left != null)
					q.offer(top.left);

				if(top.right != null)
					q.offer(top.right);


			}else{
				++level;

				System.out.println("level "+level+" : "+levelNodes);
				levelNodes = new LinkedList<Integer>();

				if(!q.isEmpty())
					q.offer(null);
			}
		}

	}

	private void inOrderTraversal(TreeNode root){

		if(root == null)
			return;

		inOrderTraversal(root.left);
		System.out.print(root.data+" ");
		inOrderTraversal(root.right);
	}

	private void preOrderTraversal(TreeNode root){

		if(root == null)
			return;

		System.out.print(root.data+" ");
		preOrderTraversal(root.left);
		preOrderTraversal(root.right);
	}

	private TreeNode build(int[] a, int start, int end){

		if(start > end)
			return null;

		int mid = start+(end-start)/2;
		TreeNode node = new TreeNode(a[mid]);
		node.left = build(a, start, mid-1);
		node.right = build(a, mid+1, end);

		return node;
	}
}