import java.util.List;
import java.util.ArrayList;
import java.util.Queue;
import java.util.LinkedList;

class GraphMain{
	
	static boolean[] visited;

	public static void main(String z[]){

		System.out.println("Graph");

		List<ArrayList<Integer>> graph = new GraphMain().graph();

		for(int i =0; i < graph.size(); ++i){
			System.out.println(i+" : "+graph.get(i));
		}

		System.out.println();


		// DFS
		visited = new boolean[7];
		new GraphMain().DFS(graph,0);
		System.out.println();

		// BFS
		visited = new boolean[7];
		new GraphMain().BFS(graph);
		System.out.println();


		// Search path
		visited = new boolean[7];
		boolean exist = new GraphMain().pathExist(graph, 3);
		System.out.println("Exist : "+exist);


	}

	private boolean pathExist(List<ArrayList<Integer>> graph, int elem){

		Queue<Integer> q = new LinkedList<Integer>();
		q.offer(0);

		int top = Integer.MIN_VALUE;

		while(!q.isEmpty()){
			top = q.poll();
			visited[top] = true;

			if(top == elem)
				return true;

			List<Integer> ll = graph.get(top);

			for(int val : ll){

				if(visited[val] == false){
					visited[val] = true;
					q.offer(val);
				}
			}
		}

		return false;
	}

	private void BFS(List<ArrayList<Integer>> graph){

		Queue<Integer> q = new LinkedList<Integer>();

		q.offer(0);

		while(!q.isEmpty()){

			int top = q.poll();

			System.out.println("BFS Item : "+top);

			visited[top] = true;

			List<Integer> items = graph.get(top);

			for(int item : items){

				if(visited[item] == false){

					visited[item] = true;
					q.offer(item);
				}
			}
		}


	}

	private void DFS(List<ArrayList<Integer>> graph, int index){

		visited[index] = true;

		System.out.println("DFS Item : "+index);

		ArrayList<Integer> items = graph.get(index);
		for(int item : items){		

			if(visited[item] == false)
				DFS(graph, item);
		}

		return;
	}

	private List<ArrayList<Integer>> graph(){

		ArrayList<Integer> l1 = new ArrayList<Integer>(){
			{
				add(1);
				add(4);
				add(5);
			}
		};

		ArrayList<Integer> l2 = new ArrayList<Integer>(){
			{
				add(3);
				add(4);
			}
		};

		ArrayList<Integer> l3 = new ArrayList<Integer>(){
			{
				add(1);
			}
		};

		ArrayList<Integer> l4 = new ArrayList<Integer>(){
			{
				add(2);
				add(4);
			}
		};

		ArrayList<Integer> l5 = new ArrayList<Integer>(){
			{
				add(4);
			}
		};

		ArrayList<Integer> l6 = new ArrayList<Integer>(){
			{
				add(5);
			}
		};

		
		List<ArrayList<Integer>> graphAdjacencyList = new ArrayList<ArrayList<Integer>>();
		graphAdjacencyList.add(l1);
		graphAdjacencyList.add(l2);
		graphAdjacencyList.add(l3);
		graphAdjacencyList.add(l4);
		graphAdjacencyList.add(l5);
		graphAdjacencyList.add(l6);
		
		return graphAdjacencyList;
	}
}