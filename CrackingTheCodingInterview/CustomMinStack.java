
public class CustomMinStack extends CustomStack<Integer>{

	CustomStack<Integer> tempMin;

	public CustomMinStack(){
		tempMin = new CustomStack<Integer>();
	}

	public void push(int data) throws Exception{

		try{

			if(tempMin.peek() > data){
				tempMin.pop();
				tempMin.push(data);
			}
		}catch(Exception e){
			tempMin.push(data);
		}

		super.push(data);
	}


	public int minStack() throws Exception{

		if(tempMin == null)
			throw new Exception("Empty min stack");
		return tempMin.pop();
	}
}


class CustomStack<T>{


	public static class StackNode<T>{

		private T data;
		private StackNode<T> next;

		public StackNode(T data){

			this.data = data;
		}
	}


	private StackNode<T> top;

	public void push(T item){

		StackNode<T> t = new StackNode<T>(item);
		t.next = top;
		top = t;
	}

	public T pop()throws Exception{

		if(top == null)
			throw new Exception("Empty stack");

		T item = top.data;
		top = top.next;
		return item;
	}

	public T peek() throws Exception{

		if(top == null)
			throw new Exception("Empty stack");

		return top.data;
	}
}