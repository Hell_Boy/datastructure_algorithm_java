
import java.util.Arrays;
import java.math.*;

class MaximalSquare{

	public static void main(String z[]){

		System.out.println("Matrix with maximum 1");

		// char[][] matrix = {{'1','0','1','0','0'},{'1','0','1','1','1'},{'1','1','1','1','1'},{'1','0','0','1','0'}};
		char[][]matrix = {{'1','0'}};
		// char[][] matrix = {{'1','1','1','1','0'},{'1','1','1','1','0'},{'1','1','1','1','1'},{'1','1','1','1','1'},{'0','0','1','1','1'}};

        
        int[][] temp = new int[matrix.length][matrix[0].length];

		int i =0, j =0;
		

		for(i =0; i < matrix.length; ++i){

			for(j =0; j < matrix[0].length; ++j){

				if(i == 0 || j == 0){
					if(matrix[i][j] == '1')
						temp[i][j] = 1;
					
				}

				else if(matrix[i][j] == '1'){

					temp[i][j] = 1;

					if(matrix[i-1][j] == '1' && matrix[i][j-1] == '1' && matrix[i-1][j-1] == '1')
						temp[i][j] = Math.min(Math.min(temp[i-1][j],temp[i][j-1]),temp[i-1][j-1]) +1;						

				}else
					temp[i][j] = 0;
				
			}
		}
	

		int maxVal = 0;

		for(int[] row: temp){
			for(int data : row){


				maxVal = data > maxVal ? data : maxVal; 
			}
		}
	
		System.out.println("Max matrix : "+maxVal);
	}

}