
// import LLNode;
import java.util.LinkedList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class LLKthLast{
	
	public static void main(String z[]) throws IOException{

		int[] a = {3,4,5,2,1};

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the kth elem : ");
		int kthElem = Integer.parseInt(br.readLine());

		System.out.println("Input diff : "+kthElem);

		Node head = new Node(a[0]);

		for(int i =1; i < a.length; ++i)
			head.append(a[i]);

		Node temp = head;

		while(temp != null){

			System.out.println(temp.getData());
			temp = temp.getNext();
		}

		// Laging pointer
		Node laging = head;

		// Leading pointer

		Node leading = head;


		for(int i =0; i < kthElem; ++i){

			if(leading == null){

				System.out.println("Kth element is greater than linked list size");
				return;
			}

			leading = leading.getNext();
		}


		while(leading != null){

			leading = leading.getNext();
			laging = laging.getNext();
		}

		System.out.println("\nKth elem from last : "+laging.getData());
	}
}

class Node{

	Node next;
	int data;

	public Node(int data){
		this.data = data;
	}

	public void setNext(Node next){
		this.next = next;
	}

	public Node getNext(){
		return this.next;
	}

	public void setData(int data){
		this.data = data;
	}

	public int getData(){
		return this.data;
	}


	public void append(int val){

		Node curr = this;


		Node prev = curr;
		while(curr != null){
			prev = curr;
			curr = curr.getNext();
		}

		Node tmp = new Node(val);

		prev.setNext(tmp);
	}
}