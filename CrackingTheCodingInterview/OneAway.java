
/* pale -> ple
1 for a

pales -> pale

1 for s

pale -> bale

1 for p

pale -> bake

2 for p and l */





import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.math.*;


class OneAway{

	public static void main(String z[]) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("\nInput 1 : ");
		String input1 = br.readLine();

		System.out.print("\nInput 2 : ");
		String input2 = br.readLine();

		String s1 = input1.length() > input2.length() ? input1: input2;
		String s2 = input1.length() > input2.length() ? input2: input1;

		System.out.println("\nString 1 : "+s1);
		System.out.println("String 2 : "+s2);

		boolean found = false;

		int i =0, j =0;

		while(i < s1.length() && j < s2.length()){


			if(s1.charAt(i) != s2.charAt(j)){

				if(found) {
					System.out.println("Strings are not one step edit away.");
					return;
				}

				found = true;


				if(s1.length() == s2.length())
					++j;
			}else{
				++j;
			}
			++i;
		}

		System.out.println("Strings are one step edit away.");
	}
}






