
import java.util.Arrays;

class RotateImage{

	public static void main(String z[]){

		System.out.println("Rotate image");

		int[][] image = {
			{9,13,5,2},
			{1,11,7,6},
			{3,7,4,1},
			{6,0,7,10}
		};


		System.out.println("\n");

		for(int[] row : image){

			System.out.println("Row : "+Arrays.toString(row));
		}

		int rows = image.length;
		int columns = image.length;

		int[][] tImage = new int[rows][columns];
		

		System.out.println("\n");

		for(int i =0; i <rows; ++i)
			for(int j =0; j < columns; ++j)
				tImage[j][i] = image[i][j];


		// Finding transpose

		for(int[] row : tImage){

			System.out.println("Row : "+Arrays.toString(row));
		}

		System.out.println("\n");

		// Rotating 180

		int[][] rImage = new int[rows][columns];

		for(int i =0; i <rows; ++i){

			for(int j=0; j <columns; ++j){

				rImage[i][j] = tImage[i][columns-1-j];
			}
		}


		// Rotated Matrix

		for(int[] row : rImage){

			System.out.println("Row : "+Arrays.toString(row));
		}
		
	}
}