import java.util.Map;
import java.util.HashMap;

class LLIntersection{


	public static void main(String z[]){

		System.out.println("Linked list intersection");

		int[] arr1 = {1,3,4,3,2,4};
		int[] arr2 = {5,2,3,1,4,6,7,8,9,5,4,2};

		Node node = null;

		for(int c : arr1){
			if(node == null){
				node = new Node(c);
			}else{
				node.append(c);
			}
		}

		Node node1 = null;

		for(int c : arr2){
			if(node1 == null){
				node1 = new Node(c);
			}else{
				node1.append(c);
			}
		}

		Map<Integer, Boolean> map = new HashMap<Integer, Boolean>();

		while(node != null){

			map.put(node.data, true);
			node = node.next;
		}

		System.out.println("Map : "+map);


		while(node1 != null){

			
			if(map.containsKey(node1.data)){

				System.out.println("Intersection point : "+node1.data);
				return;
			}

			node1 = node1.next;
		}

		System.out.println("No intersection node found");

		
	}
}

class Node{

	Node next = null;

	int data;


	public Node(int data){
		this.data = data;
	}

	public void append(int val){

		Node curr = this;

		if(curr == null){
			new Node(val);
			return;
		}

		while(curr.next != null)
			curr = curr.next;

		curr.next = new Node(val);
	}
}