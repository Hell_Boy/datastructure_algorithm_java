
class SumList{

	public static void main(String z[]){

		System.out.println("Sum List\n");

		int[] arr1 = {7,1,6};
		int[] arr2 = {5,9,2};

		Node ll1 = null;

		for(int elem : arr1){


			if(ll1 == null){
				ll1 = new Node(elem);
			}else{

				ll1.append(elem);
			}
		}


		Node ll2 = null;

		for(int elem : arr2){

			if(ll2 == null){
				ll2 = new Node(elem);
			}else{

				ll2.append(elem);
			}
		}

		


		int carry = 0;


		while(ll1 != null && ll2 != null){

			int val1 = (ll1 == null ? 0 : ll1.data);
			int val2 = (ll2 == null ? 0 : ll2.data);
			
			int sum = carry +  val1 + val2;

			carry = sum / 10;
			sum = sum % 10;

			System.out.print(sum + " ");
			ll1 = ll1.next;
			ll2 = ll2.next;
		}

		System.out.println();

	}
}


class Node{

	Node next;
	int data;

	public Node(int data){
		this.data = data;
	}

	public void append(int val){

		Node curr = this;

		Node prev = curr;

		while(curr != null){

			prev = curr;
			curr = curr.next;
		}

		prev.next = new Node(val);
	}
}
