import java.lang.*;

class LoaderMain{

  public static void main(String z[]) throws Exception{

    System.out.println("Main loader : Hello world");

    // First f = new First();

    Class cls = Class.forName("First");
    First f = (First) cls.newInstance();
    f.log();

  }
}

class First{

  static {
  
    System.out.println("first");
  }

  {
    System.out.println("first instance block called");
  }

  public void log(){

    System.out.println("dumping log message");
  }
}

class Second{

  static{
    System.out.println("second");
  }

  {
    System.out.println("second instance block called");
  }
}
