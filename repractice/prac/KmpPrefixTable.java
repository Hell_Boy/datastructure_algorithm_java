import java.util.Arrays;

class KmpPrefixTable {

  public static void main(String z[]) {

    String T = "bacbabababacaca";
    String P = "ababaca";

    KmpPrefixTable k = new KmpPrefixTable();

    int[] F = k.piTable(P.toCharArray());
    System.out.println("Prefix table : " + Arrays.toString(F));

    int index = k.subString(T.toCharArray(), P.toCharArray(), F);

    System.out.println("Substring index : " + index);
  }

  private int[] piTable(char[] c) {

    int[] F = new int[c.length];

    int i = 1, j = 0;

    while (i < c.length) {

      if (c[i] == c[j]) {

        ++j;
        F[i] = j;
        ++i;
      }

      else if (j > 0)
        j = F[j - 1];

      else
        ++i;
    }

    return F;
  }

  private int subString(char[] T, char[] P, int[] F) {

    int i = 0, j = 0;

    while (i < T.length) {


      if (T[i] == P[j]) {

        if (j == P.length-1)
          return i - j;
        
          ++i;
          ++j;

      } else if (j > 0 )
        j = F[j - 1];

      else
        ++i;
    }

    return -1;
  }
}
