import java.util.Arrays;
class Solution {

  public static void main(String z[]) {

    int[] a = { 1, 2, 3, 4, 5 };

    int[] sol = new int[a.length];

    int start = a[0];
    int end = 1;

    // Skipping first
    for (int i = 1; i < a.length; ++i) {
      end = end * a[i];
    }

    sol[0] = end;

    int ptr = 1;
    for (int i = 1; i < a.length; ++i) {
      end = (end / a[i]) * start;



      if(i +1 == a.length){
        end = start;
      }

      start = start * a[i];

      sol[ptr] = end;
      ++ptr;


    }


    System.out.println(Arrays.toString(sol));

  }
}
