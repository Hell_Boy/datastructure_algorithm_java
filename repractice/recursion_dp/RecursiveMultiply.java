
class RecursiveMultiply{

  public static void main(String z[]){
    System.out.println("Recursive multiply");

    long start, stop;

    int n = 2, m = 15;
    start = System.nanoTime();
    int val = new RecursiveMultiply().multiply(n,m);
    stop = System.nanoTime();
    System.out.println("product : "+val+" duration : "+(stop-start));

    int temp = n;
    start = System.nanoTime();
    for(int i =0; i < m-1; ++i){
      temp+=n;
    }
    stop = System.nanoTime();
    System.out.println("product : "+temp+" duration : "+(stop-start));

  }

  private int multiply(int n, int m){

    if(m < 1)
    return 0;

    if(m == 1)
    return n;

    int remainder = m%2;

    int temp;
    if(remainder == 0){
      m /=2;
      temp = multiply(n, m);
      return temp+temp;
    }else{
      m -= 1;
      m /=2;
      temp = multiply(n, m);
      return temp+temp+n;
    }

  }
}
