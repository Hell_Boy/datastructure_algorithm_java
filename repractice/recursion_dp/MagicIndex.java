
class MagicIndex{

  public static void main(String z[]){

    System.out.println("Magic Index");

    int[] a = {-40,-20,-1,1,2,3,5,7,9,12,13};
    int[] a2 = {-10,-5,2,2,2,3,4,7,9,12,13};

    long start, end;

    start = System.nanoTime();
    boolean found = new MagicIndex().solution(0, a.length, a);
    end = System.nanoTime();
    System.out.println("Index found : "+found+ " duration : "+(end-start));

    start = System.nanoTime();
    found = new MagicIndex().solution2(0, a2.length, a2) > -1;
    end = System.nanoTime();
    System.out.println("Index found : "+found+ " duration : "+(end-start));


  }

  // For distinct elements
  private boolean solution(int start, int end, int[] a){

    if(end < start)
    return false;

    int mid = start+((end-start)/2);

    if(a[mid] == mid)
    return true;

    if(a[mid] > mid)
    return solution(start, mid-1, a);

    return solution(mid+1, end, a);
  }

  // For duplicate elements
  private int solution2(int start, int end, int[] a){

    if(end < start)
    return -1;

    int mid = start + ((end-start)/2);

    if(a[mid] == mid)
    return mid;

    int min = Math.min(mid-1, a[mid]);
    int max = Math.max(mid+1, a[mid]);

    // search left
    int left = solution2(start, min, a);

    if(left > 0)
    return left;

    // search right
    int right = solution2(max, end, a);
    // if(right > 0)
    return right;
  }
}
