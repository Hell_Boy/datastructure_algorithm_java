
// https://www.geeksforgeeks.org/find-given-string-can-represented-substring-iterating-substring-n-times/

import java.util.Arrays;

class RepeatingSubstring{

  public static void main(String z[]){

    // String s = "ababaca";
    String s = "abcabcabc";

    int len = new RepeatingSubstring().prefixTable(s);

    boolean madeFromSubstring = len > 0 && s.length() %(s.length()-len) == 0;

    System.out.println("Can be made using repeated substring : "+madeFromSubstring);
  }

  private int prefixTable(String s){
    int[] t = new int[s.length()];

    int i = 1, j = 0, m = s.length();

    t[0] = 0;

    while(i < s.length()){

      if(s.charAt(i) == s.charAt(j)){
        ++j;
        t[i] = j;
        ++i;
      }else if(j > 0){
        j = t[j-1];
      }else{
        t[i] = 0;
        ++i;
      }
    }


    System.out.println(Arrays.toString(s.toCharArray()));
    System.out.println(Arrays.toString(t));

    return t[t.length-1];
  }
}
