
import java.util.List;
import java.util.ArrayList;
import java.math.BigInteger;

class Problem {

  public static void main(String z[]) {

    List<List<String>> test = new ArrayList<List<String>>();

    List<String> l1 = new ArrayList<String>(){
      {add("3");};
      {add("-2");}
    };

    List<String> l2 = new ArrayList<String>(){
      {add("5");};
      {add("5");};
      {add("4");};
      {add("2");}
    };

    List<String> l3 = new ArrayList<String>(){
      {add("4");};
      {add("4");};
      {add("4");};
    };

    List<String> l4 = new ArrayList<String>();

    List<String> l5 = new ArrayList<String>(){
      {add("-214748364801");};
      {add("-214748364802");};
    };


    test.add(l1);
    test.add(l2);
    test.add(l3);
    test.add(l4);
    test.add(l5);

    String format = "case : %d result : %s";

    int testCase = 1;
    for(List<String> c : test){

      String result = new Problem().solution(c);
      System.out.println(String.format(format, testCase, result));
      ++testCase;
    }

  }

// Solution function
  private String solution(List<String> num) {

    if (num.size() < 2)
      return "-1";

    BigInteger a = new BigInteger(num.get(0));
    BigInteger b = null;

    for(int i  = 1; i < num.size(); ++i){

      BigInteger t = new BigInteger(num.get(i));

      int comp = t.compareTo(a);


      if(comp > 0){

        b = a;
        a = t;

      }else if(comp < 0){

        if(b == null){
          b = t;
        }else{

          if(t.compareTo(b) > 0)
          b = t;
        }
      }
    }

    return b == null ? "-1" : b.toString();
  }
}
