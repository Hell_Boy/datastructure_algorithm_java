import java.util.ArrayList;

class PowerSet{

  public static void main(String z[]){

    System.out.println("Power set");

    int[] a = {1,2,3};

    ArrayList<ArrayList<Integer>> sets = new PowerSet().solution(0, a);

    System.out.println(sets);
  }

  private ArrayList<ArrayList<Integer>> solution(int n, int[] a){

    ArrayList<ArrayList<Integer>> result;
    if(n == a.length){

      result = new ArrayList<ArrayList<Integer>>();
      result.add(new ArrayList<Integer>());

    }else{

      result = solution(n+1,a);

      int elem = a[n];

      ArrayList<ArrayList<Integer>> temp = new ArrayList<ArrayList<Integer>>();

      for(ArrayList<Integer> entry : result){

        ArrayList<Integer> sublist = new ArrayList<Integer>();
        sublist.addAll(entry);
        sublist.add(elem);
        temp.add(sublist);
      }

      result.addAll(temp);
    }

    return result;
  }
}
