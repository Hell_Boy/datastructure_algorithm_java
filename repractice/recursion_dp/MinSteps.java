// Minimum Steps in infinite grid

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

class MinSteps{

  Map<int[], Integer> map = new HashMap<int[], Integer>();

  public static void main(String z[]){



    System.out.println("Minimum steps in infinite grid");

    ArrayList<Integer> A = new ArrayList<Integer>(){{
      add(0);
      add(1);
      add(1);
    }};


    ArrayList<Integer> B = new ArrayList<Integer>(){{
      add(0);
      add(1);
      add(2);
    }};

    int ans = 2;

    long start = 0, end = 0;

    start = System.nanoTime();
    int result = new MinSteps().coverPoints(A,B);
    end = System.nanoTime();

    System.out.println("Result : "+result);
    if(ans == result)
      System.out.println("Success");

    else
      System.out.println("Failed");

    System.out.println("Execution time : "+((end-start)/1000)+" ms");
  }


  private int coverPoints(ArrayList<Integer> A, ArrayList<Integer> B) {
      int steps = 0;


      for(int i =0; i < A.size()-1; ++i){

        steps += Math.max(Math.abs(A.get(i)-A.get(i+1)),Math.abs(B.get(i)-B.get(i+1)));
      }

      return steps;
    }


}
