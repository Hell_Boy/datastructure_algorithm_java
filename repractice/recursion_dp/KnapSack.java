import java.util.Arrays;
class KnapSack{

  public static void main(String z[]){

    System.out.println("Hello world");

    int[] val = { 20, 5, 10, 40, 15, 25 };
    int[] wt = { 1, 2, 3, 8, 7, 4 };
    int W = 10;

    int[][] dp = new int[wt.length+1][W+1];


    for(int i =1; i < wt.length+1; ++i)
      for(int j = 1; j < W+1; ++j){

        if(j >=wt[i-1])
          dp[i][j] = Math.max(val[i-1]+dp[i-1][j-wt[i-1]], dp[i-1][j]);
        else
          dp[i][j] = dp[i-1][j];
       }

       for(int[] row : dp)
         System.out.println(Arrays.toString(row));

  }
}
