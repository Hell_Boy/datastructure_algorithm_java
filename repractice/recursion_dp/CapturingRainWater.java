// https://www.youtube.com/watch?v=C8UjlJZsHBw&t=1361s

class CapturingRainWater{

  public static void main(String z[]){

    int[] n = {4,2,3,4,6,8};

    int water = new CapturingRainWater().twoPointer(n);

    System.out.println("Water quantity : "+water);
  }

  private int twoPointer(int[] n){

    if(n.length < 3)
    return 0;

    int trappedWater = 0;

    int maxLeft = n[0];
    int maxRight = n[n.length-1];

    int left = 1;
    int right = n.length-2;

    while(left < right){

      if(maxLeft < maxRight){
        // left greater
        if(maxLeft < n[left])
          maxLeft = n[left];
        else
        trappedWater += maxLeft -n[left];
        ++left;
      }else{
        // right greater
        if(maxRight < n[right])
          maxRight = n[right];
        else
          trappedWater += maxRight - n[right];

          ++right;
      }
    }

    return trappedWater;
  }

}
