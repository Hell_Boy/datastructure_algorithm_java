import java.util.Arrays;

class QuickSort{

  int[] a = {50, 25,92,16,76,30,43,54,19};

  public static void main(String z[]){

    System.out.println("Quick sort impl");

    QuickSort qS = new QuickSort();

    System.out.println("Unsorted array : "+Arrays.toString(qS.a));

    int[] t = qS.sort(qS.a, 0, qS.a.length-1);

    System.out.println("Sorted array : "+Arrays.toString(t));
  }

  private int[] sort(int[] a, int low, int high){

    if(high > low){

      int pivot = new QuickSort().partition(a, low, high);
      sort(a, low, pivot-1);
      sort(a, pivot+1, high);
    }
    return a;
  }

  private int partition(int[] a, int low, int high){

    int left = low, right = high, pivot_item = a[low];

    while(left < right){

      while(a[left] <= pivot_item)
      ++left;

      while(a[right] > pivot_item)
      --right;

      if(left < right){

        // swap items
        int temp = a[left];
        a[left] = a[right];
        a[right] = temp;
      }
    }

    a[low] = a[right];
    a[right] = pivot_item;
    return right;
  }
}
