

class BT{

  BT left;
  BT right;
  int data;

  BT(int data){
    this.data = data;
  }
}


class Solution{

  public static void main(String z[]){

    int[] in = {4,2,5,1,8,6,9,3,7};
    int[] p = {1,2,4,5,3,6,8,9,7};

    BT tree = new Solution().build(in, p, 0, p.length-1, 0, in.length-1);

    new Solution().preOrderTraversal(tree);
    System.out.println();
  }

  private  void preOrderTraversal(BT tree){

    if(tree == null)
    return;

    int root = tree.data;

    System.out.print(root+" ");
    preOrderTraversal(tree.left);
    preOrderTraversal(tree.right);
  }

  private BT build(int[] in, int[] p, int pS, int pE, int iS, int iE){

    System.out.println("PS : "+pS);
    if(pS > pE || iS > iE)
    return null;

    BT r = new BT(p[pS]);
    int offset = iS;

    for(; offset < iE; ++offset)
      if(in[offset] == r.data)
      break;

      String format = "%s => root: %d, offset : %d ,pS : %d, pE : %d, iS : %d, iE : %d";

      System.out.println("\n"+String.format(format,"Left", p[pS], offset,pS+1, pS+offset-iS, iS, offset-1));
      r.left = build(in, p, pS+1, pS+offset-iS, iS, offset-1);

      System.out.println("\n"+String.format(format,"Right", p[pS], offset,pS+offset-iS+1, pE, offset+1, iE));
      r.right = build(in, p, pS+offset-iS+1, pE, offset+1, iE);

      return r;
  }
}
