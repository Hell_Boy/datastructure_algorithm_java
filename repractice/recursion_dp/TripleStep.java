
class TripleStep{

  public static void main(String z[]){

    System.out.println("Triple Step");

    long start, stop;

    int steps = 11, ways;
    String format = "method : %s , ways : %d , duration : %d";



    start = System.nanoTime();
    ways = new TripleStep().solution2(steps, new int[steps+1]);
    stop = System.nanoTime();

    System.out.println(String.format(format, "memoization", ways, (stop-start)));

    start = System.nanoTime();
    ways = new TripleStep().solution1(steps);
    stop = System.nanoTime();

    System.out.println(String.format(format, "recursive", ways, (stop-start)));

  }

  private int solution1(int steps){

    if(steps < 0)
    return 0;

    if(steps == 0)
    return 1;

    return solution1(steps-1) + solution1(steps-2) + solution1(steps-3);

  }

  private int solution2(int steps, int[] memo){

    if(steps < 0)
    return 0;

    if(steps == 0)
    return 1;

    if(memo[steps] > 0)
    return memo[steps];

    memo[steps] = solution2(steps-1, memo) + solution2(steps-2, memo) + solution2(steps-3, memo);

    return memo[steps];
  }
}
