import java.util.List;
import java.util.ArrayList;
import java.lang.Math;

class NthSqrt {

  private int factor(int n, int f) {

    List<Integer> comp = new ArrayList<Integer>();

    for (int i = 1; i * i <= n; ++i) {

      if (n % i == 0) {

        --f;

        if (f == 0)
          return i;

        int compVal = n / i;

        if (i != compVal)
          comp.add(n / i);

      }
    }

    int t = comp.size() - f;

    System.out.println("comp : "+comp+" size : "+comp.size()+" remaining factor : "+f);

    if (t < 0)
      return -1;

    return comp.get(t);
  }

  public static void main(String z[]) {

    System.out.println("Hello world");

    int num = 32, fact = 4;

    int result = new NthSqrt().factor(num, fact);

    System.out.println("Result : " + result);
  }

}
