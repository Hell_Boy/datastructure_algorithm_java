import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;

class SortMapByValue{

  public static void main(String z[]){
    System.out.println("Sort By value");

    Map<Character,Integer> map = new HashMap<Character,Integer>();
    map.put('a', 4);
    map.put('b', 2);
    map.put('c', 3);
    map.put('d', 1);
    map.put('e', 8);
    map.put('f', 9);

    System.out.println("Map : "+map);

    // new SortMapByValue().method1(map);

    new SortMapByValue().method2(map);

  }

  private void method1(Map<Character, Integer> map){

    Map<Integer, Character> sortedMap = new TreeMap<Integer, Character>();

    for(Map.Entry<Character, Integer> entry : map.entrySet()){

      sortedMap.put(entry.getValue(), entry.getKey());
    }


    System.out.println("Sorted by value : "+sortedMap);
  }

  private void method2(Map<Character, Integer> map){

    List<Map.Entry<Character, Integer>> list = new ArrayList<Map.Entry<Character, Integer>>(map.entrySet());

    list.sort((o1,o2)-> o1.getValue().compareTo(o2.getValue()));

    Map<Character, Integer> newMap = new LinkedHashMap<Character, Integer>();

    for(Map.Entry<Character, Integer> e : list){

      System.out.println("Key : "+e.getKey()+" Value : "+e.getValue());
      newMap.put(e.getKey(), e.getValue());
    }


    System.out.println("New map : "+newMap);

  }
}
