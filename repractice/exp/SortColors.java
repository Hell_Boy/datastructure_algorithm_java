import java.util.Arrays;

class SortColors{

  public static void main(String z[]){

    System.out.println("Hello world");

    int[] nums = {2,0,2,1,1,0};
    int[] ans = new int[nums.length];

    Arrays.fill(ans,1);

    int start = 0, end = nums.length-1;

    for(int i =0; i < nums.length; ++i){

      if(nums[i] == 0){
        ans[start] = 0;
        ++start;
      }

      if(nums[i] == 2){
        ans[end] = 2;
        --end;
      }
    }

    System.out.println(Arrays.toString(ans));

  }
}
