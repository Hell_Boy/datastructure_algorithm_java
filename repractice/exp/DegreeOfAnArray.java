
import java.util.Map;
import java.util.HashMap;

class DegreeOfAnArray{

  public static void main(String z[]){

    System.out.println("Hello world");

    // int[] nums = {1,2,2,3,1};
    // int[] nums = {1,2,2,3,1,4,2};
    // int[] nums = {1};
    int[] nums = {2,1,1,2,1,3,3,3,1,3,1,3,2};

    Map<Integer, int[]> map = new HashMap<Integer, int[]>();

    int degree = 0, length = 0;

    for(int i =0; i < nums.length; ++i){

      int[] freq = map.getOrDefault(nums[i], new int[]{0,i});
      ++freq[0];
      map.put(nums[i], freq);


      if(degree < freq[0]){
        degree = freq[0];
        length = i - freq[1] + 1;

      }else if(degree == freq[0]){
        System.out.println("Num : "+nums[i]+" Degree : "+freq[0]+" Len : "+freq[1]);
        length = Math.min(length, i-freq[1]+1);
      }
    }

    System.out.println("Ans : "+length);

  }
}
