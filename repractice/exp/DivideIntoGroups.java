import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Arrays;

class DivideIntoGroups{

  public static void main(String z[]) throws IOException{

    System.out.println("Hello world");

    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    int n = Integer.parseInt(br.readLine());
    int k = Integer.parseInt(br.readLine());

    String msg = "Numbers : %d, Groups : %d";

    System.out.println(String.format(msg, n, k));

    int comb = new DivideIntoGroups().maxCombination(n, k);

    System.out.println("Possible combinations : "+comb);
  }

  private int maxCombination(int n, int k){

    if(n == 0 || k == 0 || n < k)
    return 0;

    int[][] dp = new int[n+1][k+1];

    for(int i = 1; i < n+1; ++i)
      for(int j = 1; j < k+1; ++j){

        if(n < k)
          dp[i][j] = 0;

        else if(j == 1 || i == j)
          dp[i][j] = 1;

        else
          dp[i][j] = dp[i-1][j-1] + j * dp[i-1][j];
      }

      for(int[] row : dp)
        System.out.println(Arrays.toString(row));

    return dp[n][k];
  }
}
