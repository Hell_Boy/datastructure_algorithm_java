
import java.io.BufferedReader;
import java.lang.Exception;
import java.io.InputStreamReader;
import java.util.Stack;

class BalanceParenthesis{

  public static void main(String z[]) throws Exception{

    System.out.println("Hello world");

    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    String input = br.readLine();

    System.out.println("Input : "+input);

    boolean balanced = new BalanceParenthesis().isBalanced(input.toCharArray());

    System.out.println("isBalanced : "+balanced);
  }

  private boolean isBalanced(char[] chars){


    Stack<Character> st = new Stack<Character>();

    for(char c : chars){

      if(c == '{' || c == '(' || c == '[')
      st.push(c);

      else if(st.isEmpty())
      return false;

      else if(c == '}' && st.peek() == '{')
      st.pop();

      else if(c == ')' && st.peek() == '(')
      st.pop();

      else if(c == ']' && st.peek() == '[')
      st.pop();

      else
        return false;
    }

    return st.isEmpty();
  }

}
