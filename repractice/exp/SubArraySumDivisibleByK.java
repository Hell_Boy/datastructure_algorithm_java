
import java.util.Map;
import java.util.HashMap;

class SubArraySumDivisibleByK{

  public static void main(String z[]){

    System.out.println("Hello world");

    int[] a = {-1,-9,-4,0};
    int k = 9;

    int sum = 0, rem = 0, ans = 0;

    Map<Integer, Integer> map = new HashMap<Integer, Integer>();
    map.put(0,1);

    for(int i =0; i < a.length; ++i){

      sum +=a[i];

      if(sum % k == 0) ++ans;

      rem = sum%k > 0 ? sum%k : (sum % k)+k;

      if(map.containsKey(rem))
      ans+=map.get(rem);

      map.put(rem, map.getOrDefault(rem,0)+1);
    }

    System.out.println("Cummulative sum : "+sum);
    System.out.println("Map : "+map);
    System.out.println("Answer : "+ans);

  }

}
