import java.lang.Math;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

class MaxSlidingWindow {

  public static void main(String z[]) {

    System.out.println("Hello world");

    Map<int[], Integer> test = new HashMap<int[], Integer>();
    test.put(new int[]{ 1, 3, -1, -3, 5, 3, 6, 7 }, 3);
    test.put(new int[]{ 1, -1 }, 1);
    test.put(new int[]{9, 11}, 2);
    test.put(new int[]{4, -2}, 2);
    test.put(new int[]{1}, 1);
    test.put(new int[]{7, 2, 4}, 2);
    test.put(new int[]{1,3,1,2,0,5}, 3);

    for(Map.Entry<int[], Integer> entry : test.entrySet())
      new MaxSlidingWindow().max(entry.getKey(), entry.getValue());
  }


  private void max(int[] a, int k) {

    if(k == 1){

      System.out.println(Arrays.toString(a));
      return;
    }

    int i = 0, start = 0, end = start+k, max = Integer.MIN_VALUE;

    for(; i < k; ++i)
    max = Math.max(max, a[i]);

    List<Integer> ll = new ArrayList<Integer>();
    ll.add(max);

    for(; i < a.length; ++i){

      // System.out.println(a[i-k]+" == "+max);

      max = Math.max(a[i], max);

      // System.out.println(ll);
      // System.out.println("i : "+i+" k : "+k+" i-k : "+(i-k)+" max : "+max);
      if(i-k > -1 && a[i-k] == max){
        // recalculate max
        max = Integer.MIN_VALUE;

        // System.out.println("Ran");

        for(int j = i-k+1; j < i+1; ++j)
          max = Math.max(max, a[j]);

      }else{
        max = Math.max(max, a[i]);
      }

      ll.add(max);
    }

    System.out.println(Arrays.toString(a)+" : "+ll);
  }
}
