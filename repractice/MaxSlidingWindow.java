import java.lang.Math;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

class MaxSlidingWindow {

  public static void main(String z[]) {

    System.out.println("Hello world");

    int[] a = { 1, 3, -1, -3, 5, 3, 6, 7 };
    int k = 3;

    new MaxSlidingWindow().max(a, k);

  }


  private void max(int[] a, int k) {

    int i = 0;
    int tempMax = Integer.MIN_VALUE;

    for(; i < k+1; ++i)
    tempMax = Math.max(tempMax, a[i]);

    List<Integer> ll = new ArrayList<Integer>();

    ll.add(tempMax);

    for (; i < a.length; ++i) {


      if(a[i] != tempMax){

        tempMax = Math.max(tempMax, a[i]);
      }else{

        for(int j = i; j < i+k; ++j)
        tempMax = Math.max(tempMax, a[i]);
      }

      ll.add(tempMax);
    }

    System.out.println(ll);

  }
}
