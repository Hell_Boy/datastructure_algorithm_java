class TowerOfHanoi {

    public static void main(String z[]) {
     
        System.out.println("Tower of Hanoi");
        fnTowerOfHanoi(4, 'A', 'C', 'B');
    }

    private static void fnTowerOfHanoi(int n, char fromPeg, char toPeg, char auxPeg){

        if(n==1){
            System.out.println("Move disk "+n+" from peg "+fromPeg +" to "+toPeg);
            return;
        }
       
        /**
         * A : Source Peg
         * B : Auxillary Peg
         * C : Destination Peg (toPeg)
         */

        // Move n-1 disk from A to B using C as auxillary peg
        // fnTowerOfHanoi(n-1, fromPeg, auxPeg, toPeg);
        fnTowerOfHanoi(n-1, fromPeg, auxPeg, toPeg);

        // Move remaining from A to C
        System.out.println("Move disk "+n+" from peg "+fromPeg +" to peg "+toPeg);

        // Move n-1 disk from B to C using A as auxillary peg
        fnTowerOfHanoi(n-1, auxPeg, toPeg, fromPeg);

    }
}