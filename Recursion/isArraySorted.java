/**
 * isArraySorted
 */
public class isArraySorted {

    public static void main(String z[]) {
        
        int a[] = new int[]{1, 2, 3, 4, 5, 6};

        int size = a.length;
        System.out.println("isSorted : "+sorted(a, size));
    }

    private static int sorted(int a[], int size ){


        // Already sorted
        if(size == 1 || size == 0)
        return 1;

        return a[size - 1] < a[size-2] ? 0 : sorted(a, size-1);
    }
}