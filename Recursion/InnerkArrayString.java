import java.util.Arrays;

/**
 * InnerkArrayString
 */
public class InnerkArrayString {

    int a[];

    public InnerkArrayString(int size){
        a = new int[size];
    }

    public void base_K_array_String(int size, int k){

        if( size <= 0 ){

            System.out.println("K array string : "+ Arrays.toString(a));
        }else{

            for(int i =0 ; i < k; i++){
                a[size-1] = i;
                base_K_array_String(size-1, k);
            }
        }
    }
}