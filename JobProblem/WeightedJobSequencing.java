
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;

class WeightedJobSequencing{


	public static void main(String z[]){

		System.out.println("Weighted job sequencing");

		int[] start = {0,0,1,2,5,10};
		int[] end = {1,10,2,4,15,15};
		int[] profit = {2,10,3,1,13,7};

		int i =0, j =0;

		List<Job> jobs = new ArrayList<Job>();

		for(i =0; i < start.length; ++i){

			jobs.add(new Job(start[i], end[i], profit[i]));
		}

		Collections.sort(jobs, (o1,o2)-> o1.end > o2.end ? 0 : -1);

		int[] tempProfit = new int[profit.length];

		for(i =0; i < jobs.size(); ++i){

			Job job = jobs.get(i);
			start[i] = job.start;
			end[i] = job.end;
			profit[i] = job.profit;
			tempProfit[i] = job.profit;
			String result = String.format("start : %d, end : %d, profit : %d", job.start, job.end, job.profit);
			System.out.println(result);
		}

		for(i =1; i < tempProfit.length; ++i){
			for(j =0; j < i; ++j){

				// System.out.println("i : "+i+" j : "+j);
				if(end[j] <= start[i] && (tempProfit[j] + profit[i]) > tempProfit[i]){
					tempProfit[i] = tempProfit[j] + profit[i];
					System.out.println("i : "+i+" j : "+j);
					System.out.println("New profits : "+Arrays.toString(tempProfit));
				}
			}
		}

		// System.out.println("New profits : "+Arrays.toString(tempProfit));

	}
}

class Job {

	int start;
	int end;
	int profit;

	public Job(int start, int end, int profit){
		this.start = start;
		this.end = end;
		this.profit = profit;
	}
}