
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;

class JobSequencing{

	

	public static void main(String z[]){

		System.out.println("Job sequencing");

		char[] jobId = {'a','b','c','d','e'};
		int[] profit = {20,15,10,5,1};
		int[] deadline = {2,2,1,3,3};

		List<Job> jobs = new ArrayList<Job>();

		int maxDeadline = Integer.MIN_VALUE;

		for(int i =0; i < jobId.length; ++i){

			maxDeadline = maxDeadline < deadline[i] ? deadline[i] : maxDeadline;
			jobs.add(new Job(jobId[i], profit[i], deadline[i]));
		}

		Collections.sort(jobs, (j1, j2)-> j2.profit < j1.profit ? -1 : 0);


		System.out.println("Max deadline : "+maxDeadline);

		int[] slots = new int[maxDeadline];

		for(Job j : jobs){
			String result = String.format("jobid : %s profit : %d deadline : %d", j.jobId, j.profit, j.deadline);
			System.out.println(result);

			int position = j.deadline-1;

			while(position > -1){

				if(slots[position] == 0){
					slots[position] = j.profit;
					break;
				}else{
					--position;
				}
			}
		}

		// System.out.println("Profits : "+Arrays.toString(slots));

		int maxProfitVal = 0;

		for(int val : slots)
			maxProfitVal +=val;

		System.out.println("Max profit : "+maxProfitVal);

	}

	
}

class Job{

		char jobId;
		int profit;
		int deadline;
		public Job(char jobId, int profit, int deadline){


			this.jobId = jobId;
			this.profit = profit;
			this.deadline = deadline;
		}
	}