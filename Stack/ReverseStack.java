import java.util.Iterator;
import java.util.Stack;

class ReverseStack {

    public static void main(String[] args) {
        
        // System.out.println("Hello world");

        Stack<Integer> stack = new Stack<Integer>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);

        System.out.println("Print Stack : "+stack.toString());
        System.out.println("Rev Stack : "+reverseStack(stack).toString());
    }


    public static Stack<Integer> reverseStack(Stack<Integer> stack){

        Stack<Integer> stack2 = new Stack<Integer>();

        while(!stack.isEmpty())
        stack2.push(stack.pop());

        return stack2;
    }
}