import java.util.*;

/**
 * Postfix
 */
public class Postfix {

    public static void main(String[] args) {
        String ss = "2*(2+3)+4";
        System.out.println("Equation : "+ss);

        Stack <Character> charStack = new Stack<Character>();
        char [] char_elems = ss.toCharArray();
        for (char character : char_elems) {

            if(new Postfix().isOperator(character)){

                if(charStack.empty()){
                    charStack.push(character);
                }else{
                    char top_elem = charStack.pop();
                    System.out.print(top_elem);
                    charStack.push(character);
                }
            }else if(character == ')' || character == '}'){

                char top_most = charStack.pop();

                while(top_most != '(' || top_most != '{'){
                    System.out.println(top_most);
                    top_most = charStack.pop();
                }
            }
            else{
                System.out.print(character);
            }

            // System.out.println(charStack);
        }

        System.out.print(charStack.pop());
    }

    public boolean isOperator(char c){

        switch (c) {
            case '+':
                return true;
        case '-':
                return true;
        case '*':
                return true;
        case '/':
            return true;
        default:
            return false;
        }
    }
}