FROM openjdk:11

RUN apt-get update -y && apt-get upgrade -y && add-apt-repository ppa:git-core/ppa -y && apt update; apt install git

RUN git --version 
RUN git remote set-url origin https://hell_boy@gitlab.com/Hell_Boy/datastructure_algorithm_java.git

COPY . /src
WORKDIR /src

CMD ["/bin/bash"]