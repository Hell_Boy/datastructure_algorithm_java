// leetcode problem
import java.util.Queue;
import java.util.LinkedList;

class ShortestPath1{

	public static void main(String z[]){

		System.out.println("Shortest path problem");

		int[][] graph = {
			{0,0,0,0},
			{1,1,1,0},
			{1,1,0,0}
		};


		// Solution using bfs
		if(graph[0][0] == 1)
			System.out.println("No starting point found");

		int size = Integer.MIN_VALUE, m = graph.length, n = graph[0].length, row = 0, col = 0;
		int[] point = null;

		int[][] dir = {
			{-1,-1},
			{-1,0},
			{-1,1},
			{0,1},
			{1,1},
			{1,0},
			{1,-1},
			{0,-1}
		};

		Queue<int[]> q = new LinkedList<int[]>();
		q.add(new int[]{0,0,1});
		graph[0][0] = 1;

		
		while(!q.isEmpty()){

			size = q.size();

			while(size -- > 0){

				point = q.poll();

				if(point[0] == m-1 && point[1] == n-1){
					System.out.println("End point reached : "+point[2]);
					return;
				}

				for(int[] d : dir){

					row = d[0] + point[0];
					col = d[1] + point[1];

					if(row >= 0 && row < m && col >=0 && col < n && graph[row][col] == 0){

						q.add(new int[]{row,col,point[2]+1});
						// System.out.println("row : "+ row + " col : "+col);
						graph[row][col] = 1;
					}
				}
			}
		}

		System.out.println("No path found");
		return;
	}
}