// Geeks for geeks (Rat in a maze problem)
import java.util.Queue;
import java.util.LinkedList;
import java.util.List;

class RatMazeShortestPathProblem{


	public static void main(String z[]){


		 int[][] graph = { 
		 		  { 1, 0, 0, 0, 0 },
                  { 1, 1, 1, 1, 1 },
                  { 1, 1, 1, 0, 1 },
                  { 0, 0, 0, 0, 1 },
                  { 0, 0, 0, 0, 1 } };


         if(graph[0][0] == 0){

         	System.out.println("No entry point present");
         	return;
         }

        int rows = graph.length, cols = graph[0].length, size = Integer.MIN_VALUE, r = 0, c2 = 0;

        int[][]dir = {
        	{0,-1},
        	{0,1},
        	{-1,0},
        	{1,0}
        };

        // List<String> paths = new LinkedList<String>();
        Queue<Cordinates> q = new LinkedList<Cordinates>();
        q.add(new Cordinates());
        graph[0][0]= 0;

        while(!q.isEmpty()){

        	size = q.size();

        	while(size-- > 0){

        		Cordinates c = q.poll();
        		if(c.row == rows-1 && c.col == cols-1){

        			System.out.println("Dir : "+c.dir);
        			// return;
        		}else{



	        		for(int i =0; i < dir.length; ++i){

	        			int[] d = dir[i];
	        			r = d[0]+c.row;
	        			c2 = d[1]+c.col;


	        			if(r >=0 && r < rows && c2 >=0 && c2 <cols && graph[r][c2]==1){
	        				String s = "";
	        				if(i == 0)
	        					c.dir += "L";

	        				if(i == 1)
	        					c.dir += "R";

	        				if(i == 2)
	        					c.dir += "U";

	        				if(i == 3)
	        					c.dir += "D";

	        				q.add(new Cordinates(r,c2,c.dir));
	        				graph[r][c2] = 0;
	        			}

	        		}
        		}
        	}
        }

        System.out.println("No direction found");
        return;
	}
}


class Cordinates{
	int row = 0;
	int col = 0;
	String dir = "";

	public Cordinates(){}
	public Cordinates(int row, int col, String d){
		this.row = row;
		this.col = col;
		this.dir = d;
	}

}