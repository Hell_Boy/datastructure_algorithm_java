// Geeks for geeks

import java.util.LinkedList;
import java.util.List;


class RatMazeAllPossiblePathProblem{

	static List<String> paths;
	static String path="";
	static int[][] visited, graph;
	static int row, col;

	public static void main(String z[]){

		System.out.println("Rat maze all path problem");

		paths = new LinkedList<String>();

		graph = new int[][]{ 
	 		{ 1, 0, 0, 0, 0 },
            { 1, 1, 1, 1, 1 },
            { 1, 1, 1, 0, 1 },
            { 0, 0, 0, 0, 1 },
            { 0, 0, 0, 0, 1 } 
        };

        if(graph[0][0] == 0){

        	System.out.println("No entry point found");
        	return;
        }

        row = graph.length;
        col = graph[0].length;


        visited = new int[row][col];
        pathUtil(0,0);

        for(String p : paths)
        	System.out.println(p);


	}

	static boolean isSafe(int r, int c ){
		return (r >=0 && r <row && c >=0 && c <col && visited[r][c] == 0 && graph[r][c] == 1);
	}


	static void pathUtil(int r, int c){

		if(isSafe(r, c)){

			if(r == row-1 && c == col-1){
				paths.add(path);
				return;
			}

			visited[r][c] = 1;

			// Down
			if(isSafe(r+1,c)){
				path+="D";
				pathUtil(r+1,c);
				path = path.substring(0,path.length()-1);
			}


			// Left
			if(isSafe(r,c-1)){
				path+="L";
				pathUtil(r,c-1);
				path = path.substring(0,path.length()-1);
			}

			// Right
			if(isSafe(r,c+1)){
				path+="R";
				pathUtil(r,c+1);
				path = path.substring(0,path.length()-1);
			}

			// Up
			if(isSafe(r-1,c)){
				path+="U";
				pathUtil(r-1,c);
				path = path.substring(0,path.length()-1);
			}

			visited[r][c] = 0;
		}else{
			return;
		}
	}
}